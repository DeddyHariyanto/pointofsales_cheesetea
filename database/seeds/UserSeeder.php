<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\User;
use App\Gerai;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        $faker = Factory::create('id_ID');
        $limit = 10;
        $user = User::get();
        $status = 'aktif';
        // for($i=0; $i < $limit; $i++){
        // 	$data[$i] = [
        //         'level' => 'karyawan',
        //         'nama' => $faker->name, 
        //         'email' => 'kasir'. $i .'@gmail.com',
        //         'no_tlpn' => $faker->e164PhoneNumber, 
        //         'password' => bcrypt('123456'),
        //         're_password' => '123456',
        //         'foto_user' => $faker->image('public/berkas/karyawan',50,50, null, false),
        //         'status_user' => 'aktif',
        //     ];
        // }
        if ($user->isEmpty()) {
            $test = User::create([
                'level' => 'owner',
                'nama' => 'Super User', 
                'email' => 'the.qseries@gmail.com',
                'no_tlpn' => $faker->e164PhoneNumber, 
                'password' => bcrypt('qmilkness@123'),
                're_password' => 'qmilkness@123',
                // 'foto_user' => $faker->image('public/berkas/karyawan',50,50, null, false),
                'status_user' => 'aktif',
            ]);
            // User::insert($data);
        } else {
            $this->command->info('data sudah ada');
        }
    }
}
