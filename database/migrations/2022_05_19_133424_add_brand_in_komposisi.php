<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBrandInKomposisi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('komposisis', function (Blueprint $table) {
            $table->string('brand_komposisi')->nullable()->after('kategori_komposisi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('komposisis','brand_komposisi'))
        {
          Schema::table('komposisis', function (Blueprint $table)
          {
            
            $table->dropColumn('brand_komposisi');
          });
        }
    }
}
