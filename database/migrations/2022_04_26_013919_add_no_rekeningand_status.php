<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNoRekeningandStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaksis', function (Blueprint $table) {
            $table->string('nama_rekening')->nullable()->after('tipe_bayar_transaksi');
            $table->string('no_rekening')->nullable()->after('tipe_bayar_transaksi');
            $table->string('status')->nullable()->after('total_dibayar_transaksi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('transaksis','status','nama_rekening','no_rekening'))
        {
          Schema::table('transaksis', function (Blueprint $table)
          {
            $table->dropColumn('nama_rekening');
            $table->dropColumn('no_rekening');
            $table->dropColumn('status');
          });
        }
    }
}
