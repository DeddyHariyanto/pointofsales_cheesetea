<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->id();
            $table->string('code_transaksi')->nullable();
            $table->integer('id_gerai_transaksi')->nullable();
            $table->integer('id_user_transaksi')->nullable();
            $table->integer('id_customer_transaksi')->nullable();
            $table->integer('tipe_bayar_transaksi')->nullable();
            $table->string('jumlah_bayar_transaksi')->nullable();
            $table->string('discount_transaksi')->nullable();
            $table->string('nama_rekening_transaksi')->nullable();
            $table->string('no_rekening_transaksi')->nullable();
            $table->string('total_dibayar_transaksi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksis');
    }
}
