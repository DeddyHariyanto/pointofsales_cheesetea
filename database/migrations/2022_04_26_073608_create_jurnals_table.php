<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJurnalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jurnals', function (Blueprint $table) {
            $table->id();
            $table->integer('type_id')->nullable();
            $table->integer('doc_id')->nullable();
            $table->integer('gerai_id')->nullable();
            $table->text('title')->nullable();
            $table->text('description')->nullable();
            $table->decimal('debit',12,2)->nullable();
            $table->decimal('kredit',12,2)->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jurnals');
    }
}
