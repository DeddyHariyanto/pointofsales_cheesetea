<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');
// require __DIR__.'/auth.php';


Route::get('/dashboard', function () {
    return view('dashboard');
});
Route::get('/', function () {
    return view('welcome');
});

Route::get('/index', function () {
    return view('page.index');
});

Route::get('/pin', function () {
    return view('page.pin');
});
Route::get('/404', function () {
    return view('page.404');
});

//layout realtime date and times
// Route::get('/realtime-date',function(){
//     $data = date('H:i:s');
//     return response()->json($data);
// });
// kasir

Route::get('/kasir',['as' => 'KasirController.kasir', 'uses' => 'KasirController@kasir']);
Route::post('/kasir-akses',['as' => 'KasirController.kasirAkses', 'uses' => 'KasirController@kasirAkses']);
Route::get('/kasir-struk/{id}',['as' => 'KasirController.kasirStruk', 'uses' => 'KasirController@kasirStruk']);

Route::get('/daftar-menu',['as' => 'PreTransaksiController.menuCustomer', 'uses' => 'PreTransaksiController@menuCustomer']);
Route::post('/kasir-pre-post',['as' => 'PreTransaksiController.preKasirPost', 'uses' => 'PreTransaksiController@preKasirPost']);
Route::get('/kasir-pre-struk/{id}',['as' => 'PreTransaksiController.prekasirStruk', 'uses' => 'PreTransaksiController@prekasirStruk']);

Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::get('/pesanan-online-index',['as' => 'PreTransaksiController.pesananOnlineindex', 'uses' => 'PreTransaksiController@pesananOnlineindex']);
    Route::get('/get-konfirmasi-pembelian/{id}',['as' => 'PreTransaksiController.getKonfirmasiPembelian', 'uses' => 'PreTransaksiController@getKonfirmasiPembelian']);
    Route::post('/konfirmasi-pembelian',['as' => 'PreTransaksiController.konfirmasiPembelian', 'uses' => 'PreTransaksiController@konfirmasiPembelian']);
    // Route::get('/home', 'HomeController@index')->name('home');
    //kasir
    Route::get('/kasir-export-all/{id}',['as' => 'KasirController.exportTransaksiAll', 'uses' => 'KasirController@exportTransaksiAll']);
    Route::get('/kasir-export-hari-ini/{id}',['as' => 'KasirController.exportTransaksiHariIni', 'uses' => 'KasirController@exportTransaksiHariIni']);
    Route::get('/kasir-export-bulan-ini/{id}',['as' => 'KasirController.exportTransaksiBulanIni', 'uses' => 'KasirController@exportTransaksiBulanIni']);
    Route::get('/jurnal-export-bulan-ini/{id}',['as' => 'KasirController.exportJurnalBulanIni', 'uses' => 'KasirController@exportJurnalBulanIni']);
    Route::post('/kasir-post',['as' => 'KasirController.kasirPost', 'uses' => 'KasirController@kasirPost']);
    Route::post('/kasir-kirim-struk',['as' => 'KasirController.kasirKirim', 'uses' => 'KasirController@kasirKirim']);
    Route::post('/kasir-cash',['as' => 'KasirController.kasirCash', 'uses' => 'KasirController@kasirCash']);
    Route::post('/jurnal-import',['as' => 'KasirController.jurnalImport', 'uses' => 'KasirController@jurnalImport']);

    //karyawan
    Route::get('/karyawan-index',['as' => 'KaryawanController.karyawanIndex', 'uses' => 'KaryawanController@karyawanIndex']);
    Route::post('/karyawan-post',['as' => 'KaryawanController.karyawanPost', 'uses' => 'KaryawanController@karyawanPost']);
    Route::post('/karyawan-aktivasi',['as' => 'KaryawanController.karyawanAktivasi', 'uses' => 'KaryawanController@karyawanAktivasi']);

    //Customer
    Route::get('/customer-index',['as' => 'CustomerController.customerIndex', 'uses' => 'CustomerController@customerIndex']);
    Route::post('/customer-post',['as' => 'CustomerController.customerPost', 'uses' => 'CustomerController@customerPost']);
    Route::post('/customer-kirim-email',['as' => 'CustomerController.customerEmail', 'uses' => 'CustomerController@customerEmail']);
    
    //Pesanan
    Route::get('/pesanan-index/{status}',['as' => 'PesananController.pesananindex', 'uses' => 'PesananController@pesananindex']);
    Route::post('/pesanan-selesai',['as' => 'PesananController.pesananKonfirmasi', 'uses' => 'PesananController@pesananKonfirmasi']);
    Route::post('/hapus-pesanan',['as' => 'PesananController.hapusPesanan', 'uses' => 'PesananController@hapusPesanan']);


    //gerai
    Route::get('/gerai-index/{status}',['as' => 'GeraiController.geraiIndex', 'uses' => 'GeraiController@geraiIndex']);
    Route::post('/gerai-post',['as' => 'GeraiController.geraiPost', 'uses' => 'GeraiController@geraiPost']);
    Route::delete('/gerai-hapus',['as' => 'GeraiController.hapusGerai', 'uses' => 'GeraiController@hapusGerai']);
    Route::post('/gerai-aktifasi/{code}',['as' => 'GeraiController.geraiAktifasi', 'uses' => 'GeraiController@geraiAktifasi']);
    Route::post('/gerai-ubah-pin',['as' => 'GeraiController.ubahPinGerai', 'uses' => 'GeraiController@ubahPinGerai']);
    Route::get('/gerai-detail/{id}',['as' => 'GeraiController.geraiDetail', 'uses' => 'GeraiController@geraiDetail']);
    Route::get('/gerai-karyawan/{id}',['as' => 'GeraiController.karyawanGerai', 'uses' => 'GeraiController@karyawanGerai']);
    Route::get('/gerai-pin',['as' => 'GeraiController.geraiPin', 'uses' => 'GeraiController@geraiPin']);
    Route::post('/gerai-tambah-karyawan',['as' => 'GeraiController.tambahKaryawanGerai', 'uses' => 'GeraiController@tambahKaryawanGerai']);
    Route::post('/gerai-edit',['as' => 'GeraiController.geraiEdit', 'uses' => 'GeraiController@geraiEdit']);
    Route::post('/gerai-validasi-transaksi',['as' => 'GeraiController.geraiValidasiTransaksi', 'uses' => 'GeraiController@geraiValidasiTransaksi']);
    Route::get('/check-pin/{pin}',['as' => 'GeraiController.checkPin', 'uses' => 'GeraiController@checkPin']);
    // Route::get('/users',['as' => 'GeraiController.users', 'uses' => 'GeraiController@users']);

    //menu
    Route::get('/menu-index',['as' => 'MenuController.index', 'uses' => 'MenuController@index']);
    Route::get('/komposisi/{id}',['as' => 'MenuController.getKomposisiMenu', 'uses' => 'MenuController@getKomposisiMenu']);
    Route::post('/menu-post',['as' => 'MenuController.menuPost', 'uses' => 'MenuController@menuPost']);
    Route::post('/menu-edit',['as' => 'MenuController.editMenu', 'uses' => 'MenuController@editMenu']);
    Route::post('/menu-tambah-kategori',['as' => 'MenuController.tambahKategori', 'uses' => 'MenuController@tambahKategori']);
    Route::delete('/menu-hapus',['as' => 'MenuController.deleteMenu', 'uses' => 'MenuController@deleteMenu']);

    //inventory
    Route::get('/inventory-index',['as' => 'InventoryController.inventoryIndex', 'uses' => 'InventoryController@inventoryIndex']);
    Route::post('/inventory-post',['as' => 'InventoryController.inventoryPost', 'uses' => 'InventoryController@inventoryPost']);
    Route::post('/aktivasi-inventory',['as' => 'InventoryController.aktivasiInventory', 'uses' => 'InventoryController@aktivasiInventory']);

    //absensi
    Route::get('/absensi-index',['as' => 'AbsensiController.absensiIndex', 'uses' => 'AbsensiController@absensiIndex']);
    Route::get('/absensi-export/{id}',['as' => 'AbsensiController.exportAbsensi', 'uses' => 'AbsensiController@exportAbsensi']);
    Route::post('/absensi-post',['as' => 'AbsensiController.absensiPost', 'uses' => 'AbsensiController@absensiPost']);

    //laporan
    Route::get('/laporan-index',['as' => 'LaporanController.index', 'uses' => 'LaporanController@index']);
    Route::get('/laporan-api',['as' => 'LaporanController.laporanApi', 'uses' => 'LaporanController@laporanApi']);
});