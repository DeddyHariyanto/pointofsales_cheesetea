@extends('layouts.app')

@section('sidebar')
@include('layouts.sidebar.sidebar')
@endsection

@section('header')
@include('layouts.header.header')
@endsection

@section('content')
<style>
	/* for handle in place autocomplate in modal bostrap */
.pac-container {
        z-index: 1051 !important;
    }
</style>
<style>
#map {
	height: 400px;
	/* The height is 400 pixels */
	width: 100%;
	/* The width is the width of the web page */
}
#mapKu {
	height: 400px;
	/* The height is 400 pixels */
	width: 100%;
	/* The width is the width of the web page */
}

#description {
	font-family: Roboto;
	font-size: 15px;
	font-weight: 300;
}

#infowindow-content .title {
font-weight: bold;
}

#infowindow-content {
display: none;
}

#map #infowindow-content {
display: inline;
}

.pac-card {
background-color: #fff;
border: 0;
border-radius: 2px;
box-shadow: 0 1px 4px -1px rgba(0, 0, 0, 0.3);
margin: 10px;
padding: 0 0.5em;
font: 400 18px Roboto, Arial, sans-serif;
overflow: hidden;
font-family: Roboto;
padding: 0;
}

#pac-container {
padding-bottom: 12px;
margin-right: 12px;
}

.pac-controls {
display: inline-block;
padding: 5px 11px;
}

.pac-controls label {
font-family: Roboto;
font-size: 13px;
font-weight: 300;
}

#pac-input {
background-color: #fff;
font-family: Roboto;
font-size: 15px;
font-weight: 300;
margin-left: 12px;
padding: 0 11px 0 13px;
text-overflow: ellipsis;
width: 400px;
}

#pac-input:focus {
border-color: #4d90fe;
}

#title {
color: #fff;
background-color: #4d90fe;
font-size: 25px;
font-weight: 500;
padding: 6px 12px;

}
</style>
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-fluid">
	@if (session('error'))
<div class="alert alert-danger">{{ session('error') }}</div>
@endif
	<div class="content flex-row-fluid" id="kt_content">
		<div class="row justify-content-center">
			<div class="col-xl-12">
				{{-- <div class="row mb-5">
					<div class="col-12">
						<label>Autocomplete search</label>
						<div style="display: none">
							<input type="radio" name="type" id="changetype-address" checked="checked"/>
							<label for="changetype-address">address</label>
						</div>
						<div id="strict-bounds-selector" class="pac-controls" style="display: none">
							<input type="checkbox" id="use-location-bias" value="" checked />
							<label for="use-location-bias">Bias to map viewport</label>
						</div>
						<input class="form-control" id="alamatKu" type="text" placeholder="Enter a location" />
					</div>
				</div>
				<div class="row mb-5">
					<div class="col-md-12">
						<div id="map" class="map-canvas" data-menara="lokasi" lok-lat="-1.2564674" lok-lng="116.874169" style="height: 300px"></div>
						<div id="infowindow-content">
							<span id="place-name" class="title"></span><br />
							<span id="place-address"></span>
						</div>
					</div>
				</div> --}}
				<div class="row justify-content-center gy-0 gx-10">
					<div class="col-xl-12 mb-4">
						<div class="toolbar d-flex flex-stack mb-0" id="kt_toolbar">
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack flex-wrap">
								<div class="page-title d-flex flex-column me-3">
									<div class="card-toolbar">
										<ul class="nav">
											<li class="nav-item">
												<a class="nav-link btn btn-sm btn-color-muted btn-active btn-active-light-primary fw-bolder px-4 me-1 {{ $status === "aktif" ? "active" : "" }}" href="/gerai-index/aktif">Gerai Aktif</a>
											</li>
											<li class="nav-item">
												<a class="nav-link btn btn-sm btn-color-muted btn-active btn-active-light-primary fw-bolder px-4 me-1 {{ $status === "nonaktif" ? "active" : "" }}" href="/gerai-index/nonaktif">Gerai Non Aktif</a>
											</li>
										</ul>
									</div>
								</div>
								<div class="d-flex align-items-center py-2">
									<a href="#modal_tambah" data-bs-toggle="modal" class="btn btn-warning text-dark" id="-1.2564674||116.874169" onclick="setTambah(this.id)"><i class="fas fa-plus me-2 text-dark"></i>Tambah Gerai</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-12 mb-4">
						<table id="kt_datatable_example_1" class="table table-row-bordered gy-5">
							<thead>
								<tr class="fw-bold fs-7 text-muted">
									<th>#</th>
									<th>Gerai</th>
									<th>Karyawan</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
                @foreach ($gerais as $key => $gerai)
                  <tr>
										<td class="align-middle text-muted">#{{$gerai->id}}</td>
										<td>
											<div class="d-flex align-items-sm-center">
												<div class="symbol symbol-50px me-5">
													<span class="symbol-label">
														<img src="{{asset('assets')}}/media/cheestea/icon.png" class="h-50 align-self-center" alt="">
													</span>
												</div>
												<div class="d-flex align-items-center flex-row-fluid flex-wrap">
													<div class="flex-grow-1 me-2">
														<a href="#" class="text-gray-800 text-hover-primary fs-5 fw-bolder">{{$gerai->nama_gerai}}</a>
														<span class="text-muted fw-bold d-block fs-6">{{$gerai->lokasi_gerai}}</span>
														<span class="badge {{ $status === "aktif" ? "badge-light-success" : "badge-light-danger" }} fw-bold me-1 fs-6">{{$gerai->status_gerai}}</span>
													</div>
												</div>
											</div>
										</td>
										<td>
                      @php
                        $karyawans = App\UserGerai::where('id_gerai',$gerai->id)
                        ->join('users','users.id','=','user_gerais.id_user')
                        ->get();
                      @endphp
                      @foreach ($karyawans as $key => $karyawan)
					  @if ($karyawan->status_user_gerai == 'aktif')
					  	<span class="badge badge-light-primary fw-bold me-1 fs-6">{{$karyawan->nama}}</span>
					@else
					  	<span class="badge badge-light-danger fw-bold me-1 fs-6">{{$karyawan->nama}}</span>
					  @endif
                        <br>
                      @endforeach
										</td>
										<td>
											<button class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-overflow="true">
												<span class="svg-icon svg-icon-1">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4" fill="black" />
														<rect x="11" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
														<rect x="15" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
														<rect x="7" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
													</svg>
												</span>
											</button>
											<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px" data-kt-menu="true">
												<div class="menu-item px-3">
													<div class="menu-content fs-6 text-dark fw-bolder px-3 py-4">Aksi Cepat</div>
												</div>


												<div class="separator mb-3 opacity-75"></div>

												<div class="menu-item px-3">
													<a href="/gerai-pin" target="_blank" class="menu-link px-3">Kasir</a>
												</div>

												<div class="menu-item px-3">
													<a href="/gerai-detail/{{$gerai->id}}" class="menu-link px-3">Detail</a>
													<a href="#modal_tambah" data-bs-toggle="modal" class="menu-link px-3" id="{{$gerai->id}}||{{$gerai->nama_gerai}}||{{$gerai->lokasi_gerai}}||{{$gerai->lat_lng_gerai}}||{{$gerai->pin_gerai}}" onclick="setEdit(this.id)">Edit</a>
													<a href="#modalHapusDataKaryawan" data-bs-toggle="modal" class="menu-link px-3" id="{{$gerai->id}}" onclick="setHapus(this.id)">Hapus</a>
													<a href="#modalUbahPinDataKaryawan" data-bs-toggle="modal" class="menu-link px-3" id="{{$gerai->id}}||{{$gerai->pin_gerai}}" onclick="setUbahPin(this.id)">Ubah Pin</a>
													<a href="#modalTambahKaryawan" data-bs-toggle="modal" class="menu-link px-3" id="{{$gerai->id}}" onclick="setTambahKaryawan(this.id)">Tambah Karyawan</a>
												</div>


												<div class="separator mt-3 opacity-75"></div>
												@if ($gerai->status_gerai == 'nonaktif')
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<div class="menu-content px-3 py-3">
															<a href="#modal-aktifvasi" data-bs-toggle="modal" class="menu-link px-3" id="{{$gerai->id}}||{{$gerai->nama_gerai}}||{{1}}" onclick="setAktif(this.id)">Aktifkan Gerai</a>
														</div>
													</div>
													<!--end::Menu item-->
												@elseif($gerai->status_gerai == 'aktif')
													<!--begin::Menu item-->
													<div class="menu-item px-3">
														<div class="menu-content px-3 py-3">
															<a href="#modal-aktifvasi" data-bs-toggle="modal" class="menu-link px-3" id="{{$gerai->id}}||{{$gerai->nama_gerai}}||{{0}}" onclick="setNonAktif(this.id)">Non Aktifkan Gerai</a>
														</div>
													</div>
													<!--end::Menu item-->
												@endif
													{{-- <div class="menu-item px-3">
														<div class="menu-content px-3 py-3">
															<form method="post" action="{{ $status === "aktif" ? "/gerai-aktifasi/$gerai->id||0" : "/gerai-aktifasi/$gerai->id||1" }}" autocomplete="off" enctype="multipart/form-data">@csrf @method('post')
																<button type="submit" class="btn {{ $status === "aktif" ? "btn-danger" : "btn-success" }} btn-sm px-4">
																{{ $status === "aktif" ? "Nonaktifkan Gerai" : "Akktifkan Gerai" }}
																</button>
															</form>
														</div>
													</div> --}}

											</div>
										</td>
									</tr>
                @endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@include('gerai.modal.modal-edit-data')
@include('gerai.modal.modal-tambah-karyawan')
@include('gerai.modal.modal-ubah-pin-data')
@include('gerai.modal.modal-hapus-data')
@include('gerai.modal.modal-aktivasi')
@include('gerai.modal.modal-tambah-data')

<script async
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1MgLuZuyqR_OGY3ob3M52N46TDBRI_9k&libraries=places&callback=initMap">
</script>
<script type="text/javascript">	
function setEdit(id) {
	let idku = id.split('||');
	let code = 2;
	let latlng = idku[3].split('/');
	let lat = latlng[0];
	let lng = latlng[1];
	$('.title').html('Edit Gerai');
	$('.tambahgerai').hide(500);
	$(".form_tambah_gerai").attr("action", '/gerai-edit');
	$('.data-id-gerai').val(idku[0]);
	$('.data-nama-gerai').val(idku[1]);
	$('.data-alamat-gerai').val(idku[2]);
	$('.data-alamat-pin').val(idku[4]);
	$('.lat').val(latlng[0]);
	$('.lng').val(latlng[1]);
	initMap(code, lat, lng);
}
function setTambah(id) {
	let idku = id.split('||');
	let code = 1;
	let lat = idku[0];
	let lng = idku[1];
	$('.title').html('Tambah Gerai');
	$('.tambahgerai').show(500);
	$(".form_tambah_gerai").attr("action", '/gerai-post');
	initMap(code, lat, lng);
}
function setNonAktif(id) {
	let idku = id.split('||');
	// let codeKu = '/gerai-aktifasi/'+idku[2]+'';
	$(".form-aktivasi").attr("action", '../gerai-aktifasi/'+idku[2]+'');
	$('.title').html('Non Aktivasi')
	$('.pesan').html('Apakah Anda Yakin Menonaktifkan '+ idku[1] +' ini ?')
	$('.idGerai').val(idku[0]);
}
function setAktif(id) {
	let idku = id.split('||');
	$(".form-aktivasi").attr("action", '../gerai-aktifasi/'+idku[2]+'');
	$('.title').html('Aktivasi')
	$('.pesan').html('Apakah Anda Yakin Mengaktifkan '+ idku[1] +' ini ?')
	$('.idGerai').val(idku[0]);
}

function setHapus(id) {
	console.log(id);
	$('.hapusGerai').val(id);
}
function setUbahPin(id) {
	let idku = id.split('||');
	$('.id').val(idku[0]);
	$('.masterpin').val(idku[1]);
}

	$(document).ready(function() {
		$("#kt_datatable_example_2").DataTable({
			"scrollY": "170px",
			"scrollCollapse": true,
			"paging": false,
			"dom": "<'table-responsive'tr>"
		});
		$('#kt_datatable_example_1').DataTable( {
			"language": {
				"lengthMenu": "Show _MENU_",
			},
			"dom":
			"<'row'" +
			"<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
			"<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
			">" +

			"<'table-responsive'tr>" +

			"<'row'" +
			"<'col-sm-12 col-xl-5 d-flex align-items-center justify-content-center justify-content-xl-start'i>" +
			"<'col-sm-12 col-xl-7 d-flex align-items-center justify-content-center justify-content-xl-end'p>" +
			">"
		} );
	});
</script>
@endsection
