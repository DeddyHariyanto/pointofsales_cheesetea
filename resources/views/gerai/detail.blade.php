@extends('layouts.app')

@section('sidebar')
@include('layouts.sidebar.sidebar')
@endsection

@section('header')
@include('layouts.header.header')
@endsection

@section('content')
@if (session('error'))
<div class="alert alert-danger">{{ session('error') }}</div>
@endif
	<div class="toolbar d-flex flex-stack mb-0 mb-lg-n4 pt-5" id="kt_toolbar">
						<div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
							<div class="page-title d-flex flex-column me-3">
								<h1 class="d-flex text-dark fw-bolder my-1 fs-3">{{$data['nama_gerai']}}</h1>
							</div>
						</div>
					</div>
	<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
							<!--begin::Post-->
							<div class="content flex-row-fluid" id="kt_content">
								<!--begin::Row-->
								<div class="row g-5 g-xl-10 mb-xl-10">
									<div class="col-md-6 col-lg-6 col-xl-6 col-xxl-3 mb-md-5 mb-xl-10">
										{{-- ini untuk card penjualan produk hari ini --}}
										<div class="card card-flush h-md-50 mb-5 mb-xl-10">
											<div class="card-header pt-5">
												<div class="card-title d-flex flex-column">
													<div class="d-flex align-items-center">
														<span class="fs-4 fw-bold text-gray-400 me-1 align-self-start">Rp</span>
														<span class="fs-2hx fw-bolder text-dark me-2 lh-1">{{number_format($data['totalPlusDiscount'])}}</span>
														<span class="badge badge-success fs-6 lh-1 py-1 px-2 d-flex flex-center" style="height: 22px">
														<!--end::Svg Icon-->Setelah Diskon</span>
													</div>
													<span class="text-gray-400 pt-1 fw-bold fs-6">Penjualan Produk hari ini</span>
												</div>
											</div>
											<div class="card-body pt-2 pb-4 d-flex align-items-center">
												<div class="d-flex flex-center me-5 pt-2">
													<canvas id="penjualan_produk_hari_ini_chart" style="min-width: 50px; min-height: 50px" ></canvas>
												</div>
												<div class="d-flex flex-column content-justify-center w-100">
													<div class="d-flex fs-6 fw-bold align-items-center">
														<div class="bullet w-8px h-6px rounded-2 bg-primary me-3"></div>
														<div class="text-gray-500 flex-grow-1 me-4">{{$data['minuman']}} Minuman</div>
														<div class="fw-boldest text-gray-700 text-xxl-end">Rp.{{number_format($data['totalMinumanDiscount'],0)}}</div>
														<input type="hidden" class="minum" value="{{$data['minuman']}}">
													</div>
													<div class="d-flex fs-6 fw-bold align-items-center my-3">
														<div class="bullet w-8px h-6px rounded-2 bg-danger me-3"></div>
														<div class="text-gray-500 flex-grow-1 me-4">{{$data['makanan']}} Makanan</div>
														<div class="fw-boldest text-gray-700 text-xxl-end">Rp.{{number_format($data['totalMakananDiscount'],0)}}</div>
														<input type="hidden" class="makan" value="{{$data['makanan']}}">
													</div>
													<div class="d-flex fs-6 fw-bold align-items-center">
														<div class="bullet w-8px h-6px rounded-2 bg-success me-3"></div>
														<div class="text-gray-500 flex-grow-1 me-4">{{$data['addOn']}} Add on</div>
														<div class="fw-boldest text-gray-700 text-xxl-end">Rp.{{number_format($data['totalAddOnDiscount'],0)}}</div>
														<input type="hidden" class="add" value="{{$data['addOn']}}">
													</div>
												</div>
												
											</div>
										</div>
										{{-- end untuk card penjualan produk hari ini --}}
										{{-- absen karyawan gerai --}}
										<div class="card card-flush h-md-50 mb-5 mb-xl-10">
											<div class="card-header align-items-center border-0">
												<h3 class="fw-bolder text-gray-900 m-0">Absen Karyawan Gerai</h3>
												<button class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-overflow="true">
													<span class="svg-icon svg-icon-1">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
															<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4" fill="black" />
															<rect x="11" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
															<rect x="15" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
															<rect x="7" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
														</svg>
													</span>
												</button>
												<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px" data-kt-menu="true">
													<div class="menu-item px-3">
														<div class="menu-content fs-6 text-dark fw-bolder px-3 py-4">Quick Actions</div>
													</div>
													<div class="separator mb-3 opacity-75"></div>
													<div class="menu-item px-3">
														<a href="../absensi-export/{{$data['id']}}" class="menu-link px-3">Tarik Absen Sebulan (Excel)</a>
													</div>
												</div>
											</div>
											<div class="card-body pt-2">
												<ul class="nav nav-pills nav-pills-custom mb-3">
													{{-- @php
														dd($data['userGerai']);
													@endphp --}}
													@for ($i = 0; $i < count($data['userGerai']); $i++)
														<li class="nav-item mb-3 me-3 me-lg-6">
															<a class="nav-link d-flex justify-content-between flex-column flex-center overflow-hidden active w-80px h-85px py-4" data-bs-toggle="pill" href="#{{$data['userGerai'][$i]['id']}}">
																<div class="nav-icon">
																	<img alt="" src="../{{('berkas')}}/karyawan/{{$data['userGerai'][$i]['foto_user']}}" class="" />
																</div>
																<span class="nav-text text-gray-700 fw-bolder fs-6 lh-1">{{explode(' ',$data['userGerai'][$i]['nama'])[0]}}</span>
																<span class="bullet-custom position-absolute bottom-0 w-100 h-4px bg-primary"></span>
															</a>
														</li>
													@endfor
												</ul>

												<div class="tab-content">
													
														<div class="tab-pane fade show active" id="">
															<div class="table-responsive">
																<table class="table table-row-dashed align-middle gs-0 gy-4 my-0">
																	<thead>
																		<tr class="fs-7 fw-bolder text-gray-500 border-bottom-0">
																			<th class="ps-0 w-50px">#</th>
																			<th class="min-w-140px">Nama</th>
																			<th class="min-w-140px">Status</th>
																			<th class="min-w-140px">Waktu</th>
																		</tr>
																	</thead>
																	<tbody>
																		@for ($i = 0; $i < count($data['userGerai']); $i++)
																			@for ($x = 0; $x < count($data['userGerai'][$i]['absensi']); $x++)
																			<tr>
																				<td>{{$x+1}}</td>
																				<td>
																					<span class="nav-text text-gray-700 fw-bolder fs-6 lh-1">{{explode(' ',$data['userGerai'][$i]['nama'])[0]}}</span>
																				</td>
																				<td>
																					<span class="text-gray-800 fw-bolder text-end">{{$data['userGerai'][$i]['absensi'][$x]['status_absensi']}} {{$x+1}}</span>
																				</td>
																				<td>
																					<span class="text-gray-800 fw-bolder d-block fs-6">{{$data['userGerai'][$i]['absensi'][$x]['waktu_absensi']}}</span>
																				</td>
																			</tr>
																			@endfor
																		@endfor
																	</tbody>
																</table>
															</div>
														</div>
													{{-- @endfor --}}
												</div>
											</div>
										</div>
										{{-- end absen karyawan gerai --}}
									</div>
									<div class="col-md-6 col-lg-6 col-xl-6 col-xxl-3 mb-md-5 mb-xl-10">
										<div class="card card-flush h-md-50 mb-5 mb-xl-10">
											<div class="card-header pt-5">
												<div class="card-title d-flex flex-column">
													<div class="d-flex align-items-center">
														<span class="fs-2hx fw-bolder text-dark me-2 lh-1">{{$data['total_transaksi']}}</span>
														<input type="hidden" class="total_transaksi" value="{{$data['total_transaksi']}}">
														<input type="hidden" class="langganan" value="{{$data['langganan']}}">
														<span class="badge badge-success fs-6 lh-1 py-1 px-2 d-flex flex-center" style="height: 22px">Transaksi</span>
													</div>
													<span class="text-gray-400 pt-1 fw-bold fs-6">(Daily)Jumlah Transaksi VS Jumlah Customer Terinput</span>
												</div>
											</div>
											<div class="card-body pt-2 pb-4 d-flex align-items-center">
												<div class="d-flex flex-center me-5 pt-2">
													<canvas id="jumlah_pelanggan_dan_input_chart" style="min-width: 25px; min-height: 25px"></canvas>
												</div>
											</div>
										</div>
										<div class="card card-flush h-md-50 mb-xl-10">
											<div class="card-header pt-5">
												<div class="card-title d-flex flex-column">
													<span class="fs-2hx fw-bolder text-dark me-2 lh-1">Menu Terlaris</span>
													<span class="text-gray-400 pt-1 fw-bold fs-6">5 Menu Terlaris</span>
												</div>
											</div>
											<div class="card-body">
													<!--begin::Item-->
													@for ($i = 0; $i < count($data['topSales']); $i++)
														<div class="d-flex flex-stack">
															<div class="d-flex align-items-center me-3">
																<img src="../{{('berkas')}}/menu/{{$data['topSales'][$i]['foto']}}" class="me-3 w-30px" alt="" />
																<div class="flex-grow-1">
																	<a href="#" class="text-gray-800 text-hover-primary fs-5 fw-bolder lh-0">{{$data['topSales'][$i]['nama']}}</a>
																	<span class="text-gray-400 fw-bold d-block fs-6">Terjual {{$data['topSales'][$i]['jumlah']}} dari menu lainnya</span>
																</div>
															</div>
															<div class="d-flex align-items-center w-100 mw-125px">
																<div class="progress h-6px w-100 me-2 bg-light-success">
																	<div class="progress-bar bg-success" role="progressbar" style="width: {{$data['topSales'][$i]['jumlah']}}%" aria-valuenow="{{$data['topSales'][$i]['jumlah']}}" aria-valuemin="0" aria-valuemax="1000"></div>
																</div>
																<span class="text-gray-400 fw-bold">1000</span>
															</div>
														</div>
														<div class="separator separator-dashed my-4"></div>
													@endfor

												</div>
										</div>
									</div>

									<div class="col-lg-12 col-xl-12 col-xxl-6 mb-5 mb-xl-0">
										<div class="card card-flush overflow-hidden h-md-100">
												<div class="card-header pt-7">
													<h3 class="card-title align-items-start flex-column">
														<span class="card-label fw-bolder text-dark">Transaksi Hari ini</span>
														<span class="text-gray-400 mt-1 fw-bold fs-6">Rp. {{number_format($data['totalPlusDiscount'],0)}}</span>
													</h3>
													<button class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-overflow="true">
														<span class="svg-icon svg-icon-1">
															<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4" fill="black" />
																<rect x="11" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
																<rect x="15" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
																<rect x="7" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
															</svg>
														</span>
													</button>
													<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px" data-kt-menu="true">
														<div class="menu-item px-3">
															<div class="menu-content fs-6 text-dark fw-bolder px-3 py-4">Quick Actions</div>
														</div>
														<div class="separator mb-3 opacity-75"></div>
														<div class="menu-item px-3">
															<a href="../kasir-export-all/{{$data['id']}}" class="menu-link px-3">Tarik Semua Transaksi (Excel)</a>
														</div>
														<div class="menu-item px-3">
															<a href="../kasir-export-hari-ini/{{$data['id']}}" class="menu-link px-3">Tarik Transaksi Hari Ini (Excel)</a>
														</div>
														<div class="menu-item px-3">
															<a href="../kasir-export-bulan-ini/{{$data['id']}}" class="menu-link px-3">Tarik Transaksi Bulan ini (Excel)</a>
														</div>
														<div class="menu-item px-3">
															<a href="../jurnal-export-bulan-ini/{{$data['id']}}" class="menu-link px-3">Tarik Jurnal Bulan ini (Excel)</a>
														</div>
														<div class="menu-item px-3" >
															<div class="menu-item px-3">
																<a href="#modalImportJurnal" data-bs-toggle="modal" class="menu-link px-3" ></i>Validasi Jurnal Bulan Ini (upload)</a>
															</div>
														</div>
														<div class="menu-item px-3 validasi" style="display: none">
															<form action="/gerai-validasi-transaksi" method="post" id="input-validasi"enctype="multipart/form-data">
																@csrf
																<input type="hidden" name="allCheckBox" class="allCheckBox" id="">
																<input type="hidden" name="selectCheckBox" class="selectCheckBox" id="">
																<a type="submit"class="menu-link px-3" onclick="document.getElementById('input-validasi').submit();"">Setujui Jurnal (checked)</a>
																{{-- <button type="submit"class="menu-link px-3">Setujui Jurnal (checked)</button> --}}
															</form>
														</div>
													</div>
												</div>
												<div class="card-body py-3">
									<!--begin::Table container-->
									<div class="table-responsive">
										
										<!--begin::Table-->
										<table id="kt_datatable_example_1" class="table table-row-bordered gy-5">
											<thead>
												<tr class="fw-bolder text-muted">
													<th class="w-25px">
														<div class="form-check form-check-sm form-check-custom form-check-solid">
															<input class="form-check-input" id="checkAll" type="checkbox" value="1" data-kt-check="true" data-kt-check-target=".widget-13-check" />
														</div>
													</th>
													<th class="min-w-150px">Order Id</th>
													<th class="min-w-140px">Karyawan</th>
													<th class="min-w-120px">Date</th>
													<th class="min-w-120px">Pembayaran</th>
													<th class="min-w-120px">Total</th>
													<th class="min-w-120px">Status</th>
													<th class="min-w-100px text-end">Actions</th>
												</tr>
											</thead>
											<!--end::Table head-->
											<!--begin::Table body-->
											<tbody>
												@for ($i=0; $i < count($data['struk']); $i++)
													<tr>
														<td>
															<div class="form-check form-check-sm form-check-custom form-check-solid">
																<input class="form-check-input widget-13-check checkedStatus" type="checkbox" name="transaksi_id[]" value="{{$data['struk'][$i]['id']}}" id="data{{$data['struk'][$i]['id']}}" />
															</div>
														</td>
														<td>
															<a href="#" class="text-dark fw-bolder text-hover-primary fs-6">#{{$data['struk'][$i]['code']}}</a>
														</td>
														<td>
															<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">{{$data['struk'][$i]['kasir']}}</span>
														</td>
														<td>
															<a href="#" class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">{{$data['struk'][$i]['tanggal']}}</a>
															@if ($data['struk'][$i]['tipe_bayar'] == 1)
																<span class="text-muted fw-bold text-muted d-block fs-7">Cash</span>
															@elseif ($data['struk'][$i]['tipe_bayar'] == 2)
																<span class="text-muted fw-bold text-muted d-block fs-7">E-money</span>
															@elseif ($data['struk'][$i]['tipe_bayar'] == 3)
																<span class="text-muted fw-bold text-muted d-block fs-7">Debit</span>
															@elseif ($data['struk'][$i]['tipe_bayar'] == 4)
																<span class="text-muted fw-bold text-muted d-block fs-7">Cash In Tunai</span>
															@elseif ($data['struk'][$i]['tipe_bayar'] == 5)
																<span class="text-muted fw-bold text-muted d-block fs-7">Cash In Debit</span>
															@elseif ($data['struk'][$i]['tipe_bayar'] == 6)
																<span class="text-muted fw-bold text-muted d-block fs-7">Cash Out Tunai</span>
															@elseif ($data['struk'][$i]['tipe_bayar'] == 7)
																<span class="text-muted fw-bold text-muted d-block fs-7">Cash Out Debit</span>
															@endif
														</td>
														<td>
															@if ($data['struk'][$i]['tipe_bayar'] == 1)
																<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">Cash</span>
															@elseif ($data['struk'][$i]['tipe_bayar'] == 2)
																<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">E-money</span>
															@elseif ($data['struk'][$i]['tipe_bayar'] == 3)
																<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">Debit</span>
															@elseif ($data['struk'][$i]['tipe_bayar'] == 4)
																<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">Cash In Tunai</span>
															@elseif ($data['struk'][$i]['tipe_bayar'] == 5)
																<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">Cash In Debit</span>
															@elseif ($data['struk'][$i]['tipe_bayar'] == 6)
																<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">Cash Out Tunai</span>
															@elseif ($data['struk'][$i]['tipe_bayar'] == 7)
																<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">Cash Out Debit</span>
															@endif
															
															<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">Discount {{$data['struk'][$i]['discount']}}% : {{number_format($data['struk'][$i]['discount']/100 * $data['struk'][$i]['total_dibayar'],0)}}</span>
														</td>
														<td class="text-dark fw-bolder text-hover-primary fs-6">
															{{-- total : {{number_format($data['struk'][$i]['total_dibayar'],0)}}<br> --}}
															{{-- Total Bayar :  --}}
															Rp. {{number_format($data['struk'][$i]['total_dibayar'] - ($data['struk'][$i]['discount']/100 * $data['struk'][$i]['total_dibayar']),0)}}<br>
															{{-- Tunai : {{number_format($data['struk'][$i]['jumlah_bayar'],0)}}<br> --}}
														</td>
														<td>
															@if ($data['struk'][$i]['status'] == 'valid')
																<span class="badge badge-light-success">{{$data['struk'][$i]['status']}}</span>
															@elseif($data['struk'][$i]['status'] == 'invalid')
																<span class="badge badge-light-danger">{{$data['struk'][$i]['status']}}</span>
															@elseif($data['struk'][$i]['status'] == 'selesai')
																<span class="badge badge-light-info">{{$data['struk'][$i]['status']}}</span>
															@elseif($data['struk'][$i]['status'] == 'batal')
																<span class="badge badge-light-danger">{{$data['struk'][$i]['status']}}</span>
															@endif
														</td>
														<td class="text-end">
															@if ($data['struk'][$i]['tipe_bayar'] >= 1 && $data['struk'][$i]['tipe_bayar'] <= 3)
																
																@php
																	$dis = 0;
																	if($data['struk'][$i]['discount'] != null){
																		$dis = $data['struk'][$i]['discount'];
																	}else{
																		$dis = 0;
																	}

																	$item = DB::table('detail_transaksis')
																				->leftJoin('menus', 'detail_transaksis.id_menu','=','menus.id')
																				->where('id_transaksi', $data['struk'][$i]['id'])
																				->groupBy('detail_transaksis.id_menu')
																				->select('detail_transaksis.id','menus.foto_menu','detail_transaksis.nama','detail_transaksis.harga',DB::raw('count(detail_transaksis.id_menu) as qty'))
																				->get();
																	// App\detailTransaksi::where('id_transaksi', $data['struk'][$i]['id'])
																	// 		->groupBy('id_menu')
																	// 		->select('id','nama','harga',DB::raw('count(id_menu) as qty'))
																	// 		->get();
																@endphp
																<button  class="showDetailRow btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1" id="{{$item}}||{{$dis}}||{{$data['struk'][$i]['total_dibayar']}}||{{$data['struk'][$i]['jumlah_bayar']}}">
																	<i class="fas fa-angle-double-down"></i>
																</button>
															@endif
														</td>
													</tr>
													{{-- child row --}}

													{{-- end child row --}}
												@endfor
											</tbody>
										</table>
									</div>
								</div>
										</div>
									</div>

									<div class="col-lg-12 col-xl-12 col-xxl-6 mb-5 mb-xl-0">
										<div class="card card-flush overflow-hidden h-md-100">
												<div class="card-header pt-7">
													<h3 class="card-title align-items-start flex-column">
														<span class="card-label fw-bolder text-dark">List Karyawan</span>
														<span class="text-gray-400 mt-1 fw-bold fs-6">karyawan yang bekerja di gerai</span>
													</h3>

													<div class="card-toolbar" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="Klik untuk menambahkan, menghapus dan Menambahkan">
														<a href="#modalTambahKaryawan" class="btn btn-sm btn-light btn-active-primary" data-bs-toggle="modal" id="{{$data['id']}}" onclick="setTambahKaryawan(this.id)">
															{{-- <i class="fas fa-plus"></i> --}}
														Option Karyawan</a>
													</div>

												</div>
												<div class="card-body py-3">
									<!--begin::Table container-->
									<div class="table-responsive">
										<!--begin::Table-->
										<table class="table table-row-bordered gy-5 datatable_general">
											<thead>
												<tr class="fw-bolder text-muted">
													<th class="min-w-150px">Nama Karyawan</th>
													<th class="min-w-120px">Penjualan</th>
													<th class="min-w-100px text-end">Actions</th>
												</tr>
											</thead>
											<!--end::Table head-->
											<!--begin::Table body-->
											<tbody>
												@for ($i = 0; $i < count($data['userGerai']); $i++)
												<tr>
													<td>
														<div class="d-flex align-items-center">
															<div class="symbol symbol-45px me-5">
																<img src="../{{('berkas')}}/karyawan/{{$data['userGerai'][$i]['foto_user']}}" alt="" />
															</div>
															<div class="d-flex justify-content-start flex-column">
																<a href="#" class="text-dark fw-bolder text-hover-primary fs-6">{{$data['userGerai'][$i]['nama']}}</a>
																<span class="text-muted fw-bold text-muted d-block fs-7">{{$data['userGerai'][$i]['level']}}</span>
															</div>
														</div>
													</td>
													<td class="text-end">
														@php
															$penjualan = DB::table('transaksis')
																		->where('transaksis.id_user_transaksi', $data['userGerai'][$i]['id'])
																		->whereDate('transaksis.created_at','=',Carbon\Carbon::today())
																		->select(
																			'transaksis.total_dibayar_transaksi as total_dibayar',
																			'transaksis.discount_transaksi'
																		)->get();
															// dd($penjualan);
															$totalPenjualan = 0; 
														@endphp
														
														@foreach ($penjualan as $penjualans)
														@php
															$totalPenjualan += $penjualans->total_dibayar - ($penjualans->discount_transaksi/100 * $penjualans->total_dibayar);
														@endphp
														@endforeach
														<div class="d-flex flex-column w-100 me-2">
															<div class="d-flex flex-stack mb-2">
																<span class="me-2 fs-7 fw-bold">Rp.{{number_format($totalPenjualan,0)}}</span>
															</div>
														</div>
													</td>
													<td>
														<div class="d-flex justify-content-end flex-shrink-0">
															{{-- <a href="#modalUbahDataKaryawan" data-bs-toggle="modal" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="Klik untuk mengubah data">
																<i class="fas fa-user-edit"></i>
															</a> --}}
															{{-- <a href="#modalHapusDataKaryawan" data-bs-toggle="modal" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="Klik untuk menghapus karyawan">
																<i class="fas fa-trash"></i>
															</a> --}}
														</div>
													</td>
												</tr>
											@endfor
												
											</tbody>
										</table>
									</div>
									</div>
										</div>
									</div>

							<div class="row gy-5 g-xl-10">
								<div class="col-xl-4">
									<div class="card card-flush h-xl-100">
										<div class="card-header pt-7 mb-3">
											<h3 class="card-title align-items-start flex-column">
												<span class="card-label fw-bolder text-gray-800">Stock Terpakai</span>
												<span class="text-gray-400 mt-1 fw-bold fs-6">{{count($data['inven'])}} stock paling sedikit Minggu ini</span>
											</h3>
										</div>
										<div class="card-body pt-4">
										@for ($i = 0; $i < count($data['inven']); $i++)
												<div class="d-flex flex-stack">
														<div class="d-flex align-items-center me-5">
															<div class="me-5">
																<a href="#" class="text-gray-800 fw-bolder text-hover-primary fs-6">{{$data['inven'][$i]['nama']}}</a>
																<span class="text-gray-400 fw-bold fs-7 d-block text-start ps-0">{{$data['inven'][$i]['qty']}} {{$data['inven'][$i]['satuan']}} Terpakai</span>
															</div>
														</div>
													<div class="text-gray-400 fw-bolder fs-7 text-end">
														<span class="text-gray-800 fw-bolder fs-6 d-block">{{$data['inven'][$i]['qty']}} {{$data['inven'][$i]['satuan']}}</span>
													</div>
												</div>
											<div class="separator separator-dashed my-5"></div>
										@endfor

											<div class="text-center pt-9">
												{{-- <a href="../../demo18/dist/apps/ecommerce/catalog/add-product.html" class="btn btn-primary">Tambah Stok</a> --}}
												<a href="#kt_modal_create_app" data-bs-toggle="modal" class="btn btn-primary text-dark" id="kt_toolbar_primary_button"></i>Tambah Stok</a>
											</div>
										</div>
									</div>
								</div>


								<div class="col-xl-8">
									<div class="card card-flush h-xl-100">
										<div class="card-header pt-7">
											<h3 class="card-title align-items-start flex-column">
												<span class="card-label fw-bolder text-dark">Stock Report</span>
												<span class="text-gray-400 mt-1 fw-bold fs-6">Total {{count($data['dataStock'])}} Items in the Stock</span>
											</h3>
										</div>
										<div class="card-body">
											<table class="table align-middle table-row-dashed fs-6 gy-3 datatable_general">
												<thead>
													<tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
														<th class="min-w-100px">Item</th>
														<th class="text-center pe-3 min-w-100px">Kategori & <br> Lokasi</th>
														<th class="text-end pe-3 min-w-150px">Date Added</th>
														<th class="text-end pe-3 min-w-100px">Price</th>
														<th class="text-end pe-3 min-w-50px">Status</th>
														<th class="text-end pe-0 min-w-25px">Qty</th>
													</tr>
												</thead>
												<tbody class="fw-bolder text-gray-600">
													@for ($i = 0; $i < count($data['dataStock']); $i++)
														<tr>
															<td>
																<a href="#" class="text-dark text-hover-primary">{{$data['dataStock'][$i]['brand_inventory']}}</a>
															</td>
															<td class="text-center">
																{{$data['dataStock'][$i]['kategori']}}<br>
																<span class="badge py-3 px-4 fs-7 badge-light-info">{{$data['dataStock'][$i]['lokasi']}}</span>
															</td>
															<td class="text-end">{{$data['dataStock'][$i]['created_at']}}</td>
															<td class="text-end">{{$data['dataStock'][$i]['harga_inventory']}}</td>
															<td class="text-end">
																<span class="badge py-3 px-4 fs-7 badge-light-primary">{{$data['dataStock'][$i]['status_inventory']}}</span>
															</td>
															<td class="text-end" data-order="58">
																<span class="text-dark fw-bolder">{{$data['dataStock'][$i]['kuantitas_inventory']}} {{$data['dataStock'][$i]['satuan_inventory']}}</span>
															</td>
														</tr>
													@endfor
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>


								</div>
							</div>
						</div>

@include('gerai.modal.modal-import-jurnal')
@include('gerai.modal.modal-tambah-stock')
@include('gerai.modal.modal-tambah-karyawan')
@include('gerai.modal.modal-ubah-data-karyawan')
@include('gerai.modal.modal-hapus-data-karyawan')

<script type="text/javascript">
$('#checkAll').on('click', function() {
  if($(this).prop("checked") == true){
    $('.checkedStatus').prop('checked', true);
    var dos = [];
      $(".checkedStatus").each(function(i){
          dos[i] = $(this).val();
    });
    // console.log(dos);
    var loc2 = dos.filter(function (el) {
                return el != '';
              });
    $('.allCheckBox').val(loc2);
    console.log(loc2);
      $('.validasi').show();
  }else {
      $('.checkedStatus').prop('checked', false);
      $('.validasi').hide();
  }
});

$('.checkedStatus').on('click', function() {
    var ary = [];
    $("input[name='transaksi_id[]']:checked").each(function(i){
        ary[i] = $(this).val();
    });
    // console.log(ary.length);
    console.log(ary);
    $('.selectCheckBox').val(ary);
    if ($(this).prop("checked") != true && ary.length == 0) {
            $('.validasi').hide();
            $('#checkAll').prop('checked', false);
    }else {
        $('.validasi').show();
    }
});
	$(document).ready(function() {
    function childRowKosong(){
        return 'tidak ada komposisi';
    }
    function childRow (data,discount,total_dibayar,jumlah_bayar) {
        var isi = '';
        for (var i = 0; i < data.length; i++) {
            isi +='<div class="d-flex align-items-center">'+
						'<div class="symbol symbol-45px me-5">'+
							'<img src="../berkas/menu/'+ data[i]['foto_menu'] +'" class="" alt="" />'+
						'</div>'+
						'<div class="d-flex justify-content-start flex-column">'+
							'<div class="me-5">'+
								'<a href="#" class="text-gray-800 fw-bolder text-hover-primary fs-6">'+data[i]['nama']+'</a>'+
								'<span class="text-muted fw-bold text-muted d-block fs-7">Rp. '+data[i]['harga']+'</span>'+
							'<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">'+data[i]['qty']+'x</span>'+
							'</div>'+
						'</div>'+
						'<div class="text-gray-400 fw-bolder fs-7 text-end">'+
							'<span class="text-gray-800 fw-bolder fs-6 d-block">Rp. '+(data[i]['qty'] * data[i]['harga'])+'</span>'+
						'</div>'+
					'</div>'+
					'<div class="separator separator-dashed my-5"></div>'
        }
		if (discount != 0) {
			isi +='<div class="d-flex align-items-center">'+
					'<div class="d-flex justify-content-start flex-column">'+
						'<div class="me-5">'+
							'<a href="#" class="text-gray-800 fw-bolder text-hover-primary fs-6">Discount '+ discount +'%</a>'+
						// '<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">'+data[i]['qty']+'x</span>'+
						'</div>'+
					'</div>'+
					'<div class="text-gray-400 fw-bolder fs-7 text-end">'+
						'<span class="text-gray-800 fw-bolder fs-6 d-block">Rp. '+ (discount/100 * total_dibayar)+'</span>'+
					'</div>'+
				'</div>'+
				'<div class="separator separator-dashed my-5"></div>'
		}
		isi +='<div class="d-flex align-items-center">'+
				'<div class="d-flex justify-content-start flex-column">'+
					'<div class="me-5">'+
						'<a href="#" class="text-gray-800 fw-bolder text-hover-primary fs-6">Total</a>'+
					// '<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">'+data[i]['qty']+'x</span>'+
					'</div>'+
				'</div>'+
				'<div class="text-gray-400 fw-bolder fs-7 text-end">'+
					'<span class="text-gray-800 fw-bolder fs-6 d-block">Rp. '+ (total_dibayar - (discount/100 * total_dibayar))+'</span>'+
				'</div>'+
			'</div>'+
			'<div class="separator separator-dashed my-5"></div>'+
			'<div class="d-flex align-items-center">'+
				'<div class="d-flex justify-content-start flex-column">'+
					'<div class="me-5">'+
						'<a href="#" class="text-gray-800 fw-bolder text-hover-primary fs-6">Tunai</a>'+
					// '<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">'+data[i]['qty']+'x</span>'+
					'</div>'+
				'</div>'+
				'<div class="text-gray-400 fw-bolder fs-7 text-end">'+
					'<span class="text-gray-800 fw-bolder fs-6 d-block">Rp. '+ jumlah_bayar +'</span>'+
				'</div>'+
			'</div>'+
			'<div class="separator separator-dashed my-5"></div>'
			
        return 	'<table class="table table-row-bordered gy-5">'+
        '<tbody>'+
            '<tr>'+
                '<td></td>'+
                '<td>'+
                isi+
                '</td>'+
                '<td></td>'+
                '<td></td>'+
                '<td></td>'+
                '<td></td>'+
                '<td></td>'+
                '<td></td>'+
            '</tr>'+
            '</tbody>'+
            '</table>';
    }
    var table = $('#kt_datatable_example_1').DataTable( {
        "language": {
            "lengthMenu": "Show _MENU_",
        },
        "dom":
        "<'row'" +
        "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
        "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
        ">" +

        "<'table-responsive'tr>" +

        "<'row'" +
        "<'col-sm-12 col-xl-5 d-flex align-items-center justify-content-center justify-content-xl-start'i>" +
        "<'col-sm-12 col-xl-7 d-flex align-items-center justify-content-center justify-content-xl-end'p>" +
        ">"
    } );
    // Add event listener for opening and closing details
$('#kt_datatable_example_1 tbody').on('click', '.showDetailRow', function () {
        var data = JSON.parse($(this).attr('id').split("||")[0]);
        var discount = JSON.parse($(this).attr('id').split("||")[1]);
		var total_dibayar = JSON.parse($(this).attr('id').split("||")[2]);
        var jumlah_bayar = JSON.parse($(this).attr('id').split("||")[3]);
        console.log(data);
    var tr = $(this).closest('tr');
    var row = table.row( tr );

    if ( row.child.isShown() ) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
    } else {
        // Open this row
        if (data.length == 0) {
            row.child( childRowKosong() ).show();
            tr.addClass('shown');
        }else {
            row.child( childRow(data,discount,total_dibayar,jumlah_bayar) ).show();
            tr.addClass('shown');
        }

    }
} );
});
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
<script type="text/javascript">
let minum = $('.minum').val();
let makan = $('.makan').val();
let add = $('.add').val();
var ctx = document.getElementById("penjualan_produk_hari_ini_chart");
// tampilan chart
var piechart = new Chart(ctx,{type: 'doughnut',
          data : {
			  labels: [
				    'Minuman',
				    'Makanan',
				    'Add On',
				],
        datasets: [{
          // Jumlah Value yang ditampilkan
		  data:[minum,makan,add],
		  
          backgroundColor:[
			  'rgba(0, 0, 255, 0.5)',
                 'rgba(255, 0, 0, 0.5)',
                 'rgba(0, 255, 0, 0.5)',
				]
			}],
        }
        });

		let total_transaksi = $('.total_transaksi').val();
		let langganan = $('.langganan').val();
		var ctx = document.getElementById("jumlah_pelanggan_dan_input_chart");
        // tampilan chart
        var piechart = new Chart(ctx,{type: 'pie',
          data : {
						labels: [
				    'Transaksi',
				    'Berlangganan',
				  ],
        datasets: [{
          // Jumlah Value yang ditampilkan
           data:[total_transaksi,langganan],

          backgroundColor:[
                 'rgba(0, 0, 255, 0.5)',
                 'rgba(255, 0, 0, 0.5)',
                 ]
        }],
        }
        });
	
</script>

@endsection
