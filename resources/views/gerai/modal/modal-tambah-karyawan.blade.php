<div class="modal fade" id="modalTambahKaryawan" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <form class="form" action="/gerai-tambah-karyawan" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-header" id="kt_tambah_data_header">
                            <h3 class="modal-title">Tambah Karyawan Gerai</h3>
                            <div class="btn btn-sm btn-icon btn-active-warning btn-color-muted" data-bs-dismiss="modal">
                                <i class="fas fa-times"></i>
                            </div>
                        </div>
                        @php
                            $users = App\User::where('id','!=',1)
                                                ->where('status_user','aktif')
                                                ->where('level','karyawan')
                                                ->select('id','nama')->get();
                        @endphp
                        
                        <input type="hidden" name="id" class="id">
                        <div class="modal-body py-10 px-lg-17">
                            <!--begin::Scroll-->
                            <div class="scroll-y me-n7 pe-7" id="kt_tambah_data_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_tambah_data_header" data-kt-scroll-wrappers="#kt_tambah_data_scroll" data-kt-scroll-offset="300px">
                                
                                {{-- <div class="notice d-flex bg-light-warning rounded border-warning border border-dashed mb-9 p-6">
                                    <span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
                                            <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black" />
                                            <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black" />
                                        </svg>
                                    </span>
                                    <div class="d-flex flex-stack flex-grow-1">
                                        <div class="fw-bold">
                                            <h5 class="text-gray-900 fw-bolder mb-0">Perhatian</h5>
                                            <div class="fs-6 text-gray-700">1. Gerai Anda Akan bersifat Aktif ketika ditambahkan</div>
                                            <div class="fs-6 text-gray-700">2. Karyawan dapat ditugaskan lewat fitur ini namun tidak diwajibkan</div>
                                            <div class="fs-6 text-gray-700">3. Karyawan yang diinput melalui fitur ini akan diberikan PIN yang berbeda secara otomatis</div>
                                            <div class="fs-6 text-gray-700">4. Master PIN adalah milik owner dan berbeda dengan PIN milik Karyawan</div>
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="existingKaryawan"></div>
                                <br>
                                <div class="newKaryawan"></div>
                                {{-- <div class ="row mb-5">
                                    <div class="col-md-5 fv-row">
                                        <label class="fs-5 fw-bold mb-2">Karyawan</label>
                                        <select class="form-select form-select-solid" name="karyawanNew[]" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true">
                                            <option value=""></option>
                                            @foreach ($users as $karyawan)
                                                <option value="{{$karyawan->id}}">{{$karyawan->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-5 fv-row">
                                        <label class="fs-5 fw-bold mb-2">PIN</label>
                                        <select class="form-select form-select-solid pin_new" name="pinKaryawanNew[]" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="col-md-2 fv-row">
                                        <a href="#!" class="btn_tambah_karyawan"><i class="fas fa-plus text-success me-2"></i></a>
                                        {{-- <a href="#!" class="remove" ><i class="fas fa-trash text-danger"></i></a> --}}
                                    {{-- </div>
                                </div> --}}
                                <div class="new-karyawan"></div>
                             </div>
                        </div>
                        <div class="modal-footer flex-center">
                            <button type="reset" id="kt_tambah_data_cancel" class="btn btn-light me-3">Batal</button>
                            <button type="submit" id="kt_tambah_data_submit" class="btn btn-warning">
                                <span class="indicator-label text-dark">Submit</span>
                                <span class="indicator-progress">Please wait...
                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script>
// let option = '';
//     // let suggestionPin = [];
//     for (let i = 0; i < 15; i++) {
//         let suggestionPin = Math.floor(100000 + Math.random() * 900000);
//         option += '<option value="'+ suggestionPin +'">'+ suggestionPin +'</option>';
//     }
// $('.pin_new').html(option);

function setTambahKaryawan(id) {
	console.log(id);
	$('.id').val(id);
	fetch('../gerai-karyawan/'+id+'', {
				method: 'get'
	}).then(response => response.json())
	.then(data => {
		let komposisi = '';
		for (let i = 0; i < data.length; i++) {
            komposisi += '<div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid modal-bayarKu" id="modal-bayarKu"></div>'+
                            '<div class ="row mb-5">'+
							'<div class="col-md-5 fv-row">'+
								'<label class="fs-5 fw-bold mb-2">Karyawan</label>'+
								'<input type="hidden" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" name="user_gerai_id[]" value="'+data[i]['id']+'"/>'+
								'<select class="form-select form-select-solid pin_new1" name="karyawanExisting[]" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true">'+
									'<option value="'+data[i]['user_id']+'">'+data[i]['name']+'</option>'+
									'@foreach ($users as $karyawan)'+
										'<option value="{{$karyawan->id}}">{{$karyawan->nama}}</option>'+
									'@endforeach'+
								'</select>'+
							'</div>'+
							'<div class="col-md-5 fv-row">'+
								'<label class="fs-5 fw-bold mb-2">PIN</label>'+
                                '<select class="form-select form-select-solid pin_new1" name="pin[]" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true">'+
                                    '<option value="'+data[i]['pin']+'">'+data[i]['pin']+'</option>'
                                    for (let x = 0; x < 15; x++) {
                                        let suggestionPin = Math.floor(100000 + Math.random() * 900000);
                                        komposisi += '<option value="'+ suggestionPin +'">'+ suggestionPin +'</option>';
                                    }
                                komposisi +='</select>'+
                                
							'</div>'+
                            '<div class="col-md-2 fv-row">'+
								'<label class="fs-5 fw-bold mb-2">Karyawan</label>'+
								'<select class="form-select form-select-solid" name="status[]" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true">'+
									'<option value="'+data[i]['status']+'">'+data[i]['status']+'</option>';
                                    if (data[i]['status'] == 'aktif') {
                                        komposisi +='<option value="nonaktif">nonaktif</option>';
                                    }else if(data[i]['status'] == 'nonaktif'){
                                        komposisi +='<option value="aktif">aktif</option>';
                                    }
                    komposisi +='</select>'+
							'</div>'+
							// '<div class="col-md-2 fv-row">'+
							// 	'<a href="#!" class="btn_tambah_komposisi"><i class="fas fa-plus text-success me-2"></i></a>'+
							// 	'<a href="#!" class="remove" ><i class="fas fa-trash text-danger"></i></a>'+
							// '</div>'+
						'</div>';
		}
        let NewKaryawan = '<div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid untukBaru" id="modal-bayarKu"></div>'+
                            '<div class ="row mb-5">'+
							'<div class="col-md-5 fv-row">'+
								'<label class="fs-5 fw-bold mb-2">Karyawan</label>'+
								'<select class="form-select form-select-solid pin_new" name="karyawanNew[]" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true">'+
									'<option value=""></option>'+
									'@foreach ($users as $karyawan)'+
										'<option value="{{$karyawan->id}}">{{$karyawan->nama}}</option>'+
									'@endforeach'+
								'</select>'+
							'</div>'+
							'<div class="col-md-5 fv-row">'+
								'<label class="fs-5 fw-bold mb-2">PIN</label>'+
                                '<select class="form-select form-select-solid pin_new" name="pinKaryawanNew[]" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true">'+
                                    '<option value=""></option>'
                                    for (let x = 0; x < 15; x++) {
                                        let suggestionPin = Math.floor(100000 + Math.random() * 900000);
                                        NewKaryawan += '<option value="'+ suggestionPin +'">'+ suggestionPin +'</option>';
                                    }
                                NewKaryawan +='</select>'+
                                
							'</div>'+
							'<div class="col-md-2 fv-row">'+
								'<a href="#!" class="btn_tambah_karyawan"><i class="fas fa-plus text-success me-2"></i></a>'+
								// '<a href="#!" class="remove" ><i class="fas fa-trash text-danger"></i></a>'+
							'</div>'+
						'</div>';
            
        
		$('.existingKaryawan').html(komposisi);
		// $('.pin_new1').select2({
        //     tags: true,
		// 	dropdownParent: $(".modal-bayarKu"),
        // });
		$('.newKaryawan').html(NewKaryawan);
		// $('.pin_new').select2({
        //     tags: true,
		// 	dropdownParent: $(".untukBaru"),
        // });
	}).catch(err => console.log(err));
}

function getRndInteger(min, max) {
    console.log(min);
  let random =  Math.floor(100000 + Math.random() * 900000);
  $('.pinBaru').val(random);
}

let byKar = 1;
    tambahKompisi(byKar);
    function tambahKompisi(number) {
        // let pilih = 'selectpicker';
        let newKaryawan = '<div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid untukBaru1" id="modal-bayarKu"></div>'+
                            '<div class ="row mb-5">'+
							'<div class="col-md-5 fv-row">'+
								'<label class="fs-5 fw-bold mb-2">Karyawan</label>'+
								'<select class="form-select form-select-solid pin_new_multi" name="karyawanNew[]" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true">'+
									'<option value=""></option>'+
									'@foreach ($users as $karyawan)'+
										'<option value="{{$karyawan->id}}">{{$karyawan->nama}}</option>'+
									'@endforeach'+
								'</select>'+
							'</div>'+
							'<div class="col-md-5 fv-row">'+
								'<label class="fs-5 fw-bold mb-2">PIN</label>'+
                                '<select class="form-select form-select-solid pin_new_multi" name="pinKaryawanNew[]" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true">'+
                                    '<option value=""></option>'
                                    for (let x = 0; x < 15; x++) {
                                        let suggestionPin = Math.floor(100000 + Math.random() * 900000);
                                        newKaryawan += '<option value="'+ suggestionPin +'">'+ suggestionPin +'</option>';
                                    }
                                newKaryawan +='</select>'+
                                
							'</div>'+
							'<div class="col-md-2 fv-row">'+
								'<a href="#!" class="btn_tambah_karyawan"><i class="fas fa-plus text-success me-2"></i></a>'+
								'<a href="#!" class="remove" ><i class="fas fa-trash text-danger"></i></a>'+
							'</div>'+
						'</div>';
        if (number > 1) {
            $('.new-karyawan').append(newKaryawan);
        }
    }
    $(document).on('click', '.btn_tambah_karyawan', function() {
        byKar++;
        tambahKompisi(byKar);
        // $('.pilih').selectpicker();
        // $('.pin_new_multi').select2({
        //     tags: true,
		// 	dropdownParent: $(".untukBaru1"),
        // });
    });
    $(document).on('click', '.remove', function(){
        byKar--;
        $(this).closest("#tambahKariawan").remove();
    });
</script>
