<div class="modal fade pac-container" id="modal_edit" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <form class="form" action="/gerai-post" method="POST" enctype="multipart/form-data" id="kt_tambah_data_form" autocomplete="off">
                @csrf
                <div class="modal-header" id="kt_tambah_data_header">
                    <h3 class="modal-title">Edit Data</h3>
                    <div class="btn btn-sm btn-icon btn-active-warning btn-color-muted" data-bs-dismiss="modal">
                        <i class="fas fa-times"></i>
                    </div>
                </div>
                <div class="modal-body py-10 px-lg-17">
                    <!--begin::Scroll-->
                    <div class="scroll-y me-n7 pe-7" id="kt_tambah_data_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_tambah_data_header" data-kt-scroll-wrappers="#kt_tambah_data_scroll" data-kt-scroll-offset="300px">
                        <div class="notice d-flex bg-light-warning rounded border-warning border border-dashed mb-9 p-6">
                            <span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
                                    <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black" />
                                    <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black" />
                                </svg>
                            </span>
                            <div class="d-flex flex-stack flex-grow-1">
                                <div class="fw-bold">
                                    <h5 class="text-gray-900 fw-bolder mb-0">Perhatian</h5>
                                    <div class="fs-6 text-gray-700">1. Gerai Anda Akan bersifat Aktif ketika ditambahkan</div>
                                    <div class="fs-6 text-gray-700">2. Karyawan dapat ditugaskan lewat fitur ini namun tidak diwajibkan</div>
                                    <div class="fs-6 text-gray-700">3. Karyawan yang diinput melalui fitur ini akan diberikan PIN yang berbeda secara otomatis</div>
                                    <div class="fs-6 text-gray-700">4. Master PIN adalah milik owner dan berbeda dengan PIN milik Karyawan</div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-5">
                            <div class="col-md-6 fv-row">
                                <label class="required fs-5 fw-bold mb-2">Nama Gerai</label>
                                <input type="text" class="form-control data-id-gerai" placeholder="" name="id" required/>
                                <input type="text" class="form-control data-nama-gerai" placeholder="" name="nama_gerai" required/>
                            </div>
                        </div>
                        <div class="row mb-5">
                            <div class="col-12">
                                <label class="required fs-5 fw-bold mb-2">Alamat</label>
                                <textarea class="form-control data-alamat-gerai" name="alamat" rows="5" cols="90" required></textarea>
                            </div>
                        </div>
                        {{-- <div class="row mb-5">
                            <div class="col-12">
                                <label>Search</label>
                                <div style="display: none">
                                    <input
                                        type="radio"
                                        name="type"
                                        id="changetype-all"
                                        checked="checked"
                                    />
                                    <label for="changetype-all">All</label>
                                </div>
                                <div id="strict-bounds-selector" class="pac-controls" style="display: none">
                                    <input type="checkbox" id="use-location-bias" value="" checked />
                                    <label for="use-location-bias">Bias to map viewport</label>
                                </div>
                                <input class="form-control" id="alamatKu" type="text" placeholder="Enter a location" />
                            </div>
                        </div>
                        <div class="row mb-5">
                            <div class="col-md-12">
                                <div id="infowindow-content">
                                    <span id="place-name" class="title"></span><br />
                                    <span id="place-address"></span>
                                </div>
                            </div>
                        </div> --}}
                        <div id="mapKu" class="map-canvas" data-menara="lokasi" lok-lat="-1.2564674" lok-lng="116.874169" style="height: 300px"></div>
                            <input type="text" class="data-lat-gerai">
                            <input type="text" class="data-lng-gerai">
                        </div>
                </div>
                <div class="modal-footer flex-center">
                    <button type="reset" id="kt_tambah_data_cancel" class="btn btn-light me-3">Batal</button>
                    {{-- <button type="submit" id="kt_tambah_data_submit" class="btn btn-warning"> --}}
                    <button type="submit" id="simpan" class="btn btn-warning">
                        <span class="indicator-label text-dark">Tambah</span>
                        <span class="indicator-progress">Please wait...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<script async
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1MgLuZuyqR_OGY3ob3M52N46TDBRI_9k&libraries=places&callback=Maps">
</script>
<script>
    
</script>