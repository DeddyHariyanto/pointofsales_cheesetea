@extends('layouts.app')

@section('sidebar')
@include('layouts.sidebar.sidebar')
@endsection

@section('header')
@include('layouts.header.header')
@endsection

@section('content')
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-fluid">
	<div class="content flex-row-fluid" id="kt_content">
		<div class="row justify-content-center">
			<div class="col-xl-12">
				<div class="row justify-content-center gy-0 gx-10">
					<div class="col-xl-12 mb-4">
						<div class="toolbar d-flex flex-stack mb-0" id="kt_toolbar">
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack flex-wrap">
								<div class="page-title d-flex flex-column me-3">
									<h1 class="d-flex text-dark fw-bolder my-1 fs-3">Absensi</h1>

								</div>
								<div class="d-flex align-items-center py-2">
									{{-- <a href="#modal_tambah" data-bs-toggle="modal" class="btn btn-warning text-dark createButton" id="kt_toolbar_primary_button"><i class="fas fa-plus me-2 text-dark"></i>Tambah Data</a>
									<a href="#" class="btn btn-light-primary" data-bs-toggle="modal" data-bs-target="#modal-absen"><span class="bullet bullet-dot bg-danger h-6px w-6px translate-middle animation-blink"></span><i class="fas fa-clock me-2"></i>Clock In</a> --}}
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-12 mb-4">
						<table id="kt_datatable_example_1" class="table table-row-bordered gy-5">
										<thead>
											<tr class="text-start text-muted fw-bolder fs-7 text-uppercase gs-0">
												<th class="min-w-125px">Nama </th>
												<th class="min-w-125px">Waktu</th>
												<th class="min-w-125px">Lokasi</th>
												<th class="text-end min-w-100px">Status Absensi</th>
											</tr>
										</thead>
										<tbody class="text-gray-600 fw-bold">
											@foreach ($data as $key => $datas)
												<tr>
													<td class="d-flex align-items-center">
														<div class="symbol symbol-circle symbol-50px overflow-hidden me-3">
															<a>
																<div class="symbol-label">
																	<img src="assets/media/svg/avatars/blank.svg" class="w-100" />
																</div>
															</a>
														</div>
														<div class="d-flex flex-column">
															<a class="text-gray-800 text-hover-primary mb-1">{{$datas->nama}}</a>
															{{-- <span>{{$datas->email}}</span> --}}
														</div>
													</td>
													<td>{{$datas->waktu_absensi}}</td>
													<td>{{$datas->lokasi}}</td>
													{{-- <td>
														<div class="badge badge-light fw-bolder">Yesterday</div>
													</td> --}}
													<td>
														<div class="badge @if ($datas->status_absensi =="check-in") badge-light-success @else badge-light-danger @endif fw-bolder">{{$datas->status_absensi}}</div>
													</td>
												</tr>
											@endforeach
										</tbody>
									</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
</script>

@endsection
