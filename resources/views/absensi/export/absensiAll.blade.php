
          <table class="table align-items-center table-flush table-hover" id="dataTableHover">
            <thead class="thead-light">
              <tr>
                  <th>no</th>
                  <th>Nama</th>
                  <th>gerai</th>
                  <th>lokasi_absensi</th>
                  <th>tanggal</th>
                  <th>waktu</th>
                  <th>status</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($data as $key => $datas)
                <tr>
                  <td>{{$key +1}}</td>
                  <td>{{$datas->nama}}</td>
                  <td>{{$datas->nama_gerai}}</td>
                  <td>{{$datas->lokasi}}</td>
                  <td>{{$datas->tanggal_absensi}}</td>
                  <td>{{$datas->waktu_absensi}}</td>
                  <td>{{$datas->status_absensi}}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
