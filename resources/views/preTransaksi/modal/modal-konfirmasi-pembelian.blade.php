<div class="modal fade" tabindex="-1" id="modalKonfirmasiPembelian">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header py-3">
                <h5 class="modal-title">Detail Pesanan</h5>
                <div class="btn btn-icon btn-sm btn-active-light-warning ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times"></i>
                </div>
            </div>
            <div class="modal-body scroll-y mx-5 mx-xl-18 pt-0">
                <div class="mb-5 mt-5">
                    <span class="text-gray-600 fs-6 fw-bold"><i class="fas fa-file me-2"></i>Order #<b class="text-dark no_pesanan"></b></span>
                    <br>
                    <span class="text-gray-600 fs-6 fw-bold"><i class="fas fa-calendar-alt me-2"></i>Tanggal : <b class="text-dark tanggal_pesanan"></b></span>
                </div>
                <div class="mb-15">
                    <div class="mh-200px scroll-y me-n7 pe-7" id="checkoutFinal">
                    </div>
                </div>
                <div class="d-flex justify-content-between">
                    <div class="fw-bold">
                        <label class="fs-2 fw-boldest">Total :</label>
                    </div>
                    <br>
                    <span class="badge badge-dark border border-dashed fs-2 fw-boldest text-warning p-3 total_harus_bayar">
                    </div>
                    <div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid" id="modal-bayarKu">
                    <form class="" action="/konfirmasi-pembelian" method="post" enctype="multipart/form-data">
                        @csrf
                      {{-- <div class="inputCheckout">

                      </div> --}}
                      @php
                        $getGerai = '';
                        $userGerai = App\UserGerai::where('id_user', Auth::user()->id)
                                                          ->Where('status_user_gerai', "aktif")
                                                          ->orderby('id','desc')
                                                          ->first();
                        if ($userGerai) {
                            $getGerai = $userGerai->id_gerai;
                        }else{
                            $getGerai = 0;
                        }
                      @endphp
                      <div class="col-12 mt-4 mb-4">
                        <div class="row">
                            <div class="col-xl-12">
                                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                <input type="hidden" name="gerai_id" value="{{$getGerai}}">
                                <input type="hidden" class="idTransaksi" name="idPreTransaksi" >
                                <input type="hidden"  name="code_transaksi" class="code_transaksi">
                                <input type="hidden" class="total_dibayar_customer" name="total_dibayar" >
                                <input type="hidden" class="jumlah_potongan" name="jumlah_potongan" >
                                <label for="diskon" class="form-label">Customer</label>
                                <select class="form-select form-select-solid" name="customer_id" id="customer-new" data-placeholder="format pengisian email" data-allow-clear="true">
                                  <option></option>
                                  @php
                                      $user = App\User::where('level','customer')->get();
                                  @endphp
                                  @foreach ($user as $key => $users)
                                      <option value="{{$users->email}}">{{$users->email}}</option>
                                  @endforeach
                              </select>
                            </div>
                            <div class="col-xl-12 mt-4 tanpaEmail">
                                  <div class="fv-row">
                                      <div class="form-check form-check-custom form-check-solid">
                                          <input class="form-check-input me-3" name="tanpa_struk" type="radio" value="1" id="kt_modal_update_role_option_0" />
                                          <label class="form-check-label" for="kt_modal_update_role_option_0">
                                              <div class="fw-bolder text-gray-800">Tanpa Struk</div>
                                              <div class="text-gray-600">customer tidak dapat menginputkan email dan siap tidak menerima struk transaksi</div>
                                          </label>
                                      </div>
                                  </div>
                              </div>
                            <div class="col-xl-4 mt-4">
                              <label for="diskon" class="required form-label">Jumlah Tunai <span class="fs-9 text-muted"></span></label>
                              <input type="number" pattern="[0-9]{10}"class="form-control jumlah_dibayar" placeholder="" name="jumlah_dibayar" required/>
                          </div>
                            <div class="col-xl-4 mt-4">
                                <label for="diskon" class="required form-label">Metode Pembayaran</label>
                                <select class="form-select tipe_pembayaran" aria-label="Select example" name="tipe_pembayaran" required>
                                    <option></option>
                                    <option value="1">Cash</option>
                                    <option value="2">E-Money</option>
                                    <option value="3">Debit/Credit</option>
                                </select>

                            </div>
                            <div class="col-xl-4 mt-4">
                                <label for="diskon" class="form-label">Diskon <span class="fs-9 text-muted">(%)</span></label>
                                <input type="number" class="form-control discount" placeholder="%" name="discount"/>
                            </div>
                        </div>
                        <div class="row debit" style="display: none">
                          <div class="col-xl-6 mt-6">
                              <label for="diskon" class="form-label">Nama</label>
                              <input type="text" class="form-control" placeholder="A/n Nama Customer" name="nama_rekening"/>
                          </div>
                          <div class="col-xl-6 mt-6">
                              <label for="diskon" class="form-label">No Rekening</label>
                              <input type="text" class="form-control" placeholder="27xxxxxxx" name="no_rekening"/>
                          </div>
                        </div>
                    </div>
                    <div class="modal-footer flex-center border-0">
                        <button type="reset" id="kt_tambah_data_cancel" class="btn btn-light me-3" data-bs-dismiss="modal" aria-label="Close">Discard</button>
                        <button type="submit" id="kt_tambah_data_submit" class="btn btn-warning">
                            <span class="indicator-label text-dark tombol-bayar"></span>
                            <span class="indicator-progress">Please wait...
                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                        </button>
                    </div>
                </form>
                </div>
            </div>

        </div>
<script>
    
    $(document).on('change', '.tipe_pembayaran', function() {
        if ($(this).val() == 3) {
            $('.debit').show(500);
        }else{
            $('.debit').hide(500);
        }
    });
    $(document).ready(function() {
			$("#customer-new").select2({
				tags: true,
				dropdownParent: $("#modal-bayarKu"),
				createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  },
		   templateResult: function (data) {
		    var $result = $("<span></span>");

		    $result.text(data.text);

		    if (data.newOption) {
		      $result.append(" <em>(new)</em>");
		    }

		    return $result;
		  }

		});
        $(document).on('change', '#customer-new', function() {
            if ($(this).val() != '') {
                $('.tanpaEmail').css("display", "none");
                // $('.tanpaEmail').hide(500);
            }else{
                $('.tanpaEmail').show(50);

            }
            
        });
    });

</script>
