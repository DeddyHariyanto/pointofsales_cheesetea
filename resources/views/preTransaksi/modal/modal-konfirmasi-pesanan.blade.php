<div class="modal fade" tabindex="-1" id="modalKonfirmasiPesanan">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header py-3">
                <h5 class="modal-title">Detail Pesanan</h5>
                <div class="btn btn-icon btn-sm btn-active-light-warning ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times"></i>
                </div>
            </div>
            <div class="modal-body scroll-y mx-5 mx-xl-18 pt-0">
                <div class="mb-5 mt-5">
                    <br>
                    <span class="text-gray-600 fs-6 fw-bold"><i class="fas fa-calendar-alt me-2"></i>Tanggal : <b class="text-dark">{{Carbon\Carbon::now('Asia/Makassar')->format('d-m-Y')}}</b></span>
                </div>
                <div class="mb-15">
                    <div class="mh-200px scroll-y me-n7 pe-7" id="checkoutFinal">
                    </div>
                </div>
                <div class="d-flex justify-content-between">
                    <div class="fw-bold">
                        <label class="fs-2 fw-boldest">Total :</label>
                    </div>
                    <br>
                    <span class="badge badge-dark border border-dashed fs-2 fw-boldest text-warning p-3 total_harus_bayar">
                    </div>
                    <br>
                    <div class="notice d-flex bg-light-info rounded border-info border border-dashed mb-9 p-6">
                        <span class="svg-icon svg-icon-2tx svg-icon-info me-4">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
                                <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black" />
                                <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black" />
                            </svg>
                        </span>
                        <div class="d-flex flex-stack flex-grow-1">
                            <div class="fw-bold">
                                <h5 class="text-gray-900 fw-bolder mb-0">Info</h5>
                                <div class="fs-6 text-gray-700">Mohon Untuk Mengingat/Mendownload No.Pesanan agar Mempermudah Memproses Pesanan Anda</div>
                            </div>
                        </div>
                    </div>
                    <div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid" id="modal-bayarKu">
                    <form class="" action="/kasir-pre-post" method="post" enctype="multipart/form-data">
                        @csrf
                      <div class="inputCheckout">

                      </div>
                      <div class="col-12 mt-4 mb-4">
                          <div class="row">
                              <input type="hidden" class="total_dibayar_customer" name="total_dibayar" >
                              <input type="hidden"  name="code_transaksi" value="{{rand(100000, 999999)}}">
                            </div>
                      </div>
                    <div class="modal-footer flex-center border-0">
                        <button type="reset" id="kt_tambah_data_cancel" class="btn btn-light me-3" data-bs-dismiss="modal" aria-label="Close">Discard</button>
                        <button type="submit" id="kt_tambah_data_submit" class="btn btn-warning">
                            <span class="indicator-label text-dark tombol-bayar"></span>
                            <span class="indicator-progress">Please wait...
                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                        </button>
                    </div>
                </form>
                </div>
            </div>

        </div>
<script>
    
    $(document).on('change', '.tipe_pembayaran', function() {
        if ($(this).val() == 3) {
            $('.debit').show(500);
        }else{
            $('.debit').hide(500);
        }
    });
    $(document).ready(function() {
			$("#customer-new").select2({
				tags: true,
				dropdownParent: $("#modal-bayarKu"),
				createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  },
		   templateResult: function (data) {
		    var $result = $("<span></span>");

		    $result.text(data.text);

		    if (data.newOption) {
		      $result.append(" <em>(new)</em>");
		    }

		    return $result;
		  }

		});
        $(document).on('change', '#customer-new', function() {
            if ($(this).val() != '') {
                $('.tanpaEmail').css("display", "none");
                // $('.tanpaEmail').hide(500);
            }else{
                $('.tanpaEmail').show(50);

            }
            
        });
    });

</script>
