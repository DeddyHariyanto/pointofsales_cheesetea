@extends('layouts.app')

@section('sidebar')
@include('layouts.sidebar.sidebar')
@endsection

@section('header')
@include('layouts.header.header')
@endsection

@section('content')
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-fluid">
	<div class="content flex-row-fluid" id="kt_content">
		<div class="row justify-content-center">
			<div class="col-xl-12">
				<div class="row justify-content-center gy-0 gx-10">
					<div class="col-xl-12 mb-4">
						<div class="toolbar d-flex flex-stack mb-0" id="kt_toolbar">
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack flex-wrap">
								<div class="page-title d-flex flex-column me-3">
									<h1 class="d-flex text-dark fw-bolder my-1 fs-3">List Pesanan</h1>
								</div>
								<div class="card-toolbar">
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-12 mb-4">
						<table id="kt_datatable_example_1" class="table table-row-bordered gy-5">
							<thead>
								<tr class="fw-bolder text-muted">
									<th class="min-w-150px">No</th>
									<th class="min-w-150px">Code Pesanan</th>
									<th class="min-w-120px">status</th>
									<th class="min-w-100px text-end">Actions</th>
								</tr>
							</thead>
							<tbody>
								@for ($i = 0; $i < count($data); $i++)
									<tr>
										<td>{{$i + 1}}</td>
										<td>
											<div class="d-flex align-items-center">
												<div class="d-flex justify-content-start flex-column">
													<a href="#" class="text-dark fw-bolder text-hover-primary fs-6">#{{$data[$i]['code']}}</a>
												</div>
											</div>
										</td>
										<td>
											<div class="d-flex align-items-center">
												<div class="d-flex justify-content-start flex-column">
													@if ($data[$i]['status'] == 'order')
														<span class="badge badge-light-warning fw-bold me-1 fs-6">Order</span>
													@elseif($data[$i]['status'] == 'order konfirmasi')
														<span class="badge badge-light-success fw-bold me-1 fs-6">Order telah diKonfirmasi</span>
													@endif
												</div>
											</div>
										</td>
										<td>
											<div class="d-flex justify-content-end flex-shrink-0">
												<div class="menu-item px-3">
													<button class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-overflow="true">
														<span class="svg-icon svg-icon-1">
															<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4" fill="black" />
																<rect x="11" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
																<rect x="15" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
																<rect x="7" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
															</svg>
														</span>
													</button>
													<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px" data-kt-menu="true">
														<div class="menu-item px-3">
															<div class="menu-content fs-6 text-dark fw-bolder px-3 py-4">Quick Actions</div>
														</div>
														<div class="separator mb-3 opacity-75"></div>
															{{-- <div class="menu-item px-3">
																<a href="/kasir-struk/{{$data[$i]['id']}}" target="_blank"class="menu-link px-3" ></i>Struck</a>
															</div> --}}
															@if($data[$i]['status'] == 'order')
																<div class="menu-item px-3">
																	<a href="#modalKonfirmasiPembelian" data-bs-toggle="modal" class="menu-link px-3" id="{{$data[$i]['id']}}" onclick="setKonfirmasiPembelian(this.id)"></i>Konfirmasi Pembelian</a>
																</div>
															@endif
														</div>
												</div>
												{{-- <div class="menu-item px-3">
												@php
													$item = DB::table('pre_detail_transaksis')
																->leftJoin('menus','menus.id','=','pre_detail_transaksis.id_menu')
																// ->leftJoin('komposisis','komposisis.menu_id','=','pre_detail_transaksis.id_menu')
																->where('id_transaksi',$data[$i]['id'])
																->groupBy('pre_detail_transaksis.id_menu')
																->select(
																	'menus.id',
																	'menus.foto_menu',
																	'pre_detail_transaksis.nama',
																	'pre_detail_transaksis.harga',
																	DB::raw('count(pre_detail_transaksis.id_menu) as qty'))->get();
												@endphp
												<button  class="showDetailRow btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1" id="{{$item}}">
													<i class="fas fa-angle-double-down"></i>
												</button>
											</div> --}}

										</td>
									</tr>
								@endfor
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('preTransaksi.modal.modal-konfirmasi-pembelian')
<script>

function setKonfirmasiPembelian(id) {
	
	
	fetch('../get-konfirmasi-pembelian/'+id+'', {
				method: 'get'
			})
	.then(response => response.json())
	.then(data => {
		console.log(data);
		$('.idTransaksi').val(id);
		$('.code_transaksi').val(data['code']);
		$('.no_pesanan').html(data['code']);
		$('.tanggal_pesanan').html(data['tanggal']);
		$(".jumlah_dibayar").attr("placeholder", data['total_dibayar']);
		$(".total_dibayar_customer").val(data['total_dibayar']);
		$(".tombol-bayar").html(''+ new Intl.NumberFormat("id-ID",{ style: "currency", currency: "IDR" }).format(data['total_dibayar']));
		$('.total_harus_bayar').html(new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(data['total_dibayar']));
		var isi_final = '';
		for (let i = 0; i < data['detail'].length; i++) {
			isi_final += '<div class="d-flex flex-stack py-5 border-bottom border-gray-300 border-bottom-dashed">'+
					'<div class="d-flex align-items-center">'+
							'<h5 class="mb-0 fw-boldest">x '+data['detail'][i]['qty']+'</h5>'+
							'<div class="ms-6">'+
									'<span class="d-flex align-items-center fs-5 fw-bolder text-dark">'+data['detail'][i]['nama']+'</span>'+
									'<span class="badge badge-light-primary fs-8 fw-bold">'+new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(data['detail'][i]['harga_satuan'])+'</span>'+
							'</div>'+
					'</div>'+
					'<div class="d-flex">'+
							'<div class="text-end">'+
									'<div class="fs-5 fw-bolder text-dark">'+new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(data['detail'][i]['harga_total'])+'</div>'+
							'</div>'+
					'</div>'+
			'</div>';
		}
		$("#checkoutFinal").html(isi_final);
		$(document).on('keyup', '.discount', function() {
			let dis = ($('.discount').val()/100*data['total_dibayar']);
			$(".total_harus_bayar").html(''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(data['total_dibayar']-dis));
			// $(".total_dibayar_customer").val(data['total_dibayar']-dis);
			$(".jumlah_potongan").val(dis);
			$(".jumlah_dibayar").attr("placeholder", data['total_dibayar']-dis);
			$(".tombol-bayar").html(''+ new Intl.NumberFormat("id-ID",{ style: "currency", currency: "IDR" }).format(data['total_dibayar']-dis));
		});
	}).catch(err => console.log(err))
}
 </script>

@endsection
