<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Struk #{{$data['code']}}</title>
</head>
<style>
    table {
      /* font-family: arial, sans-serif;
      border-collapse: collapse; */
      width: 100%;
    }
    
    td, th {
      /* border: 1px solid #dddddd; */
      text-align: left;
      /* padding: 8px; */
    }
    
    /* tr:nth-child(even) {
      background-color: #dddddd;
    } */
    </style>
<body>
    <p>
       No.Pesanan: <b>#{{$data['code']}}</b><br>
       Tanggal : <b>{{$data['tanggal']}}</b><br>
       ----------------------------------------------
    </p>
    <table>
        <tr>
            <th>No</th>
            <th>menu</th>
            <th>qty</th>
            <th>total</th>
        </tr>
        @for ($i = 0; $i < count($data['detail']); $i++)
        <tr>
            <td>{{$i + 1}}<br>
            <td>{{$data['detail'][$i]['nama']}}<br>Rp. {{number_format($data['detail'][$i]['harga_satuan'],0)}}
            </td>
            <td>{{ $data['detail'][$i]['qty']}}</td>
            <td>
                Rp. {{number_format($data['detail'][$i]['harga_total'],0)}}
            </td>
        </tr>
        @endfor
    </table>
    <p>----------------------------------------------</p>
    <table>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <td>Total</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Rp. {{number_format($data['total_dibayar'],0)}}</td>
        </tr>
        
        </table>
</body>
</html>