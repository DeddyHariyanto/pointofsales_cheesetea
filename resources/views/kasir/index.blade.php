@extends('layouts.app')

@section('sidebar')
@include('layouts.sidebar.sidebar')
@endsection

@section('header')
@include('layouts.header.header')
@endsection

@section('content')
@php
	$no = 0;
	$transaksi = App\transaksi::orderby('id','desc')->first();
	if ($transaksi == null) {
		$no = 1;
	}else{
		$no = $transaksi->id + 1;
	}
@endphp
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-fluid">
	<div class="content flex-row-fluid" id="kt_content">
		<div class="row justify-content-center">
					<div class="col-xl-12">
						<div class="row gy-0 gx-10">
							<div class="col-12">
								<div class="row">
									<div class="col-xl-8 col-6">
										<div class="accordion" id="kt_accordion_1">
											<div class="accordion-item">
												@for ($i = 0; $i < count($data['dataMenu']); $i++)
													<h2 class="accordion-header" id="{{$data['dataMenu'][$i]['kategori']}}_header">
														<button class="accordion-button fs-4 fw-bold" type="button" data-bs-toggle="collapse" data-bs-target="#{{$data['dataMenu'][$i]['kategori']}}_body" aria-expanded="true" aria-controls="{{$data['dataMenu'][$i]['kategori']}}_body">
															{{$data['dataMenu'][$i]['kategori']}}
														</button>
													</h2>
													<div id="{{$data['dataMenu'][$i]['kategori']}}_body" class="accordion-collapse collapse {{ ($i == 0) ? 'show' : ''}}" aria-labelledby="{{$data['dataMenu'][$i]['kategori']}}_header" data-bs-parent="#kt_accordion_1">
														<div class="accordion-body">
															<div class="col-12 mb-3">
																<div class="row">
																	@for ($x = 0; $x < count($data['dataMenu'][$i]['menu']); $x++)
																		<div class="col-xl-3 col-md-4 mb-5">
																			<a href="#!" class="menuButton" id="{{$data['dataMenu'][$i]['menu'][$x]['id']}}||{{$data['dataMenu'][$i]['menu'][$x]['nama_menu']}}||{{$data['dataMenu'][$i]['menu'][$x]['harga_menu']}}">
																				<div class="card-xl-stretch card-rounded border">
																					<div class="card-body">
																						<div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-175px" style="background-image:url('{{('berkas')}}/menu/{{$data['dataMenu'][$i]['menu'][$x]['foto_menu']}}')"></div>
																						<div class="mt-5">
																							<h5 class="fw-bolder text-dark mb-0">{{$data['dataMenu'][$i]['menu'][$x]['nama_menu']}}</h5>
																							<span class="fs-6 fw-bolder text-gray-600">
																							<span class="fs-7 fw-bold text-gray-600">Rp</span> {{number_format($data['dataMenu'][$i]['menu'][$x]['harga_menu'],0)}}</span>
																						</div>
																					</div>
																				</div>
																			</a>
																		</div>
																	@endfor
																</div>
															</div>
														</div>
													</div>
												@endfor
											</div>
										</div>
									</div>
												<div class="col-xl-4 col-6" style="position:fixed; right: 0;">
													<div class="card card-flush py-4 flex-row-fluid">
														<div class="card-body py-3">
															<div class="row">
																<div class="col-12 mb-4">
																	<h2 class="fw-bolder mb-0">Order Details (<b class="text-warning">#{{$no}}</b>)</h2><br>
																	<h5 class="fw-bolder mb-0">Nama Gerai : <b class="text-primary">{{$data['nama_gerai']}}</b></h5>
																	<i class="fas fa-calendar-alt me-2"></i><span class="text-gray-600 fs-7">Tanggal : <b class="text-dark">{{Carbon\Carbon::now('Asia/Makassar')->format('d-m-Y')}}</b></span>
																	<button class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-overflow="true">
																		<span class="svg-icon svg-icon-1">
																			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																				<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4" fill="black" />
																				<rect x="11" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
																				<rect x="15" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
																				<rect x="7" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
																			</svg>
																		</span>
																	</button>
																	<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px" data-kt-menu="true">
																		<div class="menu-item px-3">
																			<div class="menu-content fs-6 text-dark fw-bolder px-3 py-4">Quick Actions</div>
																		</div>
																		<div class="separator mb-3 opacity-75"></div>
																			<div class="menu-item px-3">
																				<a href="#kt_modal_create_app" data-bs-toggle="modal" class="menu-link px-3" id="kt_toolbar_primary_button"></i>Tambah Stok</a>
																			</div>
																			<div class="menu-item px-3">
																				<a href="#kt_modal_cash" data-bs-toggle="modal" class="menu-link px-3" id="kt_toolbar_primary_button"  onclick="setCashIn(1)"></i>Cash In</a>
																			</div>
																			<div class="menu-item px-3">
																				<a href="#kt_modal_cash" data-bs-toggle="modal" class="menu-link px-3" id="kt_toolbar_primary_button" onclick="setCashOut(2)"></i>Cash Out</a>
																			</div>
																		</div>
																		
																	</div>
																</div>
																<div class="col-12">
																	<hr class="mt-2 text-gray-600">
																	<table id="kt_datatable_example_2" class="table table-row-bordered gs-7">
																		<thead>
																			<tr class="fw-bold fs-6 text-gray-800" style="display: none;">
																				<th>Produk</th>
																			</tr>
																		</thead>
																		<tbody id="checkout">



																		</tbody>
																	</table>
																</div>
																<div class="col-12 mt-4 generalBayar" style="display:none">
																	<h5 class="mb-3">Total :</h5>
																	<div class="fs-6 fw-bolder d-flex flex-stack">
																		<span class="badge badge-dark border border-dashed fs-2 fw-boldest text-warning p-3 total_harus_bayar">
																		</span>
																			<a href="#modal-bayar" data-bs-toggle="modal" class="btn btn-warning text-dark"><i class="fas fa-wallet me-2 text-dark"></i>Bayar</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						@include('kasir.modal.modal-tambah-stock')
						@include('kasir.modal.modal-cash')
						@include('kasir.modal.modal-bayar')

						<script type="text/javascript">
						function setCashIn(id) {
							$('.title').html('Cash In');
							$('.form_cash_kasir').attr("action", '../kasir-cash');
							$('.codeCash').val(id);
						}
						function setCashOut(id) {
							$('.title').html('Cash Out');
							$('.form_cash_kasir').attr("action", '../kasir-cash');
							$('.codeCash').val(id);

						}
							$(document).ready(function() {
								$("#kt_datatable_example_2").DataTable({
									"scrollY": "170px",
									"scrollCollapse": true,
									"paging": false,
									"dom": "<'table-responsive'tr>"
								});
								$("#kt_datatable_example_3").DataTable({
									"scrollY": "200px",
									"scrollCollapse": true,
									"paging": false,
									"dom": "<'table-responsive'tr>"
								});
							});

								var checkout=[];var run = 0;
								var indexed = [];var x = 1;

								function index_chekout(tmp,code){
									if (code == 1) {
										//memasukan satuan ke main array isi pesanan
										checkout.push({id:tmp[0],nama_menu:tmp[1],harga_menu:tmp[2]});
										//end memasukan satuan ke main array isi pesanan
									}else if(code==0) {
											var start_index = tmp[3];
											checkout.splice(start_index,1);
									}

									if (checkout.length != 0) {
										//proses menghilangkan duplikasi untuk view
											var jumlah = 0;
											var id = tmp[0];var total_harga_menu = 0;var total_harus_bayar = 0;var index_chekout = 0;
											for (var i = 0; i < checkout.length; i++) {
												if (checkout[i]['id'] == tmp[0]) {
													jumlah += 1;
													nama_menu = tmp[1];
													harga_menu = tmp[2];
													total_harga_menu += Number(tmp[2]);
													index_chekout = i;
												}
											}
											if (code == 1) {
												if ( jumlah==1) {
													indexed.push({id:id,nama_menu:nama_menu,harga_menu:harga_menu,jumlah:jumlah,total_harga_menu:total_harga_menu,index_chekout:index_chekout});
												}else if (jumlah!=1) {
													for (var i = 0; i < indexed.length; i++) {
														if (indexed[i]['id']==id) {
															indexed[i]= {id:id,nama_menu:nama_menu,harga_menu:harga_menu,jumlah:jumlah,total_harga_menu:total_harga_menu,index_chekout:indexed[i]['index_chekout']}
														}
													}
												}

											}else {
												for (var i = 0; i < indexed.length; i++) {
													if (indexed[i]['id']==id) {
														if (indexed[i]['jumlah']>1) {
															indexed[i]= {id:id,nama_menu:nama_menu,harga_menu:harga_menu,jumlah:indexed[i]['jumlah']-1,total_harga_menu:indexed[i]['total_harga_menu']-indexed[i]['harga_menu'],index_chekout:indexed[i]['index_chekout']}
														}else {
															indexed.splice(i,1);
														}
													}
												}
											}
										//end proses menghilangkan duplikasi untuk view
										console.log(indexed.length);
										if (indexed.length==0) {
											checkout=[];
											$('.generalBayar').hide();
										}else {
											$('.generalBayar').show();
										}
									}else {
										indexed = [];
										total_harus_bayar = 0;
										$('.generalBayar').hide();
									}


									var isi = '';var isi_final = '';
									for (var i = indexed.length - 1; i >= 0; i--) {
										isi += '<tr>'+
											'<td>'+
												'<div class="row align-items-center">'+
													'<div class="col-xl-9 col-6">'+
														'<div class="d-flex align-items-center">'+
															'<h5 class="mb-0 fw-bolder">x '+indexed[i]['jumlah']+'</h5>'+
															'<div class="ms-5">'+
																'<h5 class="mb-0">'+indexed[i]['nama_menu']+'</h5>'+
																'<div>'+
																	'<h6 class="mb-0 text-gray-600 fw-bold">'+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(indexed[i]['harga_menu'])+'</h6>'+
																'</div>'+
															'</div>'+
														'</div>'+
													'</div>'+
													'<div class="col-xl-3 col-6 text-center">'+
														'<a href="#!" class="menuButton" id="'+indexed[i]['id']+'||'+indexed[i]['nama_menu']+'||'+indexed[i]['harga_menu']+'"><i class="fas fa-plus text-success me-2"></i></a>'+
														'<a href="#!" class="kurangiPesanan" id="'+indexed[i]['id']+'||'+indexed[i]['nama_menu']+'||'+indexed[i]['harga_menu']+'||'+indexed[i]['index_chekout']+'"><i class="fas fa-minus text-danger"></i></a>'+
													'</div>'+
												'</div>'+
											'</td>'+
										'</tr>';
										isi_final += '<div class="d-flex flex-stack py-5 border-bottom border-gray-300 border-bottom-dashed">'+
												'<div class="d-flex align-items-center">'+
														'<h5 class="mb-0 fw-boldest">x '+indexed[i]['jumlah']+'</h5>'+
														'<div class="ms-6">'+
																'<span class="d-flex align-items-center fs-5 fw-bolder text-dark">'+indexed[i]['nama_menu']+'</span>'+
																'<span class="badge badge-light-primary fs-8 fw-bold">'+new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(indexed[i]['harga_menu'])+'</span>'+
														'</div>'+
												'</div>'+
												'<div class="d-flex">'+
														'<div class="text-end">'+
																'<div class="fs-5 fw-bolder text-dark">'+new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(indexed[i]['total_harga_menu'])+'</div>'+
														'</div>'+
												'</div>'+
										'</div>';
									}
									$("#checkout").html(isi);
									$("#checkoutFinal").html(isi_final);
									for (var i = 0; i < indexed.length; i++) {
										total_harus_bayar += Number(indexed[i]['total_harga_menu']);
									}
									

									// <span class="fs-6 fw-bold text-white">Rp</span> 
									$(".total_harus_bayar").html(''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(total_harus_bayar));
									$(".total_dibayar_customer").val(total_harus_bayar);
									$(".jumlah_dibayar").attr("placeholder", total_harus_bayar);
									$(".tombol-bayar").html(''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(total_harus_bayar));

									$(document).on('keyup', '.discount', function() {
										let dis = ($('.discount').val()/100*total_harus_bayar);
										$(".total_harus_bayar").html(''+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(total_harus_bayar-dis));
										// $(".total_dibayar_customer").val(total_harus_bayar-dis);
										$(".jumlah_potongan").val(dis);
										$(".jumlah_dibayar").attr("placeholder", total_harus_bayar-dis);
										$(".tombol-bayar").html(''+ new Intl.NumberFormat("id-ID",{ style: "currency", currency: "IDR" }).format(total_harus_bayar-dis));
									});
									//dilempar ke form akhir modal yg akan di proses di kontroller
									var inputCheckout = '';
									for (var i = 0; i < checkout.length; i++) {
										console.log(checkout[i]);
										inputCheckout +='<input type="hidden" value="'+checkout[i]['id']+'" name="id_menu[]">'+
                										'<input type="hidden" value="'+checkout[i]['nama_menu']+'" name="nama_menu[]">'+
														'<input type="hidden" value="'+checkout[i]['harga_menu']+'" name="harga_menu[]">';
									}
									$(".inputCheckout").html(inputCheckout);
									//dilempar ke form akhir modal yg akan di proses di kontroller
								}

								//aksi button klik menu
								$(document).on('click', '.menuButton', function(){
									var tmp = $(this).attr('id').split('||');
									index_chekout(tmp,1);
								});
								$(document).on('click', '.kurangiPesanan', function(){
									var tmp = $(this).attr('id').split('||');
									index_chekout(tmp,0);
						    });
							
						</script>

						@endsection
