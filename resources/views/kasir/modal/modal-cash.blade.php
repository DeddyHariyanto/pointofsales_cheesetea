<div class="modal fade" id="kt_modal_cash" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-900px">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="title">Masukan Stock</h2>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body py-lg-10 px-lg-10">
                <div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid" id="kt_modal_cash_stepper">
                    <div class="d-flex justify-content-center justify-content-xl-start flex-row-auto w-100 w-xl-300px">
                        <form class="form_cash_kasir" method="POST" action="{{url('/inventory-post')}}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="codeCash" class="codeCash" value="3">
                            <input type="hidden" name="gerai_id" class="gerai_id" value="{{$data['gerai_id']}}">
                        <div class="w-100">
                            <div class="row mb-5">
                                <div class="col-12">
                                    <label class="required fs-5 fw-bold mb-2">Description</label> 
                                    <textarea class="form-control data-description-kasir" name="description" rows="5" cols="90" required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex-row-fluid py-lg-5 px-lg-15">
                                <div class="w-100">
                                    <div class="row mb-5">
                                        <div class="col-md-12 fv-row mb-5">
                                            <label class="required fs-5 fw-bold mb-2">jumlah nominal</label>
                                            <input type="number" pattern="[0-9]{6}" class="form-control data-nominal-kasir" placeholder="123xxx" maxlength="6" name="jumlah_nominal" id="txtNewPassword" autocomplete="new-password" required/>
            
                                        </div>
                                        <div class="col-md-12 fv-row mb-5">
                                            <label class="required fs-5 fw-bold mb-2">Jenis</label>
                                            <select name="jenis" class="required form-select form-select-solid" data-placeholder="Pilih Satuan Ukuran" data-allow-clear="true" required>
                                                <option value=""></option>
                                                <option value="tunai">Tunai</option>
                                                <option value="debit">debit</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>



                            <div class="d-flex flex-stack pt-10">
                                <div class="me-2">
                                    <button type="reset" id="kt_tambah_data_cancel"  class="btn btn-lg btn-light-primary me-3">
                                        <i class="fas fa-arrow-left"></i>Batal
                                    </button>
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-lg btn-primary">
                                        <span class="indicator-label">Simpan
                                            <i class="fas fa-check"></i>
                                        </span>
                                    </button>
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            <!--end::Actions-->
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Stepper-->
            </div>
            <!--end::Modal body-->
        </div>
        <!--end::Modal content-->
    </div>
    <!--end::Modal dialog-->
</div>
<script type="text/javascript">
$(document).ready(function() {
    $("#kategori").select2({
        tags: true,
        dropdownParent: $("#kt_modal_create_app"),
        createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true
    }
  },
   templateResult: function (data) {
    var $result = $("<span></span>");

    $result.text(data.text);

    if (data.newOption) {
      $result.append(" <em>(new)</em>");
    }

    return $result;
  }

    });
});

</script>
