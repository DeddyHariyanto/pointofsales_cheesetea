
          <table class="table align-items-center table-flush table-hover" id="dataTableHover">
            <thead class="thead-light">
              <tr>
                  <th>no</th>
                  <th>code</th>
                  <th>title</th>
                  <th>description</th>
                  <th>tanggal</th>
                  <th>Debit</th>
                  <th>Kredit</th>
                  <th>status</th>
              </tr>
            </thead>
            <tbody>
              @for ($i = 0; $i < count($data); $i++)
                <tr>
                  <td>{{$i +1}}</td>
                  <td>{{$data[$i]['id']}}</td>
                  <td>{{$data[$i]['title']}}</td>
                  <td>{{$data[$i]['description']}}</td>
                  <td>{{$data[$i]['tanggal']}}</td>
                  <td>{{$data[$i]['debit']}}</td>
                  <td>{{$data[$i]['kredit']}}</td>
                  @if ($data[$i]['status'] == 1)
                    <td>valid</td>
                  @elseif($data[$i]['status'] == 0)
                    <td>invalid</td>
                  @endif
                </tr>
              @endfor
            </tbody>
          </table>
