
          <table class="table align-items-center table-flush table-hover" id="dataTableHover">
            <thead class="thead-light">
              <tr>
                  <th>no</th>
                  <th>Code</th>
                  <th>Tanggal</th>
                  <th>Kasir</th>
                  <th>Tipe Pembayaran</th>
                  <th>Discount</th>
                  <th>Total</th>
                  <th>Total_diskon</th>
                  <th>Tunai</th>
              </tr>
            </thead>
            <tbody>
              @for ($i = 0; $i < count($data); $i++)
                <tr>
                  <td>{{$i +1}}</td>
                  <td>{{$data[$i]['code']}}</td>
                  <td>{{$data[$i]['tanggal']}}</td>
                  <td>{{$data[$i]['kasir']}}</td>
                  @if ($data[$i]['tipe_bayar'] == 1)
                  <td>Cash</td>
                @elseif ($data[$i]['tipe_bayar'] == 2)
                  <td>E-money</td>
                @elseif ($data[$i]['tipe_bayar'] == 3)
                  <td>Debit</td>
                @elseif ($data[$i]['tipe_bayar'] == 4)
                  <td>Cash In Tunai</td>
                @elseif ($data[$i]['tipe_bayar'] == 5)
                  <td>Cash In Debit</td>
                @elseif ($data[$i]['tipe_bayar'] == 6)
                  <td>Cash Out Tunai</td>
                @elseif ($data[$i]['tipe_bayar'] == 7)
                  <td>Cash Out Debit</td>
                @endif
                  <td>{{$data[$i]['discount']}}</td>
                  <td>{{$data[$i]['total_dibayar']}}</td>
                  @if ($data[$i]['discount'] == 0)
                      <td>0</td>
                  @else
                  <td>{{$data[$i]['total_dibayar'] - ($data[$i]['discount']/100 * $data[$i]['total_dibayar'])}}</td>

                  @endif
                  <td>{{$data[$i]['jumlah_bayar']}}</td>
                </tr>
              @endfor
            </tbody>
          </table>
