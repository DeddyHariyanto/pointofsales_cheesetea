<!DOCTYPE html>
<html>
<head>
    <title>Struk</title>
</head>
<body>
    @php
        $nameGerai = '';
        $gerai = App\Gerai::where('id',$details['gerai_id'])->first();
        if ($gerai) {
            $nameGerai = $gerai->nama_gerai;
        }
        $kasir = App\User::where('id',$details['kasir'])->first();
        $detailKu = App\detailTransaksi::where('id_transaksi', $details['id'])
                                ->groupBy('id_menu')
                                ->select('nama','harga',DB::raw('count(id_menu) as qty'))
                                ->get();
        $detailStruk = [];
        foreach ($detailKu as $key => $detailKus) {
            $detailStruk[$key] = [
            'nama' =>$detailKus->nama,
            'harga_satuan' =>$detailKus->harga,
            'harga_total' =>$detailKus->harga * $detailKus->qty,
            'qty' => $detailKus->qty,
            ];
        }
    
    @endphp
    <h1>No.Struk: # {{ $details['title'] }}</h1>
    <p>Gerai : {{ $nameGerai }}</p>
    <p>Kasir : {{ $kasir->nama }}</p>
    
    <table>
        <tr>
            <th>No</th>
            <th>menu</th>
            <th>qty</th>
            <th>total</th>
        </tr>
        @for ($i = 0; $i < count($detailStruk); $i++)
        <tr>
            <td>{{$i + 1}}<br>
            <td>{{$detailStruk[$i]['nama']}}<br>Rp. {{number_format($detailStruk[$i]['harga_satuan'],0)}}
            </td>
            <td>{{ $detailStruk[$i]['qty']}}</td>
            <td>
                Rp. {{number_format($detailStruk[$i]['harga_total'],0)}}
            </td>
        </tr>
        @endfor
    </table>

    <p>Total : Rp. {{number_format($details['total_dibayar_transaksi'],0)}}</p>
    <p>Discount {{$details['discount_transaksi']}}%: Rp. {{number_format($details['discount_transaksi']/100 * $details['total_dibayar_transaksi'],0)}}</p>
    <p>Tunai : Rp. {{number_format($details['jumlah_bayar_transaksi'],0)}}</p>
    <p>Terima Kasih Telah Berbelanja Di Chess Tea</p>
</body>
</html>