@extends('layouts.app')

@section('sidebar')
@include('layouts.sidebar.sidebar')
@endsection

@section('header')
@include('layouts.header.header')
@endsection

@section('content')
<style>
#map-canvas {
	height: 1000px;
	/* The height is 400 pixels */
	width: 100%;
	/* The width is the width of the web page */
}
</style>
@if (session('error'))
<div class="alert alert-danger">{{ session('error') }}</div>
@endif
	<div class="toolbar d-flex flex-stack mb-0 mb-lg-n4 pt-5" id="kt_toolbar">
						<div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
							<div class="page-title d-flex flex-column me-3">
								<h1 class="d-flex text-dark fw-bolder my-1 fs-3"></h1>
							</div>
						</div>
					</div>
	<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
							<!--begin::Post-->
							<div class="content flex-row-fluid" id="kt_content">
								<!--begin::Row-->
								<div class="row g-5 g-xl-10 mb-xl-10">
									
									{{-- <div class="col-md-6 col-lg-6 col-xl-6 col-xxl-3 mb-md-5 mb-xl-10"> --}}
										{{-- ini untuk card penjualan produk hari ini --}}
										<div class="card card-flush h-md-50 mb-5 mb-xl-10">
											<div class="card-header pt-5">
												<div class="card-title d-flex flex-column">
													<div class="d-flex align-items-center">
														{{-- <span class="fs-4 fw-bold text-gray-400 me-1 align-self-start">Rp</span> --}}
														<span class="fs-2hx fw-bolder text-dark me-2 lh-1"></span>
														<span class="badge badge-success fs-6 lh-1 py-1 px-2 d-flex flex-center" style="height: 22px">
														<!--end::Svg Icon-->Pendapatan Per Gerai</span>
													</div>
													{{-- <span class="text-gray-400 pt-1 fw-bold fs-6">Penjualan Produk hari ini</span> --}}
												</div>
											</div>
											<div class="card-body pt-2 pb-4 d-flex align-items-center">
													<canvas id="pendapatan_per_gerai" style="min-width: 25px; min-height: 25px" ></canvas>
											</div>
											<div class="card-body pt-2 pb-4 d-flex align-items-center">
												{{-- <div class="tablePendapatan"></div> --}}
												<table id="kt_datatable_example_1" class="table table-row-bordered gy-5">
													<thead>
														<tr class="fw-bolder text-muted">
															<th class="min-w-150px">No</th>
															<th class="min-w-150px">Nama Gerai</th>
															<th class="min-w-140px">Pendapatan</th>
														</tr>
													</thead>
													<tbody class="tablePendapatan">
													</tbody>
												</table>
											</div>
										</div>
										{{-- end untuk card penjualan produk hari ini --}}
									{{-- </div> --}}
									<div class="col-md-6 col-lg-6 col-xl-6 col-xxl-3 mb-md-5 mb-xl-10">
										{{-- ini untuk card penjualan produk hari ini --}}
										<div class="card card-flush h-md-50 mb-5 mb-xl-10">
											<div class="card-header pt-5">
												<div class="card-title d-flex flex-column">
													<div class="d-flex align-items-center">
														{{-- <span class="fs-4 fw-bold text-gray-400 me-1 align-self-start">Rp</span> --}}
														<span class="fs-2hx fw-bolder text-dark me-2 lh-1"></span>
														<span class="badge badge-success fs-6 lh-1 py-1 px-2 d-flex flex-center" style="height: 22px">
														<!--end::Svg Icon-->Menu Terlaris</span>
													</div>
													{{-- <span class="text-gray-400 pt-1 fw-bold fs-6">Penjualan Produk hari ini</span> --}}
												</div>
											</div>
											<div class="card-body pt-2 pb-4 d-flex align-items-center">
												<div class="d-flex flex-center me-5 pt-2">
													<canvas id="penjualan_produk_hari_ini_chart" style="min-width: 25px; min-height: 25px" ></canvas>
												</div>
												<div class="nilaiMenu"></div>
											</div>
										</div>
										{{-- end untuk card penjualan produk hari ini --}}
									</div>
									<div class="col-md-6 col-lg-6 col-xl-6 col-xxl-3 mb-md-5 mb-xl-10">
										<div class="card card-flush h-md-50 mb-5 mb-xl-10">
											<div class="card-header pt-5">
												<div class="card-title d-flex flex-column">
													<div class="d-flex align-items-center">
														<span class="badge badge-success fs-6 lh-1 py-1 px-2 d-flex flex-center" style="height: 22px">Inventory Terbanyak Digunakan</span>
													</div>
													{{-- <span class="text-gray-400 pt-1 fw-bold fs-6">(Daily)Jumlah Transaksi VS Jumlah Customer Terinput</span> --}}
												</div>
											</div>
											<div class="card-body pt-2 pb-4 d-flex align-items-center">
												<div class="d-flex flex-center me-5 pt-2">
													<canvas id="jumlah_pelanggan_dan_input_chart" style="min-width: 25px; min-height: 25px"></canvas>
												</div>
												<div class="nilaiInventory"></div>
											</div>
											
										</div>
									</div>
									<div class="card card-flush h-md-50 mb-5 mb-xl-10">
										<div class="card-header pt-5">
											<div class="card-title d-flex flex-column">
												<div class="d-flex align-items-center">
													{{-- <span class="fs-4 fw-bold text-gray-400 me-1 align-self-start">Rp</span> --}}
													<span class="fs-2hx fw-bolder text-dark me-2 lh-1"></span>
													<span class="badge badge-success fs-6 lh-1 py-1 px-2 d-flex flex-center" style="height: 22px">
													<!--end::Svg Icon-->Demografis Lokasi Gerai & Customer</span>
												</div>
												{{-- <span class="text-gray-400 pt-1 fw-bold fs-6">Penjualan Produk hari ini</span> --}}
											</div>
										</div>
											<div id="map-canvas" class="map-canvas"  lok-lat="-1.2564674" lok-lng="116.874169" style="height: 300px"></div>
									</div>
								</div>
							</div>
						</div>



<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
<script type="text/javascript">
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1MgLuZuyqR_OGY3ob3M52N46TDBRI_9k&libraries=geometry,places&callback=initMap"
	
	function initMap(gerai, customer) {
		var toko = "/argon/img/toko1.png";
		var human = "/argon/img/human.png";

		let awal = { lat: -1.2564674, lng: 116.874169 }
		const map = new google.maps.Map(document.getElementById("map-canvas"), {
            center: awal,
            zoom: 13,
        });
		const markers = gerai.map(function(gerais, i) {
			let lat = gerai[i]['lat_lng_gerai'].split("/")[0];
			let lng = gerai[i]['lat_lng_gerai'].split("/")[1];
			var myLatlng = new google.maps.LatLng(parseFloat(lat),parseFloat(lng))
			// console.log(myLatlng);
			return new google.maps.Marker({
				animation: google.maps.Animation.DROP, 
				position: myLatlng,
				map: map,
				title: gerai[i]['nama_gerai'],
				alamat: gerai[i]['lokasi_gerai'],
				icon: toko,

			})
		});

		for (let i = 0; i < markers.length; i++) {
			let infoVehicle = '<h6 style="text-align: center;">'+ markers[i]['title'] +'</h6>'+
								'<span  font-size: 10px;> Lokasi: '+ markers[i]['alamat'] +'</span>';
				
			const infowindow = new google.maps.InfoWindow({
			content: infoVehicle,
			// maxWidth: 300
			});
			markers[i].addListener("click", () => {
				infowindow.open(map, markers[i]);
			});
		}

		for (let i = 0; i < customer.length; i++) {
			let alamat = customer[i]['alamat'] +' balikpapan';
			let jumlahOrang = customer[i]['jumlah'];
			$.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+alamat+'&key=AIzaSyA1MgLuZuyqR_OGY3ob3M52N46TDBRI_9k', null, 
				function (data) {
					var p = data.results[0].geometry.location
					var latlng = new google.maps.LatLng(p.lat, p.lng);
					var Marger = new google.maps.Marker({
						position: latlng,
						map: map,
						icon: human,
						// title: ,
					});
					new google.maps.Circle({
						strokeColor: "#81f542",
						strokeOpacity: 0.8,
						strokeWeight: 2,
						fillColor: "#81f542",
						fillOpacity: 0.35,
						map,
						center: latlng,
						radius: Math.sqrt(jumlahOrang) * 100,
					});
					let areas = '<h6 style="text-align: center;">'+ alamat +'</h6>'+
								'<span  font-size: 10px;> Populasi Customer: '+ jumlahOrang +'</span>'	;
				
					const infowindow = new google.maps.InfoWindow({
					content: areas,
					// maxWidth: 300
					});
					Marger.addListener("click", () => {
						infowindow.open(map, Marger);
					});
			});
		}
	}
		fetch('/laporan-api', {
						method: 'get'
					})
			.then(response => response.json())
			.then(data => {
				let nameMenu = []
				let menuTerlaris = []

				let nameIncome = []
				let incomeTertinggi = []

				let nameInventory = []
				let inventoryTerlaris = []
				
				// let alamat = []
				
				let nilaiMenu = '';
				let nilaiInventory = '';
				for (let i = 0; i < data['terlaris'].length; i++) {
					nameMenu.push(data['terlaris'][i]['nama']) 
					menuTerlaris.push(data['terlaris'][i]['jumlah']) 
					nilaiMenu += '<div class="d-flex flex-column content-justify-center w-100">'+
									'<div class="d-flex fs-6 fw-bold align-items-center">'+
										'<div class="bullet w-8px h-6px rounded-2 bg-primary me-3"></div>'+
										'<div class="text-gray-500 flex-grow-1 me-4">'+ data['terlaris'][i]['nama'] +'</div>'+
										'<div class="fw-boldest text-gray-700 text-xxl-end">'+ data['terlaris'][i]['jumlah'] +'</div>'+
									'</div>'+
								'</div>';
				}
				$('.nilaiMenu').html(nilaiMenu);
				for (let i = 0; i < data['inventory'].length; i++) {
					if (data['inventory'][i]['nama_kategori'] != null) {
						nameInventory.push(data['inventory'][i]['nama_kategori']+' ( '+ data['inventory'][i]['satuan_komposisi']+' )') 
						inventoryTerlaris.push(data['inventory'][i]['jumlah'] ) 
						nilaiInventory += '<div class="d-flex flex-column content-justify-center w-100">'+
									'<div class="d-flex fs-6 fw-bold align-items-center">'+
										'<div class="bullet w-8px h-6px rounded-2 bg-primary me-3"></div>'+
										'<div class="text-gray-500 flex-grow-1 me-4">'+ data['inventory'][i]['nama_kategori']+'</div>'+
										'<div class="fw-boldest text-gray-700 text-xxl-end">'+ data['inventory'][i]['jumlah'] +' '+ data['inventory'][i]['satuan_komposisi']+'</div>'+
									'</div>'+
								'</div>';
					}
				}
				$('.nilaiInventory').html(nilaiInventory);
				
				
				var income = data['income'];
				let Highincome = '';
				for (let i = 0; i < income.length; i++) {
					nameIncome.push(income[i]['nama_gerai']) 
					incomeTertinggi.push(income[i]['total'])
					Highincome += '<tr>'+
									'<td>'+ i +'</td>'+
									'<td>'+ income[i]['nama_gerai']+'</td>'+
									'<td>'+ new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(income[i]['total']) +'</td>'+
								'</tr>';
				}
				$('.tablePendapatan').html(Highincome);
				var table = $('#kt_datatable_example_1').DataTable();
				
				//maps
				var gerai = data['gerai'];
				var customer = data['customer'];

				initMap(gerai, customer);

				//chart
				var ctx = document.getElementById("penjualan_produk_hari_ini_chart");
				var ctx2 = document.getElementById("jumlah_pelanggan_dan_input_chart");
				var ctx3 = document.getElementById("pendapatan_per_gerai");
		
				let backgroundColor = [
					'rgb(255, 99, 132)',
					'rgb(54, 162, 235)',
					'rgb(255, 205, 86)',
					'rgb(163, 97, 201)',
					'rgb(25, 161, 189)',
				];
				// Define fonts
				var fontFamily = KTUtil.getCssVariableValue('--bs-font-sans-serif');
				// Chart config
				const config = {
					type: 'doughnut',
					data : {
						labels: nameMenu,
						datasets: [
							{
								// label: nameMenu,
								data: menuTerlaris,
								backgroundColor: backgroundColor,
								hoverOffset: 4
							}
						],
					},
					defaults:{
						global: {
							defaultFont: fontFamily,
						},
						devicePixelRatio:window.devicePixelRatio,
					}
				};
		
				// Init ChartJS -- for more info, please visit: https://www.chartjs.org/docs/latest/
				var myChart = new Chart(ctx, config);
		
				const config2 = {
					type: 'pie',
					data : {
						labels: nameInventory,
						datasets: [
							{
								// label: nameMenu,
								data: inventoryTerlaris,
								backgroundColor: backgroundColor,
								hoverOffset: 4
							}
						],
					},
					defaults:{
						global: {
							defaultFont: fontFamily,
						},
						devicePixelRatio:window.devicePixelRatio,
					}
				};
		
				// Init ChartJS -- for more info, please visit: https://www.chartjs.org/docs/latest/
				var myChart = new Chart(ctx2, config2);

				const config3 = {
					type: 'bar',
					data : {
						labels: nameIncome,
						datasets: [
							{
								label: 'Pendapatan',
								data: incomeTertinggi,
								backgroundColor: backgroundColor,
								hoverOffset: 4
							}
						],
					},
					defaults:{
						global: {
							defaultFont: fontFamily,
						},
						devicePixelRatio:window.devicePixelRatio,
					}
				};
		
				// Init ChartJS -- for more info, please visit: https://www.chartjs.org/docs/latest/
				var myChart = new Chart(ctx3, config3);
			}).catch(err => console.log(err))
	// }  
  

		// let total_transaksi = $('.total_transaksi').val();
		// let langganan = $('.langganan').val();
		
	
</script>

@endsection
