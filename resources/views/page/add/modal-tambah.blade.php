<div class="modal fade" id="modal-tambah" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <form class="form" action="#" id="kt_tambah_data_form">
                        <div class="modal-header" id="kt_tambah_data_header">
                            <h3 class="modal-title">Tambah Data</h3>
                            <div class="btn btn-sm btn-icon btn-active-warning btn-color-muted" data-bs-dismiss="modal">
                                <i class="fas fa-times"></i>
                            </div>
                        </div>
                        <div class="modal-body py-10 px-lg-17">
                            <!--begin::Scroll-->
                            <div class="scroll-y me-n7 pe-7" id="kt_tambah_data_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_tambah_data_header" data-kt-scroll-wrappers="#kt_tambah_data_scroll" data-kt-scroll-offset="300px">
                                <div class="notice d-flex bg-light-warning rounded border-warning border border-dashed mb-9 p-6">
                                    <span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
                                            <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black" />
                                            <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black" />
                                        </svg>
                                    </span>
                                    <div class="d-flex flex-stack flex-grow-1">
                                        <div class="fw-bold">
                                            <h5 class="text-gray-900 fw-bolder mb-0">Perhatian</h5>
                                            <div class="fs-6 text-gray-700">Produk yang dimasukkan akan langsung tampil pada halaman kasir</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-5">
                                    <div class="col-md-6 fv-row">
                                        <label class="required fs-5 fw-bold mb-2">Nama Produk</label>
                                        <input type="text" class="form-control" placeholder="" name="first-name" />
                                    </div>
                                    <div class="col-md-6 fv-row">
                                        <label class="required fs-5 fw-bold mb-2">Harga</label>
                                        <input type="number" class="form-control" placeholder="" name="last-name" />
                                    </div>
                                </div>
                                <div class="d-flex flex-column mb-5 fv-row">
                                    <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                                        <span class="required">Kategori</span>
                                        <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Pilih Kategori Untuk Produk anda"></i>
                                    </label>
                                    <select class="form-select" aria-label="Select example">
                                        <option></option>
                                        <option value="1">Kategori</option>
                                        <option value="2">Kategori</option>
                                    </select>
                                </div>
                                <div class="d-flex flex-column mb-5 fv-row">
                                    <label class="required fs-5 fw-bold mb-2">Deskripsi Produk</label>
                                    <input class="form-control" placeholder="" name="deskripsi" />
                                </div>
                             </div>
                        </div>
                        <div class="modal-footer flex-center">
                            <button type="reset" id="kt_tambah_data_cancel" class="btn btn-light me-3">Discard</button>
                            <button type="submit" id="kt_tambah_data_submit" class="btn btn-warning">
                                <span class="indicator-label text-dark">Submit</span>
                                <span class="indicator-progress">Please wait...
                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>