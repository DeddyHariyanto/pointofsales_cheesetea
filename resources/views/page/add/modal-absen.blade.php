<div class="modal fade" tabindex="-1" id="modal-absen">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header py-3">
                <h3 class="modal-title">Absen</h3>
                <div class="btn btn-icon btn-sm btn-active-light-warning ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times"></i>
                </div>
            </div>
            <div class="modal-body mb-4">
                <div class="mb-2">
                    <h1 class="fw-bold text-gray-800 text-center lh-lg">Apakah anda
                    <br>telah melakukan
                    <span class="fw-boldest text-warning">Absen ?</span></h1>
                    <div class="flex-grow-1 bgi-no-repeat bgi-size-contain bgi-position-x-center card-rounded-bottom h-200px mh-200px my-5 my-lg-12" style="background-image:url('../assets/media/svg/illustrations/easy/1.svg')"></div>
                </div>
                <div class="text-center">
                    <form class="form-absensi" method="post" action="../absensi-post"enctype= multipart/form-data>
                        @csrf
                        <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                        <input type="hidden" name="lokasi_gps" id="lokasi_gps" >
                        <input type="hidden" class="form-control" id="lat" name="lat" >
                        <input type="hidden" class="form-control" id="lng" name="lng">
                        <input type="file"  accept="image/*" capture class="form-control @error('foto') is-invalid @enderror getImg text-center " name="foto" id="myFileInput" style="display:none;" />
                        <button type="button" class="btn btn-warning me-2 text-dark" onclick="document.getElementById('myFileInput').click()" ><i class="fas fa-camera me-2 text-dark"></i>Ambil Foto</button>
                        {{-- <button type="submit" class="btn btn-warning me-2 text-dark"><i class="fas fa-camera me-2 text-dark"></i>Ambil Foto</button> --}}
                        <span class="btn btn-light text-dark" id="time"></span>
                    </form>
                    {{-- <a class="btn btn-light text-dark"></a> --}}
                   
                </div>
            </div>
            
        </div>
    </div>
</div>
<script>
    $('#myFileInput').on('change', function() {
        if($(this).val()) {
            $('.form-absensi').submit();
        }
    });
    function previewFile(){
    var preview = document.querySelector('.getImg');
    var file    = document.querySelector('input[type=file]').files[0];
    var reader  = new FileReader();

    reader.onloadend = function () {
      preview.src = reader.result;
    }

    if (file) {
      reader.readAsDataURL(file); 
    } else {
      preview.src = '{{asset('argon')}}/img/icons/icon-jam.svg';
    }
  }
  previewFile(); 
function startTime() {
  const today = new Date();
    let h = today.getHours();
    let m = today.getMinutes();
    let s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
  let format = h + ":" + m + ":" + s;
  document.getElementById('time').innerHTML = format;
//   console.log(format);
  setTimeout(startTime, 1000);
}

function checkTime(i) {
  if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
  return i;
}
</script>
<script  async defer src="https://maps.googleapis.com/maps/api/js?libraries=geometry&key=AIzaSyA1MgLuZuyqR_OGY3ob3M52N46TDBRI_9k&callback=initMap"></script>
<script type="text/javascript">

function initMap() {
  let options = {
    timeout: 30000000,
  };
  if ("geolocation" in navigator){
    //success
    function success(position){
        $('#lat').val(position.coords.latitude)
        $('#lng').val(position.coords.longitude)
        let lat = position.coords.latitude;
        let lng = position.coords.longitude;

        //get lokasi
        $.ajax({
          type: 'GET',
          url: 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+position.coords.latitude+','+position.coords.longitude+'&key=AIzaSyA1MgLuZuyqR_OGY3ob3M52N46TDBRI_9k',
          dataType: 'json',
          success: function (data) {
            var lokasi_gps = data["results"]["1"]["formatted_address"];
            $('#lokasi_gps').val(lokasi_gps);
          },error:function(){
          }
        });

        //lokasi in maps
      };
    function error(err) {
      console.warn('ERROR(' + err.code + '): ' + err.message);
    }
    options = {
      // enableHighAccuracy: false,
      timeout: 30000000,
      maximumAge: 0
    };
    navigator.geolocation.watchPosition(success, error, options)
  }else{
    console.log("Browser doesn't support geolocation!");
  }
} 
// $("#form_absensi").submit(function (e) {
//         $("#submit_absen").attr("disabled", true);
//         return true;
//       });
</script>