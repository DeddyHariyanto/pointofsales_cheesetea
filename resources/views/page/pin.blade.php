@extends('layouts.app')

@section('content')
<div class="d-flex flex-column flex-root">
	<div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed pt-20" style="background-image: url('../assets/media/illustrations/sketchy-1/14.png')">
		<div class="d-flex flex-column flex-center p-10 p-lg-20">
			<div class="card shadow-none bg-white">
				<div class="card-body">
						<a href="#!" onclick="history.back()" class="h5 fs-6"><u><i class="fas fa-arrow-left me-2"></i>Kembali</u></a>
					<div class="d-flex flex-column flex-center">
				<h4 class="fw-bolder fs-2qx text-dark mt-4 mb-0">PIN ANDA</h4>
				<span class="text-muted mb-7">Silahkan masukkan PIN anda sebelum melakukan transaksi</span>
				<form class="form" method="POST" action="{{url('/kasir-akses')}}" autocomplete="off" enctype= multipart/form-data>
					@csrf @method('post')
					<div class="d-flex flex-center">
						<input type="password" pattern="[0-9]{6}" class="form-control" placeholder="123xxx" maxlength="6" name="master_pin" id="txtNewPassword" autocomplete="new-password" required/>
						<button type="submit" id="kt_coming_soon_submit" class="btn btn-warning fw-bolder m-2 text-nowrap">
							<span class="indicator-label text-dark">Submit</span>
							<span class="indicator-progress">Please wait...
								<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
							</button>
						</div>
					</form>
				</div>
				</div>
			</div>
		</div>		
	</div>
</div>
@endsection