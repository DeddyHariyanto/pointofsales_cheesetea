<div id="kt_aside" class="aside px-5" data-kt-drawer="true" data-kt-drawer-name="aside" data-kt-drawer-activate="true" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '285px'}" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_toggle">
	<div class="aside-menu flex-column-fluid">
		<div class="hover-scroll-overlay-y my-5 me-n4 pe-4" id="kt_aside_menu_wrapper" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_aside_footer" data-kt-scroll-wrappers="#kt_aside, #kt_aside_menu" data-kt-scroll-offset="2px">
			<div class="menu menu-column menu-active-bg menu-state-primary menu-title-gray-700 fs-6 menu-rounded w-100 fw-bold" id="#kt_aside_menu" data-kt-menu="true">


				<div class="menu-item">
					<a class="menu-link" href="https://preview.keenthemes.com/metronic8/demo18/layout-builder.html" title="Build your layout and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">

						<span class="menu-title">Q.Milk Cheestea</span>
					</a>
				</div>

				<div class="menu-item">
					<div class="menu-content">
						<div class="separator mx-1 my-4"></div>
					</div>
				</div>

				<div class="menu-item">
					<div class="menu-content pt-8 pb-0">
						<span class="menu-section text-muted text-uppercase fs-8 ls-1">Main Menu</span>
					</div>
				</div>
				@if (Auth::user()->level == 'owner')
					<div class="menu-item">
						<a class="menu-link" href="../gerai-index/aktif" title="Build your layout and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
							<span class="menu-icon">
								<i class="fas fa-store"></i>
							</span>
							<span class="menu-title">Gerai</span>
						</a>
					</div>
					<div class="menu-item">
						<a class="menu-link" href="../karyawan-index" title="Build your layout and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
							<span class="menu-icon">
								<i class="fas fa-users"></i>
							</span>
							<span class="menu-title">Karyawan</span>
						</a>
					</div>
					<div class="menu-item">
						<a class="menu-link" href="/menu-index" title="Build your layout and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
							<span class="menu-icon">
								<i class="fas fa-users"></i>
							</span>
							<span class="menu-title">Menu</span>
						</a>
					</div>
					<div class="menu-item">
						<a class="menu-link" href="/inventory-index" title="Build your layout and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
							<span class="menu-icon">
								<i class="fas fa-users"></i>
							</span>
							<span class="menu-title">Inventory</span>
						</a>
					</div>
					<div class="menu-item">
						<a class="menu-link" href="/customer-index" title="Build your layout and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
							<span class="menu-icon">
								<i class="fas fa-users"></i>
							</span>
							<span class="menu-title">Customer</span>
						</a>
					</div>
					<div class="menu-item">
						<a class="menu-link" href="/absensi-index" title="Build your layout and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
							<span class="menu-icon">
								<i class="fas fa-users"></i>
							</span>
							<span class="menu-title">Absensi</span>
						</a>
					</div>
					<div class="menu-item">
						<a class="menu-link" href="/pesanan-index/invalid" title="Build your layout and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
							<span class="menu-icon">
								<i class="fas fa-users"></i>
							</span>
							<span class="menu-title">Pesanan</span>
						</a>
					</div>
					<div class="menu-item">
						<a class="menu-link" href="https://preview.keenthemes.com/metronic8/demo18/layout-builder.html" title="Build your layout and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
							<span class="menu-icon">
								<i class="fas fa-users"></i>
							</span>
							<span class="menu-title">Laporan</span>
						</a>
					</div>
				@elseif(Auth::user()->level == 'karyawan')
				<div class="menu-item">
					<a class="menu-link" href="/pin" title="Build your layout and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
						<span class="menu-icon">
							<i class="fas fa-users"></i>
						</span>
						<span class="menu-title">Kasir</span>
					</a>
				</div>
				<div class="menu-item">
					<a class="menu-link" href="/menu-index" title="Build your layout and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
						<span class="menu-icon">
							<i class="fas fa-users"></i>
						</span>
						<span class="menu-title">Menu</span>
					</a>
				</div>
				<div class="menu-item">
					<a class="menu-link" href="/pesanan-index/invalid" title="Build your layout and export HTML for server side integration" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
						<span class="menu-icon">
							<i class="fas fa-users"></i>
						</span>
						<span class="menu-title">Pesanan</span>
					</a>
				</div>
				@endif

				<div class="menu-item">
					<div class="menu-content">
						<div class="separator mx-1 my-4"></div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
