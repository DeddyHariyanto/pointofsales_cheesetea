@extends('layouts.app')

@section('sidebar')
@include('layouts.sidebar.sidebar')
@endsection

@section('header')
@include('layouts.header.header')
@endsection

@section('content')
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-fluid">
	<div class="content flex-row-fluid" id="kt_content">
		<div class="row justify-content-center">
			<div class="col-xl-12">
				<div class="row justify-content-center gy-0 gx-10">
					<div class="col-xl-12 mb-4">
						<div class="toolbar d-flex flex-stack mb-0" id="kt_toolbar">
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack flex-wrap">
								<div class="page-title d-flex flex-column me-3">
									<div class="card-toolbar">
												<ul class="nav">
													<li class="nav-item">
														<a class="nav-link btn btn-sm btn-color-muted btn-active btn-active-light-primary active fw-bolder px-4 me-1" data-bs-toggle="tab" href="#kt_table_widget_7_tab_1">Gerai Aktif</a>
													</li>
													<li class="nav-item">
														<a class="nav-link btn btn-sm btn-color-muted btn-active btn-active-light-primary fw-bolder px-4 me-1" data-bs-toggle="tab" href="#kt_table_widget_7_tab_2">Gerai Non Aktif</a>
													</li>
												</ul>
											</div>
								</div>
								<div class="d-flex align-items-center py-2">
									<a href="#modal-tambah" data-bs-toggle="modal" class="btn btn-warning text-dark" id="kt_toolbar_primary_button"><i class="fas fa-plus me-2 text-dark"></i>Tambah Gerai</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-12 mb-4">
						<table id="kt_datatable_example_1" class="table table-row-bordered gy-5">
							<thead>
								<tr class="fw-bold fs-7 text-muted">
									<th>#</th>
									<th>Gerai</th>
									<th>Karyawan</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@for ($i=0; $i < 10; $i++)
									<tr>
										<td class="align-middle text-muted">{{$i}}</td>
										<td>
											<div class="d-flex align-items-sm-center">
												<div class="symbol symbol-50px me-5">
													<span class="symbol-label">
														<img src="{{asset('assets')}}/media/cheestea/icon.png" class="h-50 align-self-center" alt="">
													</span>
												</div>
												<div class="d-flex align-items-center flex-row-fluid flex-wrap">
													<div class="flex-grow-1 me-2">
														<a href="#" class="text-gray-800 text-hover-primary fs-5 fw-bolder">Gerai Spaku</a>
														<span class="text-muted fw-bold d-block fs-6">Spaku 5 Penajam Paser Utara</span>
														<span class="badge badge-light-success fw-bold me-1 fs-6">Gerai Aktif</span>
													</div>
												</div>
											</div>
										</td>
										<td>
											<span class="badge badge-light-primary fw-bold me-1 fs-6">Suketi</span>
											<br>
											<span class="badge badge-light-danger fw-bold me-1 fs-6">Tukiyem</span>
										</td>
										<td>
											<button class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-overflow="true">
												<!--begin::Svg Icon | path: icons/duotune/general/gen023.svg-->
												<span class="svg-icon svg-icon-1">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4" fill="black" />
														<rect x="11" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
														<rect x="15" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
														<rect x="7" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->
											</button>
											<!--begin::Menu 2-->
											<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px" data-kt-menu="true">
												<!--begin::Menu item-->
												<div class="menu-item px-3">
													<div class="menu-content fs-6 text-dark fw-bolder px-3 py-4">Daftar Aksi</div>
												</div>
												<!--end::Menu item-->
												<!--begin::Menu separator-->
												<div class="separator mb-3 opacity-75"></div>
												<!--end::Menu separator-->
												<!--begin::Menu item-->
												<div class="menu-item px-3">
													<a href="#" class="menu-link px-3">Kasir</a>
												</div>
												<!--end::Menu item-->
												<!--begin::Menu item-->
												<div class="menu-item px-3">
													<a href="/gerai-detail" class="menu-link px-3">Detail</a>
												</div>
												<!--end::Menu item-->
												<!--begin::Menu separator-->
												<div class="separator mt-3 opacity-75"></div>
												<!--end::Menu separator-->
												<!--begin::Menu item-->
												<div class="menu-item px-3">
													<div class="menu-content px-3 py-3">
														<a class="btn btn-danger btn-sm px-4" href="#">Nonaktifkan gerai</a>
													</div>
												</div>
												<!--end::Menu item-->
											</div>
										</td>
									</tr>
								@endfor
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@include('gerai.modal.modal-tambah-data')

<script type="text/javascript">
	$(document).ready(function() {
		$("#kt_datatable_example_2").DataTable({
			"scrollY": "170px",
			"scrollCollapse": true,
			"paging": false,
			"dom": "<'table-responsive'tr>"
		});
		$('#kt_datatable_example_1').DataTable( {
			"language": {
				"lengthMenu": "Show _MENU_",
			},
			"dom":
			"<'row'" +
			"<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
			"<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
			">" +

			"<'table-responsive'tr>" +

			"<'row'" +
			"<'col-sm-12 col-xl-5 d-flex align-items-center justify-content-center justify-content-xl-start'i>" +
			"<'col-sm-12 col-xl-7 d-flex align-items-center justify-content-center justify-content-xl-end'p>" +
			">"
		} );
	});
</script>

@endsection
