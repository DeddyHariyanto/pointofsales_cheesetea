<div class="modal fade" id="modal_tambah" tabindex="-1" aria-hidden="true">
											<!--begin::Modal dialog-->
											<div class="modal-dialog modal-dialog-centered mw-950px">
												<!--begin::Modal content-->
												<div class="modal-content">
													<!--begin::Modal header-->
													<div class="modal-header" id="kt_modal_add_user_header">
														<!--begin::Modal title-->
														<h2 class="fw-bolder">Tambah Menu</h2>
														<!--end::Modal title-->
														<!--begin::Close-->
														<div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal" aria-label="Close">
															<i class="fas fa-times"></i>
														</div>
														<!--end::Close-->
													</div>
													<!--end::Modal header-->
													<!--begin::Modal body-->
													<div class="modal-body scroll-y mx-5 mx-xl-15 my-7">
														<!--begin::Form-->
														<form id="kt_modal_add_user_form" class="form" action="#">
															<!--begin::Scroll-->
															<div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_add_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_user_header" data-kt-scroll-wrappers="#kt_modal_add_user_scroll" data-kt-scroll-offset="300px">
																<!--begin::Input group-->
																<div class="fv-row mb-7">
																	<!--begin::Label-->
																	<label class="d-block fw-bold fs-6 mb-5">Thumbnail</label>
																	<!--end::Label-->
																	<!--begin::Image input-->
																	<div class="image-input image-input-outline" data-kt-image-input="true" style="background-image: url('berkas/menu/dummy.jpg')">
																		<!--begin::Preview existing avatar-->
																		<div class="image-input-wrapper w-125px h-125px" style="background-image: url(berkas/menu/dummy.jpg);"></div>
																		<!--end::Preview existing avatar-->
																		<!--begin::Label-->
																		<label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
																			<i class="bi bi-pencil-fill fs-7"></i>
																			<!--begin::Inputs-->
																			<input type="file" name="avatar" accept=".png, .jpg, .jpeg" />
																			<input type="hidden" name="avatar_remove" />
																			<!--end::Inputs-->
																		</label>
																		<!--end::Label-->
																		<!--begin::Cancel-->
																		<span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
																			<i class="bi bi-x fs-2"></i>
																		</span>
																		<!--end::Cancel-->
																		<!--begin::Remove-->
																		<span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
																			<i class="bi bi-x fs-2"></i>
																		</span>
																		<!--end::Remove-->
																	</div>
																	<!--end::Image input-->
																	<!--begin::Hint-->
																	<div class="form-text">Allowed file types: png, jpg, jpeg.</div>
																	<!--end::Hint-->
																</div>
																<div class="row mb-5">
                                  <div class="col-md-6 fv-row">
                                      <label class="required fs-5 fw-bold mb-2">Nama Produk</label>
                                      <input type="text" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" name="last-name" />
                                  </div>
                                  <div class="col-md-6 fv-row">
                                      <label class="required fs-5 fw-bold mb-2">Harga</label>
                                      <input type="text" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" name="last-name" />
                                  </div>
                                </div>
																<!--end::Input group-->
																<!--begin::Input group-->
																<div class="mb-7">
																	<!--begin::Label-->
																	<label class="required fw-bold fs-6 mb-5">Tipe</label>

                                	<div class="d-flex fv-row">
																		<div class="form-check form-check-custom form-check-solid">
																			<input class="form-check-input me-3" name="user_role" type="radio" value="0" id="kt_modal_update_role_option_0" checked='checked' />
																			<label class="form-check-label" for="kt_modal_update_role_option_0">
																				<div class="fw-bolder text-gray-800">Minuman</div>
																				<div class="text-gray-600">Untuk Produk tipe Minuman</div>
																			</label>
																		</div>
																	</div>

                                  <div class='separator separator-dashed my-5'></div>

																	<div class="d-flex fv-row">
																		<div class="form-check form-check-custom form-check-solid">
																			<input class="form-check-input me-3" name="user_role" type="radio" value="0" id="kt_modal_update_role_option_0" checked='checked' />
																			<label class="form-check-label" for="kt_modal_update_role_option_0">
																				<div class="fw-bolder text-gray-800">Makanan</div>
																				<div class="text-gray-600">Untuk Produk tipe Makanan</div>
																			</label>
																		</div>
																	</div>

																	<div class='separator separator-dashed my-5'></div>

																	<div class="d-flex fv-row">
																		<div class="form-check form-check-custom form-check-solid">
																			<input class="form-check-input me-3" name="user_role" type="radio" value="0" id="kt_modal_update_role_option_0" checked='checked' />
																			<label class="form-check-label" for="kt_modal_update_role_option_0">
																				<div class="fw-bolder text-gray-800">Add on</div>
																				<div class="text-gray-600">Untuk Produk tipe Tambahan</div>
																			</label>
																		</div>
																	</div>

                                </div>
																<!--end::Input group-->
															</div>
															<!--end::Scroll-->
															<!--begin::Actions-->
															<div class="text-center pt-15">
																<button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal" aria-label="Close">Batal</button>
																<button type="submit" class="btn btn-primary" data-kt-users-modal-action="submit">
																	<span class="indicator-label">Tambah</span>
																	<span class="indicator-progress">Mohon Tunggu...
																	<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
																</button>
															</div>
															<!--end::Actions-->
														</form>
														<!--end::Form-->
													</div>
													<!--end::Modal body-->
												</div>
												<!--end::Modal content-->
											</div>
											<!--end::Modal dialog-->
										</div>
