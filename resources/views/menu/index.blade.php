@extends('layouts.app')

@section('sidebar')
@include('layouts.sidebar.sidebar')
@endsection

@section('header')
@include('layouts.header.header')
@endsection

@section('content')
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-fluid">
	<div class="content flex-row-fluid" id="kt_content">
		<div class="row justify-content-center">
			<div class="col-xl-12">
				<div class="row justify-content-center gy-0 gx-10">
					<div class="col-xl-12 mb-4">
						<div class="toolbar d-flex flex-stack mb-0" id="kt_toolbar">
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack flex-wrap">
								<div class="page-title d-flex flex-column me-3">
									<h1 class="d-flex text-dark fw-bolder my-1 fs-3">Menu</h1>

									{{-- <div class="card-toolbar">
										<ul class="nav">
											<li class="nav-item">
												<a class="nav-link btn btn-sm btn-color-muted btn-active btn-active-light-primary active fw-bolder px-4 me-1" data-bs-toggle="tab" href="#kt_table_widget_7_tab_1">Gerai Aktif</a>
											</li>
											<li class="nav-item">
												<a class="nav-link btn btn-sm btn-color-muted btn-active btn-active-light-primary fw-bolder px-4 me-1" data-bs-toggle="tab" href="#kt_table_widget_7_tab_2">Gerai Non Aktif</a>
											</li>
										</ul>
									</div> --}}
								</div>
								@if (Auth::user()->level == 'owner')
									<div class="d-flex align-items-center py-2">
										<a href="#modal_tambah" data-bs-toggle="modal" class="btn btn-warning text-dark" id="kt_toolbar_primary_button"><i class="fas fa-plus me-2 text-dark"></i>Tambah Menu</a>
										<a href="#modal_tambah_kategori" data-bs-toggle="modal" class="btn btn-warning text-dark" id="kt_toolbar_primary_button"><i class="fas fa-plus me-2 text-dark"></i>Tambah Kategori</a>
									</div>
								@endif
							</div>
						</div>
					</div>
					<div class="col-xl-12 mb-4">
						<table id="kt_datatable_example_1" class="table table-row-bordered gy-5">
							<thead>
								<tr class="fw-bolder text-muted">
									<th class="min-w-150px">Nama Menu</th>
									<th class="min-w-150px">Tipe</th>
									<th class="min-w-150px">Penjualan</th>
									<th class="min-w-120px">Rating</th>
									<th class="min-w-100px text-end">Actions</th>
								</tr>
							</thead>
							<!--end::Table head-->
							<!--begin::Table body-->
							<tbody>
								@foreach ($menus as $key => $menu)
								@php
									$item = App\Komposisi::where('menu_id',$menu->id)->get();
								@endphp
									<tr>
										<td>
											<div class="d-flex align-items-center">
												<div class="symbol symbol-45px me-5">
													@if ($menu->foto_menu == 'empty')
														<img src="{{('berkas')}}/menu/dummy.jpg" alt="" />
													@else
														<img src="{{('berkas')}}/menu/{{$menu->foto_menu}}" alt="" />
													@endif
												</div>
												<div class="d-flex justify-content-start flex-column">
													<a href="#" class="text-dark fw-bolder text-hover-primary fs-6">{{$menu->nama_menu}}</a>
													<span class="text-muted fw-bold text-muted d-block fs-7">Rp.{{number_format($menu->harga_menu,0)}}</span>
												</div>
											</div>
										</td>
										<td>
											@if ($menu->tipe_menu == 'minum')
												<div class="badge badge-light-success fw-bolder">Minuman</div>
											@elseif ($menu->tipe_menu == 'makan')
												<div class="badge badge-light-primary fw-bolder">Makanan</div>
											@elseif ($menu->tipe_menu == 'add')
												<div class="badge badge-light-info fw-bolder">Add on</div>
											@endif
										</td>
										@php
											$terlaris = App\detailTransaksi::select(DB::raw('count(nama) as jumlah'))
																		->where('id_menu',$menu->id)
																		->whereMonth('created_at','=',Carbon\Carbon::now()->month)
																		->groupBy('id_menu')
																		->first();
											$allTransaksi = App\detailTransaksi::select(DB::raw('count(nama) as jumlah'))
																			->whereMonth('created_at','=',Carbon\Carbon::now()->month)
																			->groupBy('id_menu')
																			->get();
											$all = 0;
											foreach ($allTransaksi as $key => $allTransaksis) {
												$all += $allTransaksis->jumlah;
											}
											// dd($all);
											// dd(($terlaris->jumlah/($all*100)));
										@endphp	
										@if ($terlaris)
											<td class="text-end">
												<div class="d-flex flex-column w-100 me-2">
													<div class="d-flex flex-stack mb-2">
														<span class="me-2 fs-7 fw-bold">{{$terlaris->jumlah}} Kali</span>
													</div>
												</div>
											</td>
											<td class="text-end">
												<div class="d-flex flex-column w-100 me-2">
													<div class="d-flex flex-stack mb-2">
														<span class="me-2 fs-7 fw-bold">{{round(($terlaris->jumlah/$all*100),0)}} %</span>
													</div>
												</div>
											</td>
										@else
											<td class="text-end">
												<div class="d-flex flex-column w-100 me-2">
													<div class="d-flex flex-stack mb-2">
														<span class="me-2 fs-7 fw-bold"> belum ada transaksi</span>
													</div>
												</div>
											</td>
											<td class="text-end">
												<div class="d-flex flex-column w-100 me-2">
													<div class="d-flex flex-stack mb-2">
														<span class="me-2 fs-7 fw-bold">belum ada transaksi</span>
													</div>
												</div>
											</td>
										@endif
										<td>
											@if (Auth::user()->level == 'owner')
												<div class="d-flex justify-content-end flex-shrink-0">
													<div class="menu-item px-3">
														
													</div>
													<a href="#modal_edit" data-bs-toggle="modal" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="Klik untuk mengubah data" id="{{$menu['id']}}||{{$menu['nama_menu']}}||{{$menu['harga_menu']}}||{{$menu['tipe_menu']}}||{{$menu['foto_menu']}}||{{$menu['kategori_menu']}}" onclick="setEdit(this.id)">
														<i class="fas fa-edit"></i>
													</a>
													<a href="#modalHapusDataKaryawan" data-bs-toggle="modal" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="Klik untuk menghapus menu"  id="{{$menu->id}}||{{$menu->nama_menu}}" onclick="setHapus(this.id)">
														<i class="fas fa-trash"></i>
													</a>
													
													<button  class="showDetailRow btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1" id="{{$item}}">
														<i class="fas fa-angle-double-down"></i>
													</button>
												</div>
												
											@endif

										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@php
	$kategoris = App\KategoriInventory::all();
	$brand = App\Inventory::groupBy('brand_inventory')->get();
@endphp
@include('menu.modal.modal-tambah-kategori')
@include('menu.modal.modal-edit-data')
@include('menu.modal.modal-tambah-data')
@include('menu.modal.modal-hapus-data')

<script type="text/javascript">

function setEdit(id) {
	let idku = id.split('||');
	console.log(idku);
	fetch('../komposisi/'+idku[0]+'', {
				method: 'get'
			})
	.then(response => response.json())
	.then(data => {
		console.log(data);
		let komposisi = '';
		for (let i = 0; i < data.length; i++) {
            komposisi += '<div class ="row mb-5" id ="editKomposisi">'+
				'<div class="col-md-3 fv-row">'+
					'<label class="fs-5 fw-bold mb-2">Katagori</label>'+
					'<input type="hidden" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" name="komposisi_id[]" value="'+data[i]['id']+'"/>'+
					// '<input type="text" class="form-control form-control-solid mb-3 mb-lg-0" id="typeKom" placeholder="" name="type[]" value="update"/>'+
					'<select class="form-select form-select-solid" name="kategori[]" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true">'+
						'<option value="'+data[i]['kategori']+'">'+data[i]['kategori']+'</option>'+
						'@foreach ($kategoris as $key => $kategori)'+
							'<option value="{{$kategori->nama_kategori}}">{{$kategori->nama_kategori}}</option>'+
						'@endforeach'+
					'</select>'+
				'</div>'+
				'<div class="col-md-2 fv-row">'+
					'<label class="fs-5 fw-bold mb-2">Brand</label>'+
					'<select class="form-select form-select-solid" name="brand[]" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true">'+
						'<option value="'+data[i]['brand_komposisi']+'">'+data[i]['brand_komposisi']+'</option>'+
						'@foreach ($brand as $key => $brands)'+
							'<option value="{{$brands->brand_inventory}}">{{$brands->brand_inventory}}</option>'+
						'@endforeach'+
					'</select>'+
				'</div>'+
				'<div class="col-md-2 fv-row">'+
					'<label class="fs-5 fw-bold mb-2">qty</label>'+
					'<input type="number" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" name="jumlah[]" value="'+data[i]['qty']+'"/>'+
				'</div>'+
				'<div class="col-md-2 fv-row">'+
					'<label class="fs-5 fw-bold mb-2">Satuan</label>'+
					'<select class="form-select form-select-solid" name="satuan[]" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true">'+
						'<option value="'+data[i]['satuan']+'">'+data[i]['satuan']+'</option>'+
						'<option value="ml">ml</option>'+
						'<option value="gram">gram</option>'+
					'</select>'+
				'</div>'+
				// '<div class="col-md-2 fv-row">'+
					// '<div class="form-check">'+
						// '<input class="form-check-input" type="checkbox" value="'+data[i]['id']+'" id="'+data[i]['id']+'" name="hapusKomposisi[]">'+
						// '<label class="form-check-label" for="'+data[i]['id']+'">'+
						// 	'Hapus'+
						// '</label>'+
					// '</div>'
				// '</div>'
				'<div class="col-md-2 text-center mb-5">'+
				// 	'<a href="#!" class="btn_tambah_komposisi"><i class="fas fa-plus text-success me-2"></i></a>'+
					// '<a href="#!" class="removekomposisi" ><i class="fas fa-trash text-danger"></i></a>'+
					'<input class="form-check-input" type="checkbox" value="'+data[i]['id']+'" id="'+data[i]['id']+'" name="hapusKomposisi[]">'+
						'<label class="form-check-label" for="'+data[i]['id']+'">'+
							'Hapus'+
						'</label>'+
				'</div>'+
			'</div>'
			
			
		}
		
		$(document).on('click', '.removekomposisi', function(){
			$(this).closest("#editKomposisi").remove();
    	});
		$('.komposisi_edit').html(komposisi);
	}).catch(err => console.log(err));
	$('.data-edit-id').val(idku[0]);
	$('.data-edit-nama').val(idku[1]);
	$('.data-edit-kategori-menu').val(idku[5]).change();
	$('.data-edit-harga').val(idku[2]);
	if (idku[3] == 'minum') {
		$('.minumanChecked').prop('checked', true);
	}else if(idku[3] == 'makan') {
		$('.makananChecked').prop('checked', true);
	}else if(idku[3] == 'add') {
		$('.addOnChecked').prop('checked', true);
	}
	console.log(idku[5]);
	let gambar = '';
	if (idku[4] == 'empty') {
		gambar = 'dummy.jpg';
	}else{
		gambar = idku[4];
	}
	// $('.data-edit-kategori').change(idku[4]);
	$('.input-data-foto2').val(idku[4]);
	$('.input-data-foto').css("background-image", "url(../berkas/menu/"+ gambar +")");
	

	// $('.pesan').html('Apakah Anda Yakin Menghapus Menu "' + idku[1] +'" ?');
}
$(".removeAvatar").click(function(){
		$('.input-data-foto2').val(null);
	});
	$('input[type="file"]').change(function(e) {
        let fileName = e.target.files[0].name;
		$('.input-data-foto2').val(fileName);
    });
function setHapus(id) {
	let idku = id.split('||');
	$('.hapusMenu').val(idku[0]);
	$('.pesan').html('Apakah Anda Yakin Menghapus Menu "' + idku[1] +'" ?');
}
	$(document).ready(function() {
		function childRowKosong(){
			return 'tidak ada komposisi';
		}
		function childRow (data) {
			var isi = '';
			for (var i = 0; i < data.length; i++) {
				isi +='<div class="d-flex flex-stack">'+
								'<div class="d-flex align-items-center me-5">'+
									'<div class="me-5">'+
										'<a href="#" class="text-gray-800 fw-bolder text-hover-primary fs-6">'+data[i]['kategori_komposisi']+'</a>'+
										'<br><a href="#" class="text-gray-800 fw-bolder text-hover-primary fs-6">'+data[i]['brand_komposisi']+'</a>'+
										// '<span class="text-gray-400 fw-bold fs-7 d-block text-start ps-0">234Kg Terpakai</span>'+
									'</div>'+
								'</div>'+
								'<div class="text-gray-400 fw-bolder fs-7 text-end">'+
								'<span class="text-gray-800 fw-bolder fs-6 d-block">'+data[i]['jumlah_komposisi']+' '+data[i]['satuan_komposisi']+'</span>'+
								'</div>'+
							'</div>'+
							'<div class="separator separator-dashed my-5"></div>'
			}
			return 	'<table class="table table-row-bordered gy-5">'+
			'<tbody>'+
				'<tr>'+
					'<td></td>'+
					'<td>'+
					isi+
					'</td>'+
					'<td></td>'+
					'<td></td>'+
					'<td></td>'+
					'<td></td>'+
					'<td></td>'+
					'<td></td>'+
				'</tr>'+
				'</tbody>'+
				'</table>';
		}
		var table = $('#kt_datatable_example_1').DataTable( {
			"language": {
				"lengthMenu": "Show _MENU_",
			},
			"dom":
			"<'row'" +
			"<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
			"<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
			">" +

			"<'table-responsive'tr>" +

			"<'row'" +
			"<'col-sm-12 col-xl-5 d-flex align-items-center justify-content-center justify-content-xl-start'i>" +
			"<'col-sm-12 col-xl-7 d-flex align-items-center justify-content-center justify-content-xl-end'p>" +
			">"
		} );
		// Add event listener for opening and closing details
    $('#kt_datatable_example_1 tbody').on('click', '.showDetailRow', function () {
			var data = JSON.parse($(this).attr('id'));
			console.log(data);
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
			if (data.length == 0) {
				row.child( childRowKosong() ).show();
				tr.addClass('shown');
			}else {
				row.child( childRow(data) ).show();
	            tr.addClass('shown');
			}

        }
    } );
	});
</script>

@endsection
