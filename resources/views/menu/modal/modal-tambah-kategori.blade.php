<div class="modal fade" id="modal_tambah_kategori" tabindex="-1" aria-hidden="true">
	<!--begin::Modal dialog-->
	<div class="modal-dialog modal-dialog-centered mw-950px">
		<!--begin::Modal content-->
		<div class="modal-content">
			<!--begin::Modal header-->
			<div class="modal-header" id="kt_modal_add_user_header">
				<!--begin::Modal title-->
				<h2 class="fw-bolder">Tambah Kategori</h2>
				<!--end::Modal title-->
				<!--begin::Close-->
				<div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</div>
				<!--end::Close-->
			</div>
			<!--end::Modal header-->
			<!--begin::Modal body-->
			<div class="modal-body scroll-y mx-5 mx-xl-15 my-7">
				<!--begin::Form-->
				<form class="form" action="/menu-tambah-kategori" method="POST" enctype="multipart/form-data">
					@csrf
					<!--begin::Scroll-->
					<div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_add_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_user_header" data-kt-scroll-wrappers="#kt_modal_add_user_scroll" data-kt-scroll-offset="300px">
						<!--begin::Input group-->
						<div class="row mb-5">
							<div class="col-md-12 fv-row">
								<label class="required fs-5 fw-bold mb-2">Kategori</label>
								<input type="text" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" name="kategori_menu" />
							</div>
						</div>
						<!--end::Input group-->
					</div>
					<!--end::Scroll-->
					<!--begin::Actions-->
					<div class="text-center pt-15">
						<button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal" aria-label="Close">Batal</button>
						<button type="submit" class="btn btn-primary" data-kt-users-modal-action="submit">
							<span class="indicator-label">Tambah</span>
							<span class="indicator-progress">Mohon Tunggu...
							<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
						</button>
					</div>
					<!--end::Actions-->
				</form>
				<!--end::Form-->
			</div>
			<!--end::Modal body-->
		</div>
		<!--end::Modal content-->
	</div>
	<!--end::Modal dialog-->
</div>
<script>
	
$(document).ready(function() {
	$("#customer-new").select2({
		tags: true,
		dropdownParent: $("#modal_tambah"),
		createTag: function (params) {
		return {
			id: params.term,
			text: params.term,
			newOption: true
		}
	},
	 templateResult: function (data) {
		var $result = $("<span></span>");

		$result.text(data.text);

		if (data.newOption) {
			$result.append(" <em>(new)</em>");
		}

		return $result;
	}

	});
});
	let by = 1;
    tambahKompisi(by);
    function tambahKompisi(number) {
        // let pilih = 'selectpicker';
        let CRtambahKompisi = '';
            CRtambahKompisi += '<div class ="row mb-5" id ="tambahCameraNew">'+
				'<div class="col-md-3 fv-row">'+
					'<label class="fs-5 fw-bold mb-2">Katagori</label>'+
					'<select class="form-select form-select-solid" name="kategori_komposisi[]" >'+
						'<option></option>@php$kategoris = App\KategoriInventory::all();@endphp'+
						'@foreach ($kategoris as $key => $kategori)<option value="{{$kategori->nama_kategori}}">{{$kategori->nama_kategori}}</option>@endforeach'+
					'</select>'+
				'</div>'+
				'<div class="col-md-2 fv-row">'+
					'<label class="fs-5 fw-bold mb-2">Brand</label>'+
					'<select class="form-select form-select-solid" name="brand[]" >'+
						'<option></option>'+
						'@foreach ($brand as $key => $merek)<option value="{{$merek->brand_inventory}}">{{$merek->brand_inventory}}</option>@endforeach'+
					'</select>'+
				'</div>'+
				'<div class="col-md-2 fv-row">'+
					'<label class="fs-5 fw-bold mb-2">qty</label>'+
					'<input type="number" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" name="jumlah_komposisi[]" />'+
				'</div>'+
				'<div class="col-md-2 fv-row">'+
					'<label class="fs-5 fw-bold mb-2">Satuan</label>'+
					'<select name="satuan_komposisi[]" class="required form-select form-select-solid" data-placeholder="Pilih Satuan Ukuran" data-allow-clear="true">'+
						'<option value="ml">ml</option>'+
						'<option value="gram">gram</option>'+
					'</select>'+
				'</div>'+
				'<div class="col-md-2 text-center mb-5">'+
					'<a href="#!" class="btn_tambah_komposisi"><i class="fas fa-plus text-success me-2"></i></a>'+
					'<a href="#!" class="remove" ><i class="fas fa-trash text-danger"></i></a>'+
				'</div>'+
			'</div>';
        if (number > 1) {
            $('.new-komposisi').append(CRtambahKompisi);
        }
    }
    $(document).on('click', '.btn_tambah_komposisi', function() {
        by++;
        tambahKompisi(by);
        // $('.pilih').selectpicker();
    });
    $(document).on('click', '.remove', function(){
        by--;
        $(this).closest("#tambahCameraNew").remove();
    });
</script>
