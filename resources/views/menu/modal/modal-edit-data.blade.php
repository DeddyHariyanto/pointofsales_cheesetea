<div class="modal fade" id="modal_edit" tabindex="-1" aria-hidden="true">
	<!--begin::Modal dialog-->
	<div class="modal-dialog modal-dialog-centered mw-950px">
		<!--begin::Modal content-->
		<div class="modal-content">
			<!--begin::Modal header-->
			<div class="modal-header" id="kt_modal_add_user_header">
				<!--begin::Modal title-->
				<h2 class="fw-bolder">Edit Menu</h2>
				<!--end::Modal title-->
				<!--begin::Close-->
				<div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</div>
				<!--end::Close-->
			</div>
			<!--end::Modal header-->
			<!--begin::Modal body-->
			<div class="modal-body scroll-y mx-5 mx-xl-15 my-7">
				<!--begin::Form-->
				<form id="kt_modal_add_user_form" class="form" action="/menu-edit" method="POST" enctype="multipart/form-data">
					@csrf
					<!--begin::Scroll-->
					<input class="input-data-foto2" name="input-data-foto2" hidden>
					<div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_add_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_user_header" data-kt-scroll-wrappers="#kt_modal_add_user_scroll" data-kt-scroll-offset="300px">
						<!--begin::Input group-->
						<div class="text-center fv-row mb-7">
							<!--begin::Label-->
							<label class="d-block fw-bold fs-6 mb-5 text-center">Thumbnail</label>
							<!--end::Label-->
							<!--begin::Image input-->
							<div class="image-input image-input-outline text-center" data-kt-image-input="true" style="background-image: url('berkas/menu/dummy.jpg')">
								<!--begin::Preview existing avatar-->
								<div class="image-input-wrapper w-200px h-200px input-data-foto"></div>
								<!--end::Preview existing avatar-->
								<!--begin::Label-->
								<label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
									<i class="bi bi-pencil-fill fs-7"></i>
									<!--begin::Inputs-->
									<input type="file" name="avatar" accept=".png, .jpg, .jpeg" />
									<input type="hidden" name="avatar_remove" />
									<!--end::Inputs-->
								</label>
								<!--end::Label-->
								<!--begin::Cancel-->
								<span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
									<i class="bi bi-x fs-2"></i>
								</span>
								<!--end::Cancel-->
								<!--begin::Remove-->
								<span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow removeAvatar" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
									<i class="bi bi-x fs-2"></i>
								</span>
								<!--end::Remove-->
							</div>
							<!--end::Image input-->
							<!--begin::Hint-->
							<div class="form-text">Allowed file types: png, jpg, jpeg.</div>
							<!--end::Hint-->
						</div>
						<div class="row mb-5">
							<div class="col-md-6 fv-row">
								<label class="required fs-5 fw-bold mb-2">Nama Produk</label>
                                <input type="hidden" name="foto" class="form-control form-control-solid mb-3 mb-lg-0 input-data-foto2" />
								<input type="hidden" class="form-control form-control-solid mb-3 mb-lg-0 data-edit-id" placeholder="" name="id" />
								<input type="text" class="form-control form-control-solid mb-3 mb-lg-0 data-edit-nama" placeholder="" name="name" />
							</div>
							<div class="col-md-6 fv-row">
								<label class="required fs-5 fw-bold mb-2">Harga</label>
								<input type="number" class="form-control form-control-solid mb-3 mb-lg-0 data-edit-harga" placeholder="" name="harga" />
							</div>
							<div class="col-md-12 fv-row">
								<label for="diskon" class="form-label">Kategori</label>
                                  <select class="form-select form-select-solid data-edit-kategori-menu" name="kategori_menu" id="customer-new2" data-placeholder="silahkan isi kategori menu" data-allow-clear="true">
                                    <option></option>
                                    @php
                                        $kategoriMenu = App\kategoriMenu::get();
                                    @endphp
                                    @foreach ($kategoriMenu as $key => $kategoriMenus)
                                        <option value="{{$kategoriMenus->name}}">{{$kategoriMenus->name}}</option>
                                    @endforeach
                                </select>
							</div>
						</div>
						<!--end::Input group-->
						<!--begin::Input group-->
						<div class="row mb-7">
							<!--begin::Label-->
							<label class="required fw-bold fs-6 mb-5">Tipe</label>
							<div class="col-md-4">
								<div class="fv-row">
									<div class="form-check form-check-custom form-check-solid">
										<input class="form-check-input me-3 minumanChecked" name="tipe" type="radio" value="minum" id="kt_modal_update_role_option_0" checked/>
										<label class="form-check-label" for="kt_modal_update_role_option_0">
											<div class="fw-bolder text-gray-800">Minuman</div>
											<div class="text-gray-600">Untuk Produk tipe Minuman</div>
										</label>
									</div>
								</div>
							</div>
							<div class='separator separator-dashed my-5'></div>
								<div class="col-md-4">
									<div class="fv-row">
										<div class="form-check form-check-custom form-check-solid">
											<input class="form-check-input me-3 makananChecked" name="tipe" type="radio" value="makan" id="kt_modal_update_role_option_0" checked/>
											<label class="form-check-label" for="kt_modal_update_role_option_0">
												<div class="fw-bolder text-gray-800">Makanan</div>
												<div class="text-gray-600">Untuk Produk tipe Makanan</div>
											</label>
										</div>
									</div>
								</div>
							<div class='separator separator-dashed my-5'></div>
							<div class="col-md-4">
								<div class="fv-row">
									<div class="form-check form-check-custom form-check-solid">
										<input class="form-check-input me-3 addOnChecked" name="tipe" type="radio" value="add" id="kt_modal_update_role_option_0" checked/>
										<label class="form-check-label" for="kt_modal_update_role_option_0">
											<div class="fw-bolder text-gray-800">Add on</div>
											<div class="text-gray-600">Untuk Produk tipe Tambahan</div>
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="notice d-flex bg-light-warning rounded border-warning border border-dashed mb-9 p-6">
                            <span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
                                    <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black" />
                                    <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black" />
                                </svg>
                            </span>
                            <div class="d-flex flex-stack flex-grow-1">
                                <div class="fw-bold">
                                    <h5 class="text-gray-900 fw-bolder mb-0">Perhatian</h5>
                                    <div class="fs-6 text-gray-700">Jika Ingin Menghapus Komposisi Silahkan Check "Hapus"</div>
                                </div>
                            </div>
                        </div>
						<div class="komposisi_edit"></div>
						<!--end::Input group-->
						<div class='separator separator-dashed my-5'></div>
						<div class="row mb-5">
							<div class="col-md-3 fv-row">
								<label class="fs-5 fw-bold mb-2">Katagori</label>
								<select class="form-select form-select-solid kategori" name="kategori_komposisi_new[]" data-placeholder="Pilih atau masukan Kategori baru" data-allow-clear="true">
									<option></option>
									@php
										$kategorisInven = App\KategoriInventory::all();
									@endphp
									@foreach ($kategorisInven as $key => $kategorisInvens)
										<option value="{{$kategorisInvens->name}}">{{$kategorisInvens->name}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-md-2 fv-row">
								<label class="fs-5 fw-bold mb-2">Brand</label>
								<select class="form-select form-select-solid kategori" name="brand_komposisi_new[]" data-placeholder="Pilih atau masukan Kategori baru" data-allow-clear="true">
									<option></option>
									@php
										$brand = App\Inventory::groupBy('brand_inventory')->get();
									@endphp
									@foreach ($brand as $key => $merek)
										<option value="{{$merek->brand_inventory}}">{{$merek->brand_inventory}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-md-2 fv-row">
								<label class="fs-5 fw-bold mb-2">qty</label>
								<input type="number" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" name="jumlah_komposisi_new[]" />
							</div>
							<div class="col-md-2 fv-row">
								<label class="fs-5 fw-bold mb-2">Satuan</label>
								<select name="satuan_komposisi_new[]" class="required form-select form-select-solid" data-placeholder="Pilih Satuan Ukuran" data-allow-clear="true">
									<option value="ml">ml</option>
									<option value="gram">gram</option>
								</select>
							</div>
							<div class="col-md-2 text-center">
								<a href="#!" class="btn_edit_komposisi"><i class="fas fa-plus text-success me-2"></i></a>
								{{-- <a href="#!" class="remove" ><i class="fas fa-trash text-danger"></i></a>	 --}}
							</div>
						</div>
						<div class="edit-komposisi"></div>
					</div>
					<!--end::Scroll-->
					<!--begin::Actions-->
					<div class="text-center pt-15">
						<button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal" aria-label="Close">Batal</button>
						<button type="submit" class="btn btn-primary" data-kt-users-modal-action="submit">
							<span class="indicator-label">Tambah</span>
							<span class="indicator-progress">Mohon Tunggu...
							<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
						</button>
					</div>
					<!--end::Actions-->
				</form>
				<!--end::Form-->
			</div>
			<!--end::Modal body-->
		</div>
		<!--end::Modal content-->
	</div>
	<!--end::Modal dialog-->
</div>
<script>
$(document).ready(function() {
	// $("#customer-new2").select2({
	// 	tags: true,
	// 	dropdownParent: $("#modal_tambah2"),
	// 	createTag: function (params) {
	// 	return {
	// 		id: params.term,
	// 		text: params.term,
	// 		newOption: true
	// 	}
	// },
	//  templateResult: function (data) {
	// 	var $result = $("<span></span>");

	// 	$result.text(data.text);

	// 	if (data.newOption) {
	// 		$result.append(" <em>(new)</em>");
	// 	}

	// 	return $result;
	// }

	// });
});
	let noEdit = 1;
    editKompisi(noEdit);
    function editKompisi(numberEdit) {
        // let pilih = 'selectpicker';
        let CReditKompisi = '';
            CReditKompisi += '<div class ="row mb-5" id ="editCameraNew">'+
				'<div class="col-md-3 fv-row">'+
					'<label class="fs-5 fw-bold mb-2">Katagori</label>'+
					'<select class="form-select form-select-solid" name="kategori_komposisi_new[]" >'+
						'<option></option>'+
						'@foreach ($kategoris as $key => $kategori)<option value="{{$kategori->nama_kategori}}">{{$kategori->nama_kategori}}</option>@endforeach'+
					'</select>'+
				'</div>'+
				'<div class="col-md-2 fv-row">'+
					'<label class="fs-5 fw-bold mb-2">Brand</label>'+
					'<select class="form-select form-select-solid" name="brand_komposisi_new[]" >'+
						'<option></option>'+
						'@foreach ($brand as $key => $merek)<option value="{{$merek->brand_inventory}}">{{$merek->brand_inventory}}</option>@endforeach'+
					'</select>'+
				'</div>'+
				'<div class="col-md-2 fv-row">'+
					'<label class="fs-5 fw-bold mb-2">qty</label>'+
					'<input type="number" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" name="jumlah_komposisi_new[]" />'+
				'</div>'+
				'<div class="col-md-2 fv-row">'+
					'<label class="fs-5 fw-bold mb-2">Satuan</label>'+
					'<select name="satuan_komposisi_new[]" class="required form-select form-select-solid" data-placeholder="Pilih Satuan Ukuran" data-allow-clear="true">'+
						'<option value="ml">ml</option>'+
						'<option value="gram">gram</option>'+
					'</select>'+
				'</div>'+
				'<div class="col-md-2 text-center mb-5">'+
					'<a href="#!" class="btn_edit_komposisi"><i class="fas fa-plus text-success me-2"></i></a>'+
					'<a href="#!" class="remove" ><i class="fas fa-trash text-danger"></i></a>'+
				'</div>'+
			'</div>';
        if (numberEdit > 1) {
            $('.edit-komposisi').append(CReditKompisi);
        }
    }
    $(document).on('click', '.btn_edit_komposisi', function() {
        noEdit++;
        editKompisi(noEdit);
        // $('.pilih').selectpicker();
    });
    $(document).on('click', '.remove', function(){
        noEdit--;
        $(this).closest("#editCameraNew").remove();
    });
</script>