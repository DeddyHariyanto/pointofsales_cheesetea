@extends('layouts.app')

@section('sidebar')
@include('layouts.sidebar.sidebar')
@endsection

@section('header')
@include('layouts.header.header')
@endsection

@section('content')
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-fluid">
	<div class="content flex-row-fluid" id="kt_content">
		<div class="row justify-content-center">
					<div class="col-xl-12">
						<div class="row gy-0 gx-10">
							<div class="col-12">
								<div class="row">
									<div class="col-xl-8 col-6">
										<ul class="nav row mb-8">
											<li class="nav-item col-4 mb-5 mb-lg-0">
												<a class="nav-link btn btn-flex btn-color-muted btn-outline btn-outline-default btn-active-dark d-flex flex-grow-1 flex-column flex-center py-8 active" data-bs-toggle="tab" href="#kt_general_widget_1_1">
													<span class="svg-icon svg-icon-3x mb-5 mx-0">
														{{-- <i class="fas fa-hamburger fs-1"></i> --}}
													</span>
													<span class="fs-6 fw-bold">Halo.. {{Auth::user()->nama}}</span>
												</a>
											</li>
										</ul>
												
													</div>
													
														</div>
													</div>
												</div>
												
												</div>
											</div>	
										</div>
									</div>
								</div>
							</div>
						</div>

@endsection