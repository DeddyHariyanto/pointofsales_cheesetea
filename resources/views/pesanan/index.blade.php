@extends('layouts.app')

@section('sidebar')
@include('layouts.sidebar.sidebar')
@endsection

@section('header')
@include('layouts.header.header')
@endsection

@section('content')
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-fluid">
	<div class="content flex-row-fluid" id="kt_content">
		<div class="row justify-content-center">
			<div class="col-xl-12">
				<div class="row justify-content-center gy-0 gx-10">
					<div class="col-xl-12 mb-4">
						<div class="toolbar d-flex flex-stack mb-0" id="kt_toolbar">
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack flex-wrap">
								<div class="page-title d-flex flex-column me-3">
									<h1 class="d-flex text-dark fw-bolder my-1 fs-3">List Pesanan</h1>
								</div>
								<div class="card-toolbar">
									<ul class="nav">
										<li class="nav-item">
											<a class="nav-link btn btn-sm btn-color-muted btn-active btn-active-light-primary fw-bolder px-4 me-1 {{ $status === "invalid" ? "active" : "" }}" href="/pesanan-index/invalid">Pesanan Dibuat</a>
										</li>
										<li class="nav-item">
											<a class="nav-link btn btn-sm btn-color-muted btn-active btn-active-light-primary fw-bolder px-4 me-1 {{ $status === "selesai" ? "active" : "" }}" href="/pesanan-index/selesai">Pesanan Selesai</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-12 mb-4">
						<table id="kt_datatable_example_1" class="table table-row-bordered gy-5">
							<thead>
								<tr class="fw-bolder text-muted">
									<th class="min-w-150px">No</th>
									<th class="min-w-150px">Code</th>
									<th class="min-w-150px">Customer</th>
									<th class="min-w-120px">status</th>
									<th class="min-w-100px text-end">Actions</th>
								</tr>
							</thead>
							<tbody>
								@for ($i = 0; $i < count($data); $i++)
									<tr>
										<td>{{$i + 1}}</td>
										<td>
											<div class="d-flex align-items-center">
												<div class="d-flex justify-content-start flex-column">
													<a href="#" class="text-dark fw-bolder text-hover-primary fs-6">#{{$data[$i]['code']}}</a>
												</div>
											</div>
										</td>
										<td>
											<div class="d-flex align-items-center">
												<div class="d-flex justify-content-start flex-column">
													<a href="#" class="text-dark fw-bolder text-hover-primary fs-6">{{$data[$i]['customer']}}</a>
													{{-- <span class="text-muted fw-bold text-muted d-block fs-7">Rp.{{number_format($menu->harga_menu,0)}}</span> --}}
												</div>
											</div>
										</td>
										<td>
											<div class="d-flex align-items-center">
												<div class="d-flex justify-content-start flex-column">
													@if ($data[$i]['status'] == 'invalid')
														<span class="badge badge-light-warning fw-bold me-1 fs-6">Pesana Sedang Di Buat</span>
													@elseif($data[$i]['status'] == 'selesai')
														<span class="badge badge-light-info fw-bold me-1 fs-6">Pesana Selesai</span>
													@endif
												</div>
											</div>
										</td>
										<td>
											<div class="d-flex justify-content-end flex-shrink-0">
												<div class="menu-item px-3">
													<button class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-overflow="true">
														<span class="svg-icon svg-icon-1">
															<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4" fill="black" />
																<rect x="11" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
																<rect x="15" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
																<rect x="7" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
															</svg>
														</span>
													</button>
													<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px" data-kt-menu="true">
														<div class="menu-item px-3">
															<div class="menu-content fs-6 text-dark fw-bolder px-3 py-4">Quick Actions</div>
														</div>
														<div class="separator mb-3 opacity-75"></div>
															<div class="menu-item px-3">
																<a href="/kasir-struk/{{$data[$i]['id']}}" target="_blank"class="menu-link px-3" ></i>Struck</a>
															</div>
															@if($data[$i]['status'] == 'invalid')
																<div class="menu-item px-3 validasi" >
																	<form action="/pesanan-selesai" method="post" id="input-konfirmasi-pesanan"enctype="multipart/form-data">
																		@csrf
																		<input type="hidden" name="idKonfirmasiPesanan" value="{{$data[$i]['id']}}">
																		{{-- <div type="submit"class="menu-link px-3" onclick="document.getElementById('input-konfirmasi-pesanan').submit();">Selesai</div> --}}
																		<button type="submit" class="menu-link px-3 btn btn-white" >Selesai</button>
																	</form>
																</div>
																<div class="menu-item px-3 validasi" >
																	<div class="menu-item px-3">
																		<a href="#modalHapusPesanan" data-bs-toggle="modal" class="menu-link px-3" id="{{$data[$i]['id']}}" onclick="setHapusPesanan(this.id)"></i>Hapus Pesanan</a>
																	</div>
																</div>
															@endif
															@if ($data[$i]['email'] == null)
																<div class="menu-item px-3">
																	<a href="#modal_kirim_email" data-bs-toggle="modal" class="menu-link px-3" id="{{$data[$i]['id']}}" onclick="setKirimEmail(this.id)"></i>Kirim Email</a>
																</div>
															@else
																<div class="menu-item px-3 validasi" >
																	<form action="/kasir-kirim-struk" method="post" id="input-validasi"enctype="multipart/form-data">
																		@csrf

																		<input type="hidden" name="code"  value="1">
																		<input type="hidden" name="email"  value="{{$data[$i]['email']}}">
																		<input type="hidden" name="id" value="{{$data[$i]['id']}}">
																		{{-- <a type="submit"class="menu-link px-3" onclick="document.getElementById('input-validasi').submit();">Kirim Email</a> --}}
																		<button type="submit" class="menu-link px-3 btn btn-white">Kirim Email</button>
																	</form>
																</div>
															@endif
														</div>
												</div>
												<div class="menu-item px-3">
												{{-- <a href="#modal_edit" data-bs-toggle="modal" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="Klik untuk mengubah data" id="{{$menu['id']}}||{{$menu['nama_menu']}}||{{$menu['harga_menu']}}||{{$menu['tipe_menu']}}||{{$menu['foto_menu']}}" onclick="setEdit(this.id)">
													<i class="fas fa-edit"></i>
												</a>
												<a href="#modalHapusDataKaryawan" data-bs-toggle="modal" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="Klik untuk menghapus menu"  id="{{$menu->id}}||{{$menu->nama_menu}}" onclick="setHapus(this.id)">
													<i class="fas fa-trash"></i>
												</a> --}}
												@php
													$item = DB::table('detail_transaksis')
																->leftJoin('menus','menus.id','=','detail_transaksis.id_menu')
																// ->leftJoin('komposisis','komposisis.menu_id','=','detail_transaksis.id_menu')
																->where('id_transaksi',$data[$i]['id'])
																->groupBy('detail_transaksis.id_menu')
																->select(
																	'menus.id',
																	'menus.foto_menu',
																	'detail_transaksis.nama',
																	'detail_transaksis.harga',
																	DB::raw('count(detail_transaksis.id_menu) as qty'))->get();
												@endphp
												<button  class="showDetailRow btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1" id="{{$item}}">
													<i class="fas fa-angle-double-down"></i>
												</button>
											</div>

										</td>
									</tr>
								@endfor
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('pesanan.modal.modal-tambah-data')
@include('pesanan.modal.modal-hapus-data')
<script>

function setKirimEmail(id) {
	
	$('.idTransaksi').val(id);
}
function setHapusPesanan(id) {
	
	$('.hapusPesanan').val(id);
}
 </script>

<script type="text/javascript">
	$(document).ready(function() {
		function childRowKosong(){
			return 'tidak ada komposisi';
		}
		function childRow (data) {
			var isi = '';
			for (var i = 0; i < data.length; i++) {
				console.log(data[i]);
				isi +='<div class="d-flex align-items-center">'+
						'<div class="symbol symbol-45px me-5">'+
							'<img src="../berkas/menu/'+ data[i]['foto_menu'] +'" class="" alt="" />'+
						'</div>'+
						'<div class="d-flex justify-content-start flex-column">'+
							'<div class="me-5">'+
								'<a href="#" class="text-gray-800 fw-bolder text-hover-primary fs-6">'+data[i]['nama']+'</a>'+
								'<span class="text-muted fw-bold text-muted d-block fs-7">Rp. '+data[i]['harga']+'</span>'+
							'<span class="text-dark fw-bolder text-hover-primary d-block mb-1 fs-6">'+data[i]['qty']+'x</span>'+
							'</div>'+
						'</div>'+
						'<div class="text-gray-400 fw-bolder fs-7 text-end">'+
							'<span class="text-gray-800 fw-bolder fs-6 d-block">Rp. '+(data[i]['qty'] * data[i]['harga'])+'</span>'+
						'</div>'+
					'</div>'+
					'<div class="separator separator-dashed my-5"></div>'

			}

			return 	'<table class="table table-row-bordered gy-5">'+
			'<tbody>'+
				'<tr>'+
					'<td></td>'+
					'<td>'+isi+'</td>'+
					'<td></td>'+
					'<td></td>'+
					'<td></td>'+
					'<td></td>'+
					'<td></td>'+
					'<td></td>'+
				'</tr>'+
				'</tbody>'+
				'</table>';
		}
		var table = $('#kt_datatable_example_1').DataTable( {
			"language": {
				"lengthMenu": "Show _MENU_",
			},
			"dom":
			"<'row'" +
			"<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
			"<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
			">" +

			"<'table-responsive'tr>" +

			"<'row'" +
			"<'col-sm-12 col-xl-5 d-flex align-items-center justify-content-center justify-content-xl-start'i>" +
			"<'col-sm-12 col-xl-7 d-flex align-items-center justify-content-center justify-content-xl-end'p>" +
			">"
		} );
		// Add event listener for opening and closing details
    $('#kt_datatable_example_1 tbody').on('click', '.showDetailRow', function () {
		var data = JSON.parse($(this).attr('id'));
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
			if (data.length == 0) {
				row.child( childRowKosong() ).show();
				tr.addClass('shown');
			}else {
				row.child( childRow(data) ).show();
	            tr.addClass('shown');
			}

        }
    } );
	});
</script>

@endsection
