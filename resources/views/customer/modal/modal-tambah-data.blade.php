<div class="modal fade" id="modal_tambah" tabindex="-1" aria-hidden="true">
											<div class="modal-dialog modal-dialog-centered mw-650px">
												<div class="modal-content">
													<div class="modal-header" id="kt_modal_add_user_header">
														<h2 class="fw-bolder judulForm">Tambah Karyawan</h2>
														<div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal" aria-label="Close">
															<span class="svg-icon svg-icon-1">
																<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																	<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
																	<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
																</svg>
															</span>
														</div>
													</div>

													<div class="modal-body scroll-y mx-5 mx-xl-15 my-7">
														<form id="kt_modal_add_user_form" class="form" method="post" action="{{url('/customer-post')}}" autocomplete="off" enctype= multipart/form-data>
															@csrf @method('post')
															<input class="id_user" name="id_user" hidden>
															<input class="code_post" name="code_post" hidden>
															<input class="judul_foto_user" name="judul_foto_user" hidden>
															<div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_add_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_user_header" data-kt-scroll-wrappers="#kt_modal_add_user_scroll" data-kt-scroll-offset="300px">
																{{-- <div class="fv-row mb-7">
																	<label class="d-block fw-bold fs-6 mb-5">foto</label>
																	<div class="image-input image-input-outline" data-kt-image-input="true" style="background-image: url('assets/media/svg/avatars/blank.svg')">
																		<div class="image-input-wrapper w-125px h-125px foto_user" style="background-image: url(assets/media/avatars/blank.png);"></div>
																		<label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
																			<i class="bi bi-pencil-fill fs-7"></i>
																			<input type="file" name="foto_user" accept=".png, .jpg, .jpeg" />
																			<input type="hidden" name="avatar_remove" />
																		</label>
																		<span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
																			<i class="bi bi-x fs-2"></i>
																		</span>
																		<span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow removeAvatar" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
																			<i class="bi bi-x fs-2"></i>
																		</span>
																	</div>
																	<div class="form-text">Allowed file types: png, jpg, jpeg.</div>
																</div> --}}
																<div class="fv-row mb-7">
																	<label class="required fw-bold fs-6 mb-2">Nama Customer</label>
																	<input type="text" name="nama" class="form-control form-control-solid mb-3 mb-lg-0 nama" placeholder="nama lengkap" />
																</div>
																<div class="fv-row mb-7">
																	<label class="required fw-bold fs-6 mb-2">Email</label>
																	<input type="email" name="email" class="form-control form-control-solid mb-3 mb-lg-0 email" placeholder="example@domain.com"/>
																</div>
																<div class="fv-row mb-7">
																	<label class="required fw-bold fs-6 mb-2">No.Telepon</label>
																	<input type="text" name="no_tlpn" class="form-control form-control-solid mb-3 mb-lg-0 no_tlpn" placeholder="08...."/>
																</div>
																<div class="fv-row mb-7">
																	<label class=" fs-5 fw-bold mb-2">Alamat</label>
                                									<textarea class="form-control" name="alamat" rows="5" cols="90" required></textarea>
																</div>
																<div class="mb-7">
																	<label class="required fw-bold fs-6 mb-5">Role</label>

                                	<div class="d-flex fv-row">
																		<div class="form-check form-check-custom form-check-solid">
																			<input class="form-check-input me-3" name="level" type="radio" value="customer" id="kt_modal_update_role_option_0" checked='checked' />
																			<label class="form-check-label" for="kt_modal_update_role_option_0">
																				<div class="fw-bolder text-gray-800">Customer</div>
																				{{-- <div class="text-gray-600">Karyawan yang menangani penjualan pada sebuah gerai</div> --}}
																			</label>
																		</div>
																	</div>

                                  <div class='separator separator-dashed my-5'></div>

                                </div>
															</div>
															<div class="text-center pt-15">
																<button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal" aria-label="Close">Batal</button>
																<button type="submit" class="btn btn-primary" data-kt-users-modal-action="submit">
																	<span class="indicator-label">Simpan</span>
																	<span class="indicator-progress">Mohon Tunggu...
																	<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
																</button>
															</div>
														</form>
													</div>
												</div>
											</div>
										</div>
