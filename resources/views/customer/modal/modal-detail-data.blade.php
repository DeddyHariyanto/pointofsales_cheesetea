<div class="modal fade" id="modal_detail" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered mw-650px">
		<div class="modal-content">
			<div class="modal-header" id="kt_modal_add_user_header">
				<h2 class="fw-bolder judulForm">Detail Customer</h2>
				<div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal" aria-label="Close">
					<span class="svg-icon svg-icon-1">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
							<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
							<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
						</svg>
					</span>
				</div>
			</div>

			<div class="modal-body scroll-y mx-5 mx-xl-15 my-7">
					<div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_add_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_user_header" data-kt-scroll-wrappers="#kt_modal_add_user_scroll" data-kt-scroll-offset="300px">
						<div class="fv-row mb-7">
							<label class="required fw-bold fs-6 mb-2">Nama Customer</label>
							<input type="text" name="nama" class="form-control form-control-solid mb-3 mb-lg-0 set-nama-customer" placeholder="nama lengkap" readonly/>
						</div>
						<div class="fv-row mb-7">
							<label class="required fw-bold fs-6 mb-2">Email</label>
							<input type="email" name="email" class="form-control form-control-solid mb-3 mb-lg-0 set-email-customer" placeholder="example@domain.com" readonly/>
						</div>
						<div class="fv-row mb-7">
							<label class="required fw-bold fs-6 mb-2">No.Telepon</label>
							<input type="text" name="no_tlpn" class="form-control form-control-solid mb-3 mb-lg-0 set-no_tlpn-customer" placeholder="08...." readonly/>
						</div>
						<div class="fv-row mb-7">
							<label class=" fs-5 fw-bold mb-2">Alamat</label>
							<textarea class="form-control set-alamat-customer" name="alamat" rows="5" cols="90" readonly></textarea>
						</div>
					</div>
				{{-- </form> --}}
			</div>
		</div>
	</div>
</div>
