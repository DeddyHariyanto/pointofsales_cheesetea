@component('mail::message')

<h1>{{$title}}</h1>
<p>{{$pesan}}</p>

{{-- @component('mail::button', ['url' => ''])
Button Text
@endcomponent --}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
