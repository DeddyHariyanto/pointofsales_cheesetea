<?php

namespace App\Imports;

use App\jurnal;
use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class jurnalImport implements ToCollection, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public $count = 0;
    public $jurnal = [];

    public function collection(Collection $collection)
    {
        foreach ($collection as $row)
        {
            if($row['status'] == 'valid' || $row['status'] == 1)  {
                $this->jurnal[$this->count] = [
                    'id'                          => $row['code'],
                    'status'                       => $row['status'],
                ];
                ++$this->count;
            }
        }
    }
}
