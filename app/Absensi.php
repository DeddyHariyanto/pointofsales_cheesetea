<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absensi extends Model
{
    protected $fillable = [
        'absensis_user_id',
        'tanggal_absensi',
        'waktu_absensi',
        'lokasi',
        'latitude',
        'longitude',
        'foto_absensi',
        'status_absensi',
    ];
}
