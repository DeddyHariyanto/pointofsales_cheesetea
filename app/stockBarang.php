<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class stockBarang extends Model
{
    protected $fillable = [
        'kategori_barang',
        'inventory_id',
        'stock',
        'satuan',
    ];
}
