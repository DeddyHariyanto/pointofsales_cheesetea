<?php

namespace App\Exports;

use App\User;
use DB;
use App\Absensi;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class TransaksiAllExport implements FromView

{
    use Exportable;

    public function id(string $id)
    {
        $this->id = $id;

        return $this;
    }
    public function view(): View{
        $data =[];
        $struk = DB::table('transaksis')
                  ->leftJoin('users','transaksis.id_user_transaksi','=','users.id')
                  ->where('transaksis.id_gerai_transaksi',$this->id)
                //   ->whereDate('transaksis.created_at','=',Carbon::today())
                  ->select('transaksis.id as id',
                    'transaksis.code_transaksi as code',
                    'users.nama as kasir',
                    'transaksis.tipe_bayar_transaksi as tipe_bayar',
                    'transaksis.jumlah_bayar_transaksi as jumlah_bayar',
                    'transaksis.discount_transaksi as discount',
                    'transaksis.total_dibayar_transaksi as total_dibayar',
                    'transaksis.created_at as tanggal')->get();
        
        foreach ($struk as $key => $strukc) {
          
          $data[$key] = [
            'id' => $strukc->id,
            'code' => $strukc->code,
            'kasir' => $strukc->kasir,
            'tipe_bayar' => $strukc->tipe_bayar,
            'jumlah_bayar' => $strukc->jumlah_bayar,
            'discount' => $strukc->discount,
            'total_dibayar' => $strukc->total_dibayar,
            'tanggal' => Carbon::parse($strukc->tanggal)->format('d M Y'),
          ];
        }
        return view('kasir.export.transaksiAll')->with(compact('data'));
    }
}
