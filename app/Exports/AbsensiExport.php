<?php

namespace App\Exports;

use App\User;
use DB;
use App\Absensi;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class AbsensiExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    public function id(string $id)
    {
        $this->id = $id;

        return $this;
    }
    public function view(): View
    {
        $data =  DB::table('user_gerais')
                        ->leftJoin('gerais', 'user_gerais.id_gerai','=','gerais.id')
                        ->leftJoin('users', 'user_gerais.id_user','=','users.id')
                        ->leftJoin('absensis', 'user_gerais.id_user','=','absensis.absensis_user_id')
                        ->where('id_gerai',$this->id)
                        ->select(
                            'users.id','users.nama',
                            'gerais.nama_gerai',
                            'absensis.lokasi',
                            'absensis.tanggal_absensi',
                            'absensis.waktu_absensi',
                            'absensis.status_absensi'
                        )->get();
        return view('absensi.export.absensiAll')->with(compact('data'));
    }
}
