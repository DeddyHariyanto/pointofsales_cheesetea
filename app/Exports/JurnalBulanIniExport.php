<?php

namespace App\Exports;

use App\User;
use DB;
use App\Absensi;
use App\jurnal;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class JurnalBulanIniExport implements FromView
{
    use Exportable;

    public function id(string $id)
    {
        $this->id = $id;

        return $this;
    }
    public function view(): View{
        $data =[];
        $jurnal = jurnal::where('gerai_id',$this->id)
                        ->whereMonth('created_at','=',Carbon::now()->month)
                        ->get();
        foreach ($jurnal as $key => $jurnals) {
          $data[$key] = [
            'id' => $jurnals->id,
            'title' => $jurnals->title,
            'description' => $jurnals->description,
            'debit' => $jurnals->debit,
            'kredit' => $jurnals->kredit,
            'tanggal' => Carbon::parse($jurnals->created_at)->format('d M Y'),
            'status' => $jurnals->status,
          ];
        }
        return view('kasir.export.jurnalGerai')->with(compact('data'));
    }
}
