<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGerai extends Model
{
  protected $fillable = [
    'id_user',
    'id_gerai',
    'pin_user_gerai',
    'status_user_gerai',
  ];
}
