<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jurnal extends Model
{
    protected $fillable = [
        'type_id',
        'doc_id',
        'gerai_id',
        'title',
        'description',
        'debit',
        'kredit',
        'status',
    ];
}
