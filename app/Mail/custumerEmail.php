<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use App\email;

class custumerEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
  
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->email->attchment != 'empty') {
            return $this->from($this->email->email)
                        ->markdown('emails.payment')
                        ->with([
                            'title'          => $this->email->title,
                            'pesan'        => $this->email->pesan,
                        ])
                        ->attach(url('/storage/' . $this->email->attchment));
        }else{
            return $this->from($this->email->email)
                        ->markdown('emails.payment')
                        ->with([
                            'title'          => $this->email->title,
                            'pesan'        => $this->email->pesan,
                        ]);
        }
    }
}
