<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaksi extends Model
{
    protected $fillable = [
        'code_transaksi',
        'id_gerai_transaksi',
        'id_user_transaksi',
        'id_customer_transaksi',
        'tipe_bayar_transaksi',
        'no_rekening',
        'nama_rekening',
        'jumlah_bayar_transaksi',
        'discount_transaksi',
        'total_dibayar_transaksi',
        'status',
    ];
}
