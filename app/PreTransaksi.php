<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreTransaksi extends Model
{
    protected $fillable = [
        'code_transaksi',
        'total_dibayar_transaksi',
        'status',
    ];
}
