<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kategoriMenu extends Model
{
    protected $fillable = [
        'name'
    ];
}
