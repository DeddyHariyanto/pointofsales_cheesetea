<?php

namespace App\Http\Controllers;

use App\PreTransaksi;
use App\PreDetailTransaksi;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\ValidationException;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
use App\Gerai;
use App\User;
use App\UserGerai;
use App\Menu;
use App\akses;
use App\transaksi;
use App\detailTransaksi;
use App\Komposisi;
use App\jurnal;
use Carbon\Carbon;
use Auth;
use DB;
use App;

use Barryvdh\DomPDF\Facade\Pdf;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class PreTransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pesananOnlineindex()
    {
        if(Auth::check()){
          $data =[];
            $pesanan = DB::table('pre_transaksis')
                        // ->whereDate('pre_transaksis.created_at', Carbon::today())
                        ->select(
                            'pre_transaksis.id as id',
                            'pre_transaksis.code_transaksi as code',
                            'pre_transaksis.total_dibayar_transaksi as total_dibayar',
                            'pre_transaksis.status as status')->orderBy('pre_transaksis.id','desc')->get();
            foreach ($pesanan as $key => $pesanans) {
                
                $data[$key] = [
                    'id' => $pesanans->id,
                    'code' => $pesanans->code,
                    'customer' => '',
                    'total_dibayar' => $pesanans->total_dibayar,
                    'status' => $pesanans->status,
                    // 'detail' => $detail,
                ];
            }
            return view('preTransaksi.index',compact('data'));
        }else{
            return redirect('/login')->with('toast_info', 'Silahkan Login kembali');
        }
        
    }
     public function menuCustomer()
    {
      $minuman = Menu::where('tipe_menu','minum')->get();
        $makanan = Menu::where('tipe_menu','makan')->get();
        $add = Menu::where('tipe_menu','add')->get();
        $data = [
            'minuman'=>$minuman,
            'makanan'=>$makanan,
            'add'=>$add,
        ];
        return view('preTransaksi.menuCustomer',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function preKasirPost(Request $request)
    {
      $no = 0;
      $transaksi = PreTransaksi::orderby('id','desc')->first();
      if ($transaksi == null) {
        $no = 1;
      }else{
        $no = $transaksi->id + 1;
      }

      $customerid = '';
      // dd(explode('@',$request->customer_id));
      if ($request->customer_id != null) {
        $users = User::where('email',$request->customer_id)->first();
        if ($users) {
          $customerid = $users->id;
        }else{
          $customer = User::create([
            'level'=>'customer',
            'nama'=> explode('@',$request->customer_id)[0],
            'email'=> $request->customer_id,
            'password'   => bcrypt('sobatcheestea'),
            'status_user'=>'aktif',
          ]);
          $customerid = $customer->id;
        }
      }else{
        $customerid = '';
      }

        $transaksi = PreTransaksi::create([
          'code_transaksi' => $request->code_transaksi,
          'total_dibayar_transaksi' => $request->total_dibayar,
          'status' => 'order',
        ]);
        $item = $request->id_menu;
        for ($i=0; $i < count($item); $i++) { 
          $menu = Menu::where('id',$item[$i])->first();
          $itemsCreate = PreDetailTransaksi::create([
            'id_transaksi'=> $transaksi->id,
            'kategori' => $menu->tipe_menu,
            'id_menu' => $item[$i],
            'kuantiti' => 1,
            'nama' => $menu->nama_menu,
            'harga' => $request->harga_menu[$i],
          ]);
        }

        return redirect()->back()->with('success', '<b>Pesanan Berhasil<b><br> 
                                                    <b>No.Pesanan :#' . $transaksi->code_transaksi .'<b></b>
                                                    <a href="/kasir-pre-struk/'. $transaksi->id .'" target="_blank">lihat No.Pesanan</a>');
        
      
    }
    public function prekasirStruk($id)
    {
      $struk = DB::table('pre_transaksis')
                  // ->leftJoin('pre_detail_transaksis','pre_transaksis.id','=','pre_detail_transaksis.id_transaksi')
                  ->where('pre_transaksis.id',$id)
                  ->select(
                    'pre_transaksis.id as id',
                    'pre_transaksis.code_transaksi as code',
                    'pre_transaksis.total_dibayar_transaksi as total_dibayar',
                    'pre_transaksis.created_at as tanggal')->first();
      $detail = PreDetailTransaksi::where('id_transaksi', $id)
                                ->groupBy('id_menu')
                                ->select('nama','harga',DB::raw('count(id_menu) as qty'))
                                ->get();
      $detailStruk = [];
      foreach ($detail as $key => $details) {
        $detailStruk[$key] = [
          'nama' =>$details->nama,
          'harga_satuan' =>$details->harga,
          'harga_total' =>$details->harga * $details->qty,
          'qty' =>$details->qty,
        ];
      }
      $data = [
        'id' => $struk->id,
        'code' => $struk->code,
        'total_dibayar' => $struk->total_dibayar,
        'tanggal' => Carbon::parse($struk->tanggal)->format('d M Y'),
        'detail' => $detailStruk
      ];
      $customPaper = array(0,0,567.00,283.80);
      $pdf = PDF::loadView('preTransaksi.pdf.struk', compact('data'))->setPaper($customPaper, 'landscape');
      return $pdf->stream();
    }
    public function getKonfirmasiPembelian($id)
    {
      $struk = DB::table('pre_transaksis')
                  // ->leftJoin('pre_detail_transaksis','pre_transaksis.id','=','pre_detail_transaksis.id_transaksi')
                  ->where('pre_transaksis.id',$id)
                  ->select(
                    'pre_transaksis.id as id',
                    'pre_transaksis.code_transaksi as code',
                    'pre_transaksis.total_dibayar_transaksi as total_dibayar',
                    'pre_transaksis.created_at as tanggal')->first();
      $detail = PreDetailTransaksi::where('id_transaksi', $id)
                                ->groupBy('id_menu')
                                ->select('nama','harga',DB::raw('count(id_menu) as qty'))
                                ->get();
      $detailStruk = [];
      foreach ($detail as $key => $details) {
        $detailStruk[$key] = [
          'nama' =>$details->nama,
          'harga_satuan' =>$details->harga,
          'harga_total' =>$details->harga * $details->qty,
          'qty' =>$details->qty,
        ];
      }
      $data = [
        'id' => $struk->id,
        'code' => $struk->code,
        'total_dibayar' => $struk->total_dibayar,
        'tanggal' => Carbon::parse($struk->tanggal)->format('d M Y'),
        'detail' => $detailStruk
      ];
      return response()->json($data, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function konfirmasiPembelian(Request $request)
    {
        // dd($request->all());
        
      $customerid = '';
      if ($request->customer_id != null) {
        $users = User::where('email',$request->customer_id)->first();
        if ($users) {
          $customerid = $users->id;
        }else{
          $customer = User::create([
            'level'=>'customer',
            'nama'=> explode('@',$request->customer_id)[0],
            'email'=> $request->customer_id,
            'password'   => bcrypt('sobatcheestea'),
            'status_user'=>'aktif',
          ]);
          $customerid = $customer->id;
        }
      }else{
        $customerid = '';
      }

      if ($request->jumlah_dibayar >= ($request->total_dibayar - $request->jumlah_potongan)) {
        $transaksi = transaksi::create([
          'code_transaksi' => $request->code_transaksi,
          'id_gerai_transaksi' => $request->gerai_id,
          'id_user_transaksi' => $request->user_id,
          'id_customer_transaksi' => $customerid,
          'tipe_bayar_transaksi' => $request->tipe_pembayaran,
          'discount_transaksi' => $request->discount,
          'jumlah_bayar_transaksi' => $request->jumlah_dibayar,
          'total_dibayar_transaksi' => $request->total_dibayar,
          'no_rekening' => $request->no_rekening,
          'nama_rekening' => $request->nama_rekening,
          'status' => 'invalid',
        ]);
        $preTransaksi = PreTransaksi::where('id', $request->idPreTransaksi)->first();
        $preTransaksi->update([
          'status' => 'order konfirmasi'
        ]);

        $jurnal = jurnal::create([
          'type_id'     => 1,
          'doc_id'      => $transaksi->id,
          'gerai_id'    => $request->gerai_id,
          'title'       => 'struk No '.$request->code_transaksi,
          'description' => 'Pembeliaan',
          'debit'       => $request->total_dibayar - $request->jumlah_potongan,
          'status'      => 'invalid',
        ]);
        $preDetail = PreDetailTransaksi::where('id_transaksi', $request->idPreTransaksi)->get();
        foreach ($preDetail as $key => $preDetails) {
          $itemsCreate = detailTransaksi::create([
            'id_transaksi'=> $transaksi->id,
            'kategori' => $preDetails->kategori,
            'id_menu' => $preDetails->id_menu,
            'kuantiti' => $preDetails->kuantiti,
            'nama' => $preDetails->nama,
            'harga' => $preDetails->harga,
          ]);
        }
        $kembalian = $request->jumlah_dibayar - ($request->total_dibayar - $request->jumlah_potongan);
        // return redirect()->back()->with('toast_success', 'Transaksi Telah Tersimpan');
        if ($request->tanpa_struk == null) {
          $details = [
            'id' => $transaksi->id,
            'title' => $transaksi->code_transaksi,
            'gerai_id' => $transaksi->id_gerai_transaksi,
            'kasir' => $transaksi->id_user_transaksi,
            // 'detail_struk' => detailTransaksi::where('id_transaksi',$transaksi->id)->get(),
            'discount_transaksi' => $transaksi->discount_transaksi,
            'jumlah_bayar_transaksi' => $transaksi->jumlah_bayar_transaksi,
            'total_dibayar_transaksi' => $transaksi->total_dibayar_transaksi,
          ];
           
          \Mail::to($request->customer_id)->send(new \App\Mail\MyTestMail($details));
        }

        return redirect()->back()->with('success', '<b>Transaksi Berhasil<b><br> 
                                                    <b>No.Transaksi :#' . $transaksi->code_transaksi .'<b></b>
                                                    <br><p>Uang Kembali<p> <b>Rp. '.number_format($kembalian).'</b><br>
                                                    <a href="/kasir-struk/'. $transaksi->id .'" target="_blank">link Struk</a>');
        
      }else{
        return back()->with('toast_error', 'Nominal yang Anda masukan Kurang Dari Total Dibayar')->withInput();
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PreTransaksi  $preTransaksi
     * @return \Illuminate\Http\Response
     */
    public function show(PreTransaksi $preTransaksi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PreTransaksi  $preTransaksi
     * @return \Illuminate\Http\Response
     */
    public function edit(PreTransaksi $preTransaksi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PreTransaksi  $preTransaksi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PreTransaksi $preTransaksi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PreTransaksi  $preTransaksi
     * @return \Illuminate\Http\Response
     */
    public function destroy(PreTransaksi $preTransaksi)
    {
        //
    }
}
