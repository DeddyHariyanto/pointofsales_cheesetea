<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KategoriInventory;
use App\Inventory;
use App\jurnal;
use App\stockBarang;
use DB;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class InventoryController extends Controller
{
    public function inventoryIndex(){
      $kategories = KategoriInventory::all();
      return view('inventory.index',compact('kategories'));
    }

    public function inventoryPost(Request $request){
      $validator = Validator::make($request->all(), [
        'nama_kategori' => 'required',
        'tipe_inventory' => 'required',
        'brand_inventory' => 'required',
      ]);
      if ($validator->fails()) {
        return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
      }else{
        $kategori = KategoriInventory::where('nama_kategori',strtolower($request->nama_kategori))->first();
        if ($kategori == false) {
          $kategori = KategoriInventory::create([
            'nama_kategori'=>strtolower($request->nama_kategori),
          ]);
        }
        if ($request->code == 1) {
            $imageName = 'empty';
            if ($request->berkas_inventory != null) {
              if ($request->berkas_inventory != null && $request->berkas_inventory != '') {
                  $imageName = uniqid('berkas_inventory_').'.'.$request->berkas_inventory->getClientOriginalExtension();
                  $path = public_path('berkas/inventory');
                  if (!File::isDirectory($path)) {
                      File::makeDirectory($path, 0777, true, true);
                      $request->berkas_inventory->move('berkas/inventory/', $imageName);
                  }else{
                    $request->berkas_inventory->move('berkas/inventory/', $imageName);
                  }
              }
            }elseif ($request->foto != null) {
              $imageName = $request->foto;
          }

          $total_kuantitas = $request->kelipatan_item * $request->kuantitas_inventory;
          $checkBrand = Inventory::where('brand_inventory',$request->brand_inventory)->where('lokasi_id', $request->lokasi_id)->first();
          if($checkBrand){
            $checkBrand->update([
              'id_kategori'=>$kategori->id,
              'lokasi_id'=>$request->lokasi_id,
              'tipe_inventory'=>$request->tipe_inventory,
              'brand_inventory'=>$request->brand_inventory,
              'satuan_inventory'=>$request->satuan_inventory,
              'kuantitas_inventory'=>$total_kuantitas + $checkBrand->kuantitas_inventory,
              'harga_inventory'=>$request->harga_inventory,
              'berkas_inventory'=>$imageName,
              'status_inventory'=>'instock'
            ]);
            return redirect()->back()->with('toast_success', 'data Barang Telah Tersimpan');
          }else{
            Inventory::create([
              'id_kategori'=>$kategori->id,
              'lokasi_id'=>$request->lokasi_id,
              'tipe_inventory'=>$request->tipe_inventory,
              'brand_inventory'=>$request->brand_inventory,
              'satuan_inventory'=>$request->satuan_inventory,
              'kuantitas_inventory'=>$total_kuantitas,
              'harga_inventory'=>$request->harga_inventory,
              'berkas_inventory'=>$imageName,
              'status_inventory'=>'instock'
            ]);
            return redirect()->back()->with('toast_success', 'data Barang Telah Tersimpan');
          }
        }elseif ($request->code == 2) {
          $imageName = 'empty';
            if ($request->berkas_inventory != null) {
              if ($request->berkas_inventory != null && $request->berkas_inventory != '') {
                  $imageName = uniqid('berkas_inventory_').'.'.$request->berkas_inventory->getClientOriginalExtension();
                  $path = public_path('berkas/inventory');
                  if (!File::isDirectory($path)) {
                      File::makeDirectory($path, 0777, true, true);
                      $request->berkas_inventory->move('berkas/inventory/', $imageName);
                  }else{
                    $request->berkas_inventory->move('berkas/inventory/', $imageName);
                  }
              }
            }elseif ($request->foto != null) {
              $imageName = $request->foto;
          }

          $total_kuantitas = $request->kelipatan_item * $request->kuantitas_inventory;
          $inven = Inventory::where('id', $request->id)->first();
          $inven->update([
            'id_kategori'=>$kategori->id,
            'lokasi_id'=>$request->lokasi_id,
            'tipe_inventory'=>$request->tipe_inventory,
            'brand_inventory'=>$request->brand_inventory,
            'satuan_inventory'=>$request->satuan_inventory,
            'kuantitas_inventory'=>$total_kuantitas,
            'harga_inventory'=>$request->harga_inventory,
            'berkas_inventory'=>$imageName,
            'status_inventory'=>$request->status_inventory,
            'status_inventory'=>'instock',
            
          ]);
          return redirect()->back()->with('toast_success', 'data Barang Telah Di Edit');
          //in detail gerai
        }elseif($request->code == 3) {
          // dd($request->all());
            $imageName = 'empty';
            if ($request->berkas_inventory != null) {
              if ($request->berkas_inventory != null && $request->berkas_inventory != '') {
                  $imageName = uniqid('berkas_inventory_').'.'.$request->berkas_inventory->getClientOriginalExtension();
                  $path = public_path('berkas/inventory');
                  if (!File::isDirectory($path)) {
                      File::makeDirectory($path, 0777, true, true);
                      $request->berkas_inventory->move('berkas/inventory/', $imageName);
                  }else{
                    $request->berkas_inventory->move('berkas/inventory/', $imageName);
                  }
              }
            }elseif ($request->foto != null) {
              $imageName = $request->foto;
          }

          $total_kuantitas = $request->kelipatan_item * $request->kuantitas_inventory;
          if ($request->asal == 'alokasi') {
            $checkStockAll = Inventory::where('brand_inventory', $request->brand_inventory)->first();
            if ($checkStockAll) {
              $sisaStock = $checkStockAll->kuantitas_inventory - $total_kuantitas;
              //check status all
              $checkIdInven = Inventory::where('brand_inventory', $request->brand_inventory)
                                          ->where('lokasi_id', $request->lokasi_id)
                                          ->first();
              if ($checkIdInven) {
                //stock yang keluar
                $getPemakain = stockBarang::where('inventory_id', $checkIdInven->id)->first();
                $updateStock = 0; 
                if ($getPemakain) {
                  // if (($total_kuantitas - $getPemakain->stock) == 0) {
                  //   $updateStock = 0; 
                  // }else{
                    $updateStock = $checkIdInven->kuantitas_inventory + $total_kuantitas; 
                  // } 
                  // $getPemakain->update([
                  //   'stock' => $updateStock
                  // ]);
                }else{
                  $updateStock = $total_kuantitas - 0; 
                }
                // else{
                //   $updateBaru = stockBarang::create([
                //     'kategori_barang' => $stock->kategori_komposisi,
                //     'inventory_id'    => $checkIdInven->id,
                //     'stock'           => 0,
                //     'satuan'          => $checkIdInven->satuan_inventory,
                //   ]);
                // }
                $checkIdInven->update([
                  'lokasi_id'=>$request->lokasi_id,
                  'satuan_inventory'=>$request->satuan_inventory,
                  'kuantitas_inventory'=>$checkIdInven->kuantitas_inventory + $total_kuantitas,
                  'harga_inventory'=>$request->harga_inventory,
                  'berkas_inventory'=>$imageName,
                  'status_inventory'=>'instock',
                  'asal_pembelian'=>$request->asal
                ]);
                $checkStockAll->update([
                  'kuantitas_inventory'=> $sisaStock,
                ]);
                
              }else{
                $inven = Inventory::create([
                  'id_kategori'=>$kategori->id,
                  'lokasi_id'=>$request->lokasi_id,
                  'tipe_inventory'=>$request->tipe_inventory,
                  'brand_inventory'=>$request->brand_inventory,
                  'satuan_inventory'=>$request->satuan_inventory,
                  'kuantitas_inventory'=>$total_kuantitas,
                  'harga_inventory'=>$request->harga_inventory,
                  'berkas_inventory'=>$imageName,
                  // 'status_inventory'=>$request->status_inventory,
                  'status_inventory'=>'instock',
                  'asal_pembelian'=>$request->asal
                ]);
                $checkStockAll->update([
                  'kuantitas_inventory'=>$sisaStock,
                ]);
              }
            }else{
              return redirect()->back()->with('toast_error', 'Barang Tidak Ditemukan, Pastikan Ada Stock Pada Gudang, Untuk Menggunakan Fitur Alokasi ');
              // $inven = Inventory::create([
              //   'id_kategori'=>$kategori->id,
              //   'lokasi_id'=>$request->lokasi_id,
              //   'tipe_inventory'=>$request->tipe_inventory,
              //   'brand_inventory'=>$request->brand_inventory,
              //   'kuantitas_inventory'=>$total_kuantitas,
              //   'satuan_inventory'=>$request->satuan_inventory,
              //   'harga_inventory'=>$request->harga_inventory,
              //   'berkas_inventory'=>$imageName,
              //   // 'status_inventory'=>$request->status_inventory,
              //   'status_inventory'=>'instock',
              //   'asal_pembelian'=>$request->asal
              // ]);
            }
            
          }else{
            $checkBrand = Inventory::where('brand_inventory',$request->brand_inventory)->where('lokasi_id', $request->lokasi_id)->first();
            if($checkBrand){
              $checkBrand->update([
                'id_kategori'=>$kategori->id,
                'lokasi_id'=>$request->lokasi_id,
                'tipe_inventory'=>$request->tipe_inventory,
                'brand_inventory'=>$request->brand_inventory,
                'satuan_inventory'=>$request->satuan_inventory,
                'kuantitas_inventory'=>$total_kuantitas + $checkBrand->kuantitas_inventory,
                'harga_inventory'=>$request->harga_inventory,
                'berkas_inventory'=>$imageName,
                // 'status_inventory'=>$request->status_inventory,
                'status_inventory'=>'instock',
                'asal_pembelian'=>$request->asal
              ]);
            }else{
              $inven = Inventory::create([
                'id_kategori'=>$kategori->id,
                'lokasi_id'=>$request->lokasi_id,
                'tipe_inventory'=>$request->tipe_inventory,
                'brand_inventory'=>$request->brand_inventory,
                'satuan_inventory'=>$request->satuan_inventory,
                'kuantitas_inventory'=>$total_kuantitas,
                'harga_inventory'=>$request->harga_inventory,
                'berkas_inventory'=>$imageName,
                // 'status_inventory'=>$request->status_inventory,
                'status_inventory'=>'instock',
                'asal_pembelian'=>$request->asal
              ]);
            }

          }
          return redirect()->back()->with('toast_success', 'data Barang Telah Tersimpan');
        }
      }
    }
    public function aktivasiInventory(Request $request)
    {
      $code = $request->code;
      if ($code == 1) {
        $inventory = Inventory::findOrFail($request->id);
        $inventory->update([
          'status_inventory' => 'instock'
        ]);
      }elseif ($code == 2) {
        $inventory = Inventory::findOrFail($request->id);
        $inventory->update([
          'status_inventory' => 'invalid'
        ]);
      }
      return redirect()->back()->with('toast_success', 'Berhasil');
    }
}
