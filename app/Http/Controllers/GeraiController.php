<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\ValidationException;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
use App\Gerai;
use App\UserGerai;
use App\detailTransaksi;
use App\User;
use App\Absensi;
use App\Menu;
use App\jurnal;
use Carbon\Carbon;
use App\KategoriInventory;
use App\transaksi;
use App\Inventory;
use App\stockBarang;
use DB;

class GeraiController extends Controller
{
    public function geraiIndex($status){
      $gerais = Gerai::where('status_gerai',$status)->get();
      $status = $status;
      return view('gerai.index',compact('gerais','status'));
    }

    public function geraiPost(Request $request){
      $validator = Validator::make($request->all(),[
          'nama_gerai' => 'required|max:255',
          'master_pin' => 'required|max:6',
          're_master_pin' => 'required|max:6',
          'alamat' => 'required',
          // 'lat' => 'required',
          // 'lng' => 'required',
      ]);
      if ($validator->fails()) {
          return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
      }else{
        if ($request->master_pin == $request->re_master_pin) {
          $gerai = Gerai::create([
            'nama_gerai' => $request->nama_gerai,
            'lokasi_gerai' => $request->alamat,
            'lat_lng_gerai' => $request->lat .'/'.$request->lng,
            'status_gerai'  => 'aktif',
            'pin_gerai'     => $request->master_pin,
          ]);

          if($request->id_user != null){
            for ($i=0; $i < count($request->id_user); $i++) {
              UserGerai::create([
                'id_user' => $request->id_user[$i],
                'id_gerai' => $gerai->id,
                'pin_user_gerai' => rand(100000, 999999),
                'status_user_gerai' => 'aktif'
              ]);
            }
          }
          return redirect()->back()->with('toast_success', 'Gerai Berhasil Di Buat');
        }else{
          return redirect()->back()->with('toast_error', 'Pin Yang anda Isi Berbeda');
        }
      }
    }
    public function geraiEdit(Request $request){
      // dd($request->all());
      $validator = Validator::make($request->all(),[
          'id' => 'required',
          'nama_gerai' => 'required|max:255',
          'master_pin' => 'required|max:6',
          're_master_pin' => 'required|max:6',
          'alamat' => 'required',
        ]);
      if ($validator->fails()) {
          return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
      }else{
        if ($request->master_pin == $request->re_master_pin) {
          $gerai = Gerai::findOrFail($request->id);
          $gerai->update([
            'nama_gerai' => $request->nama_gerai,
            'lokasi_gerai' => $request->alamat,
            'lat_lng_gerai' => $request->lat .'/'.$request->lng,
            'status_gerai'  => 'aktif',
            'pin_gerai'     => $request->master_pin,
          ]);

          if($request->id_user != null){
            for ($i=0; $i < count($request->id_user); $i++) {
              UserGerai::create([
                'id_user' => $request->id_user[$i],
                'id_gerai' => $gerai->id,
                'pin_user_gerai' => rand(100000, 999999),
                'status_user_gerai' => 'aktif'
              ]);
            }
          }
          return redirect()->back()->with('toast_success', 'Gerai Berhasil Di Buat');
        }else{
          return redirect()->back()->with('toast_error', 'Pin Yang anda Isi Berbeda');
        }
      }
    }

    public function geraiAktifasi($code, Request $request){
      
      try {
        $gerai = Gerai::findOrFail($request->id);
        if ($code==0) {
          $gerai->update([
            'status_gerai'=>'nonaktif'
          ]);
        }elseif ($code==1) {
          $gerai->update([
            'status_gerai'=>'aktif'
          ]);
        }
        
      } catch (ModelNotFoundException $exception) {
        return back()->withError($exception->getMessage())->withInput();
      }
      return redirect()->back()->with('toast_success', 'Berhasil Merubah Status Gerai');
    }
    public function hapusGerai(Request $request)
    {
      try {
        $gerai = Gerai::findOrFail($request->id);
        $karyawan = UserGerai::where('id_gerai', $request->id)->get();
        foreach ($karyawan as $key => $karyawans) {
          $karyawans->delete();
        }
        $gerai->delete();
      } catch (ModelNotFoundException $exception) {
        return back()->with('toast_error', $exception->getMessage());
      }
      return redirect()->back()->with('toast_success', 'Gerai Berhasil Di Hapus');
    }
    public function ubahPinGerai(Request $request)
    {
      try {
        $gerai = Gerai::findOrFail($request->id);
        $gerai->update([
          'pin_gerai' => $request->re_master_pin
        ]);
      } catch (ModelNotFoundException $exception) {
        return back()->with('toast_error', $exception->getMessage());
      }
      return redirect()->back()->with('toast_success', 'Master Pin Gerai Berhasil Di Ubah');
    }
    public function tambahKaryawanGerai(Request $request)
    {
      $validator = Validator::make($request->all(),[
        'pin_user_gerai' => 'unique:pin_user_gerai|max:6',
      ]);
      if ($validator->fails()) {
        return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
      }else{
        $existingKaryawan = $request->user_gerai_id;
        if ($existingKaryawan) {
          for ($i=0; $i < count($existingKaryawan); $i++) { 
            $usre = UserGerai::findOrFail($existingKaryawan[$i]);
            // $checkPin = UserGerai::where('pin_user_gerai', $request->pin[$i])->first();
            // if ($checkPin == false) {
              $usre->update([
                'id_user' => $request->karyawanExisting[$i],
                'id_gerai' => $request->id,
                'pin_user_gerai' => $request->pin[$i],
                'status_user_gerai' => $request->status[$i],
              ]);
            // }else{
            //   return back()->with('toast_error','Pin Yang Kamu Masukan Telah Ada')->withInput();
            // }
          }
        }
        $karyawanNew = $request->karyawanNew;
        for ($x=0; $x < count($karyawanNew); $x++) { 
          if ($karyawanNew[$x] != null) {
            $checkPin = UserGerai::where('pin_user_gerai', $request->pinKaryawanNew[$x])->first();
            if ($checkPin == false) {
              UserGerai::create([
                'id_user' => $karyawanNew[$x],
                'id_gerai' => $request->id,
                'pin_user_gerai' => $request->pinKaryawanNew[$x],
                'status_user_gerai' => 'aktif'
              ]);
            }else{
              return back()->with('toast_error','Pin Yang Kamu Masukan Telah Ada')->withInput();
            }
          }
        }
        return redirect()->back()->with('toast_success', 'Tambah Karyawan Gerai Berhasil');

      }
    }
    public function geraiDetail($id){
      try {
        $gerai = Gerai::findOrFail($id);

        //strukKu
        $strukKu =[];
        $struk = DB::table('transaksis')
                  ->leftJoin('users','transaksis.id_user_transaksi','=','users.id')
                  // ->leftJoin('detail_transaksis','transaksis.id','=','detail_transaksis.id_transaksi')
                  ->where('transaksis.id_gerai_transaksi',$id)
                  ->whereDate('transaksis.created_at','=',Carbon::today())
                  ->select(
                    'transaksis.id as id',
                    'transaksis.code_transaksi as code',
                    'users.nama as kasir',
                    'transaksis.tipe_bayar_transaksi as tipe_bayar',
                    'transaksis.jumlah_bayar_transaksi as jumlah_bayar',
                    'transaksis.discount_transaksi as discount',
                    'transaksis.total_dibayar_transaksi as total_dibayar',
                    'transaksis.created_at as tanggal',
                    'transaksis.status as status')->get();
        
        foreach ($struk as $key => $strukc) {
          
          $strukKu[$key] = [
            'id' => $strukc->id,
            'code' => $strukc->code,
            'kasir' => $strukc->kasir,
            'tipe_bayar' => $strukc->tipe_bayar,
            'jumlah_bayar' => $strukc->jumlah_bayar,
            'discount' => $strukc->discount,
            'total_dibayar' => $strukc->total_dibayar,
            'tanggal' => Carbon::parse($strukc->tanggal)->format('d M Y'),
            'status' => $strukc->status,
          ];
        }

        $total = 0;
        $totalDiscount = 0;
        $totalPlusDiscount = 0;
        $minuman = 0;
        $totalMinuman = 0;
        $discountMinuman = 0;
        $totalMinumanDiscount = 0;
        $makanan = 0;
        $totalMakanan = 0;
        $discountMakanan = 0;
        $totalMakananDiscount = 0;
        $addOn = 0;
        $totalAddOn = 0;
        $discountAddOn = 0;
        $totalAddOnDiscount = 0;

        $transaksi = DB::table('transaksis')
                  // ->leftJoin('detail_transaksis','transaksis.id','=','detail_transaksis.id_transaksi')
                  ->where('transaksis.id_gerai_transaksi',$id)
                  ->whereDate('transaksis.created_at','=',Carbon::today())
                  ->where('transaksis.status','!=','batal')
                  ->where('tipe_bayar_transaksi','<=',5)
                  ->select(
                    'transaksis.id as id',
                    'transaksis.discount_transaksi as discount',
                    'transaksis.tipe_bayar_transaksi as tipe_bayar_transaksi',
                    'transaksis.total_dibayar_transaksi as total_dibayar')->get();
        foreach ($transaksi as $key => $transaksis) {
          // if ($transaksis->tipe_bayar_transaksi != 6 || $transaksis->tipe_bayar_transaksi != 7) {
            $totalDiscount += $transaksis->discount/100 * $transaksis->total_dibayar;
            $totalPlusDiscount +=  $transaksis->total_dibayar - ($transaksis->discount/100 * $transaksis->total_dibayar);
            $total += $transaksis->total_dibayar;
          // }
          $makanC = detailTransaksi::where('id_transaksi', $transaksis->id)->where('kategori','makan')->get();
          $minumC = detailTransaksi::where('id_transaksi', $transaksis->id)->where('kategori','minum')->get();
          $addC = detailTransaksi::where('id_transaksi', $transaksis->id)->where('kategori','add')->get();
          $makanan += count($makanC);
          $minuman += count($minumC);
          $addOn += count($addC);
          $makan = detailTransaksi::where('id_transaksi', $transaksis->id)
                                    ->where('kategori','makan')
                                    ->select(DB::raw('sum(harga) as total_dibayar'))
                                    ->first();
          $totalMakanan += $makan->total_dibayar;
          $discountMakanan += $transaksis->discount/100 * $makan->total_dibayar;
          $totalMakananDiscount += $makan->total_dibayar - ($transaksis->discount/100 * $makan->total_dibayar);

          $minum = detailTransaksi::where('id_transaksi', $transaksis->id)
                                    ->where('kategori','minum')
                                    ->select(DB::raw('sum(harga) as total_dibayar'))
                                    ->first();
          $totalMinuman += $minum->total_dibayar;
          $discountMinuman += $transaksis->discount/100 * $minum->total_dibayar;
          $totalMinumanDiscount += $minum->total_dibayar - ($transaksis->discount/100 * $minum->total_dibayar);
          
          $add = detailTransaksi::where('id_transaksi', $transaksis->id)
                                    ->where('kategori','add')
                                    ->select(DB::raw('sum(harga) as total_dibayar'))
                                    ->first();
          $totalAddOn += $add->total_dibayar;
          $discountAddOn += $transaksis->discount/100 * $add->total_dibayar;
          $totalAddOnDiscount += $add->total_dibayar - ($transaksis->discount/100 * $add->total_dibayar);

        }
        $topSales = [];
        $terlaris = DB::table('transaksis')
                        ->leftJoin('detail_transaksis', 'detail_transaksis.id_transaksi','=','transaksis.id')
                        ->leftJoin('gerais', 'gerais.id','=','transaksis.id_gerai_transaksi')
                        ->leftJoin('menus', 'menus.id','=','detail_transaksis.id_menu')
                        ->select('menus.foto_menu','detail_transaksis.nama',DB::raw('count(nama) as jumlah'))
                        ->whereMonth('detail_transaksis.created_at','=',Carbon::now()->month)
                        ->where('transaksis.id_gerai_transaksi', $id)
                        ->where('transaksis.status','!=','batal')
                        // ->where('transaksis.id_gerai_transaksi', $id)
                        ->groupBy('nama')
                        ->orderBy('jumlah', 'desc')
                        ->limit(5)
                        ->get();
        foreach ($terlaris as $key => $terlarisGerai) {
          $topSales[$key]= [
            'nama' => $terlarisGerai->nama,
            'foto' => $terlarisGerai->foto_menu,
            'jumlah' => $terlarisGerai->jumlah,
          ];
        }
        $customer = User::where('level','customer')->whereDate('created_at','=',Carbon::today())->get();
        
        //user gerai
        $userGeraiKu = [];
        $karyawan =  DB::table('user_gerais')
                        ->leftJoin('users', 'user_gerais.id_user','=','users.id')
                        ->where('id_gerai',$id)
                        ->where('status_user_gerai','aktif')
                        ->select('users.id','users.nama','users.foto_user','users.level')
                        ->get();
        foreach ($karyawan as $key => $karyawans) {
          $absensi = Absensi::where('absensis_user_id', $karyawans->id)
                              ->select('status_absensi','waktu_absensi')
                              ->get();
          $userGeraiKu[$key]= [
            'id' => $karyawans->id,
            'nama' => $karyawans->nama,
            'level' => $karyawans->level,
            'foto_user' => $karyawans->foto_user,
            'absensi' => $absensi,
          ];
        }

        //stock
        $inven = [];
        // $qty = 0;
        $kategorie = DB::table('kategori_inventories')
                          ->leftJoin('inventories','inventories.id_kategori','=','kategori_inventories.id')
                          ->where('inventories.lokasi_id',$id)
                          // ->whereDate('inventories.created_at','=',Carbon::now()->week)
                          ->select(
                            'kategori_inventories.id as id',
                            'inventories.id as id_inven',
                            'kategori_inventories.nama_kategori as nama',
                            'inventories.satuan_inventory as satuan',
                            DB::raw('sum(inventories.kuantitas_inventory) as qty'))->groupBy('nama_kategori')->orderBy('qty','asc')->limit(4)
                          ->get();
                          // dd($kategorie);
        foreach ($kategorie as $key => $kategories) {
          // $checkStock = stockBarang::where('kategori_barang', $kategories->nama)
          //                           ->select(DB::raw('sum(stock) as qty'))
          //                           ->first();
          $checkStock = stockBarang::where('inventory_id', $kategories->id_inven)
                                    ->select(DB::raw('sum(stock) as qty'))
                                    ->first();
          // dd($kategories->qty-$checkStock->qty);
          $qty = 0;
          if ($kategories->satuan == 'gram' || $kategories->satuan == 'ml') {
            $qty = $checkStock->qty;
          }elseif ($kategories->satuan == 'kilogram') {
            $gramToKg = $checkStock->qty/1000;
            $qty = $gramToKg;
          }elseif ($kategories->satuan == 'litter') {
            $mlToL = $checkStock->qty/1000;
            $qty = $mlToL;
          }else{
            $qty = 0;
          }
          // $qty = 0;
          if ($checkStock->qty == null){
            $qty = 0;
          }else{
            $qty = $checkStock->qty;
          }
          $inven[$key] = [
            'id' => $kategories->id,
            'nama' => $kategories->nama,
            'qty' => $qty,
            'satuan' => $kategories->satuan,
          ];
        }

        $dataStock = [];
        $stock = Inventory::where('lokasi_id',$id)->get();
        foreach ($stock as $key => $stocks) {
          $checkGerai = Gerai::where('id', $stocks->lokasi_id)->first();
          $kategori = KategoriInventory::where('id', $stocks->id_kategori)->first();
          $checkStock = stockBarang::where('inventory_id', $stocks->id)
                                    ->select(DB::raw('sum(stock) as qty'))
                                    ->first();
          $qty = 0;
          if ($stocks->satuan_inventory == 'gram' || $stocks->satuan_inventory == 'ml') {
            $qty = $stocks->kuantitas_inventory-$checkStock->qty;
          }elseif ($stocks->satuan_inventory == 'kilogram') {
            $gramToKg = $checkStock->qty/1000;
            $qty = $stocks->kuantitas_inventory - $gramToKg;
          }elseif ($stocks->satuan_inventory == 'litter') {
            $mlToL = $checkStock->qty/1000;
            $qty = $stocks->kuantitas_inventory - $mlToL;
          }else{
            $qty = $stocks->kuantitas_inventory;
          }
          $dataStock [$key] = [
            'id' => $stocks->id,
            'kategori' => $kategori->nama_kategori,
            'lokasi' => $checkGerai->nama_gerai,
            'tipe_inventory' => $stocks->tipe_inventory,
            'brand_inventory' => $stocks->brand_inventory,
            'satuan_inventory' => $stocks->satuan_inventory,
            // 'kuantitas_inventory' => $qty,
            'kuantitas_inventory' => $qty,
            // 'kuantitas_inventory' => $stocks->kuantitas_inventory,
            // 'kuantitas_sisa' => $qty,
            'harga_inventory' => $stocks->harga_inventory,
            'status_inventory' => $stocks->status_inventory,
            'created_at' => Carbon::parse($stocks->created_at)->format('d/M/Y'),
          ];
        }
        
        $data = [
          'id' => $gerai->id,
          'nama_gerai' => $gerai->nama_gerai,
          'alamat' => $gerai->alamat,
          'struk' => $strukKu,
          'userGerai' => $userGeraiKu,
          'total' => $total,
          'totalDiscount' => $totalDiscount,
          'totalPlusDiscount' => $totalPlusDiscount,
          'minuman' => $minuman,
          'totalMinuman' => $totalMinuman,
          'discountMinuman' => $discountMinuman,
          'totalMinumanDiscount' => $totalMinumanDiscount,
          'makanan' => $makanan,
          'totalMakanan' => $totalMakanan,
          'discountMakanan' => $discountMakanan,
          'totalMakananDiscount' => $totalMakananDiscount,
          'addOn' => $addOn,
          'totalAddOn' => $totalAddOn,
          'discountAddOn' => $discountAddOn,
          'totalAddOnDiscount' => $totalAddOnDiscount,
          'total_transaksi' => count($transaksi),
          'langganan' => count($customer),
          'topSales' => $topSales,
          'inven' => $inven,
          'dataStock' => $dataStock,
        ];
        return view('gerai.detail',compact('data'));

      } catch (ModelNotFoundException $exception) {
        return back()->with('toast_error', $exception->getMessage());
      }
      return view('gerai.detail');
    }

    public function karyawanGerai($id)
    {
      try {
        $data = DB::table('user_gerais')
                    ->leftJoin('users', 'user_gerais.id_user','=','users.id')
                    ->where('user_gerais.id_gerai', $id)
                    ->select(
                      'user_gerais.id as id',
                      'users.id as user_id',
                      'users.nama as name',
                      'user_gerais.pin_user_gerai as pin',
                      'user_gerais.status_user_gerai as status')->get();
        return response()->json($data);
      } catch (ModelNotFoundException $exception) {
        return back()->with('toast_error', $exception->getMessage());
      }
      return view('gerai.detail');
    }
    public function geraiValidasiTransaksi(Request $request)
    {
      if($request->selectCheckBox == null){
        $allCheckBox = explode(",",$request->allCheckBox);
        for ($i=0; $i < count($allCheckBox); $i++) {
          $transaksi = transaksi::where('id', $allCheckBox[$i])->first();
          $transaksi->update([
            'status'=>'valid'
          ]);
          $stockDigunakan = DB::table('transaksis')
                                ->leftJoin('detail_transaksis','detail_transaksis.id_transaksi','=','transaksis.id')
                                ->leftJoin('menus','detail_transaksis.id_menu','=','menus.id')
                                ->leftJoin('komposisis','komposisis.menu_id','=','menus.id')
                                ->where('transaksis.id',$transaksi->id)
                                ->groupBy('komposisis.kategori_komposisi')
                                ->select('komposisis.kategori_komposisi','komposisis.brand_komposisi',DB::raw('sum(komposisis.jumlah_komposisi) as jumlah'),'komposisis.satuan_komposisi')
                                ->get();
            foreach ($stockDigunakan as $key => $stock) {
              if ($stock->brand_komposisi != null) {
                $checkIdInven = Inventory::where('brand_inventory', $stock->brand_komposisi)
                                          ->where('lokasi_id', $transaksi->id_gerai_transaksi)
                                          ->first();
                if ($checkIdInven) {
                  $getPemakain = stockBarang::where('kategori_barang', $stock->kategori_komposisi)
                                            ->where('inventory_id', $checkIdInven->id)
                                            ->first();
                  if ($getPemakain) {
                    $updateStock = $getPemakain->stock + $stock->jumlah;
                    $getPemakain->update([
                      'stock' => $updateStock
                    ]);
                  }else{
                    $updateBaru = stockBarang::create([
                      'kategori_barang' => $stock->kategori_komposisi,
                      'inventory_id'    => $checkIdInven->id,
                      'stock'           => $stock->jumlah,
                      'satuan'           => $stock->satuan_komposisi,
                    ]);
                  }
                }else{
                  $kategori = KategoriInventory::where('nama_kategori',$stock->kategori_komposisi)->first();
                  $tambahInve = Inventory::create([
                    'id_kategori' => $kategori->id,
                    'lokasi_id'   => $transaksi->id_gerai_transaksi,
                    'brand_inventory' => $stock->brand_komposisi,
                    'satuan_inventory' => $stock->satuan_komposisi,
                    'kuantitas_inventory' => 0,
                    'harga_inventory'   => 0,
                    'status_inventory' => 'invalid',
                  ]);
                  $updateBaru = stockBarang::create([
                    'kategori_barang' => $stock->kategori_komposisi,
                    'inventory_id'    => $tambahInve->id,
                    'stock'           => $stock->jumlah,
                    'satuan'          => $stock->satuan_komposisi,
                  ]);
                }
              }
            }
          $jurnal = jurnal::where('doc_id',$selectCheckBox[$i])->where('type_id', 1)->first();
            if ($jurnal) {
              $jurnal->update([
                'status' => 1
              ]);
            }
        }
        return redirect()->back()->with('toast_success', 'Transaksi Telah Dinyatakan Valid');
      }else{
        $allCheckBox = explode(",",$request->allCheckBox);
        $selectCheckBox = explode(",",$request->selectCheckBox);
        if ($allCheckBox == $selectCheckBox) {
          for ($i=0; $i < count($allCheckBox); $i++) {
            $transaksi = transaksi::where('id', $allCheckBox[$i])->first();
            $transaksi->update([
              'status'=>'valid'
            ]);
            $stockDigunakan = DB::table('transaksis')
                                ->leftJoin('detail_transaksis','detail_transaksis.id_transaksi','=','transaksis.id')
                                ->leftJoin('menus','detail_transaksis.id_menu','=','menus.id')
                                ->leftJoin('komposisis','komposisis.menu_id','=','menus.id')
                                ->where('transaksis.id',$transaksi->id)
                                ->groupBy('komposisis.kategori_komposisi')
                                ->select('komposisis.kategori_komposisi','komposisis.brand_komposisi',DB::raw('sum(komposisis.jumlah_komposisi) as jumlah'),'komposisis.satuan_komposisi')
                                ->get();
            foreach ($stockDigunakan as $key => $stock) {
              if ($stock->brand_komposisi != null) {
                $checkIdInven = Inventory::where('brand_inventory', $stock->brand_komposisi)
                                          ->where('lokasi_id', $transaksi->id_gerai_transaksi)
                                          ->first();
                if ($checkIdInven) {
                  $getPemakain = stockBarang::where('kategori_barang', $stock->kategori_komposisi)
                                            ->where('inventory_id', $checkIdInven->id)
                                            ->first();
                  if ($getPemakain) {
                    $updateStock = $getPemakain->stock + $stock->jumlah;
                    $getPemakain->update([
                      'stock' => $updateStock
                    ]);
                  }else{
                    $updateBaru = stockBarang::create([
                      'kategori_barang' => $stock->kategori_komposisi,
                      'inventory_id'    => $checkIdInven->id,
                      'stock'           => $stock->jumlah,
                      'satuan'           => $stock->satuan_komposisi,
                    ]);
                  }
                }else{
                  $kategori = KategoriInventory::where('nama_kategori',$stock->kategori_komposisi)->first();
                  $tambahInve = Inventory::create([
                    'id_kategori' => $kategori->id,
                    'lokasi_id'   => $transaksi->id_gerai_transaksi,
                    'brand_inventory' => $stock->brand_komposisi,
                    'satuan_inventory' => $stock->satuan_komposisi,
                    'kuantitas_inventory' => 0,
                    'harga_inventory'   => 0,
                    'status_inventory' => 'invalid',
                  ]);
                  $updateBaru = stockBarang::create([
                    'kategori_barang' => $stock->kategori_komposisi,
                    'inventory_id'    => $tambahInve->id,
                    'stock'           => $stock->jumlah,
                    'satuan'          => $stock->satuan_komposisi,
                  ]);
                }
              }
            }
            $jurnal = jurnal::where('doc_id',$selectCheckBox[$i])->where('type_id', 1)->first();
            if ($jurnal) {
              $jurnal->update([
                'status' => 1
              ]);
            }
          }
          return redirect()->back()->with('toast_success', 'Transaksi Telah Dinyatakan Valid');
        }elseif($allCheckBox > $selectCheckBox){
          for ($i=0; $i < count($selectCheckBox); $i++) {
            $transaksi = transaksi::where('id', $selectCheckBox[$i])->first();
            $transaksi->update([
              'status'=>'valid'
            ]);
            $stockDigunakan = DB::table('transaksis')
                                ->leftJoin('detail_transaksis','detail_transaksis.id_transaksi','=','transaksis.id')
                                ->leftJoin('menus','detail_transaksis.id_menu','=','menus.id')
                                ->leftJoin('komposisis','komposisis.menu_id','=','menus.id')
                                ->where('transaksis.id',$transaksi->id)
                                ->groupBy('komposisis.kategori_komposisi')
                                ->select('komposisis.kategori_komposisi','komposisis.brand_komposisi',DB::raw('sum(komposisis.jumlah_komposisi) as jumlah'),'komposisis.satuan_komposisi')
                                ->get();
            
            foreach ($stockDigunakan as $key => $stock) {
              if ($stock->brand_komposisi != null) {
                $checkIdInven = Inventory::where('brand_inventory', $stock->brand_komposisi)
                                          ->where('lokasi_id', $transaksi->id_gerai_transaksi)
                                          ->first();
                if ($checkIdInven) {
                  $getPemakain = stockBarang::where('kategori_barang', $stock->kategori_komposisi)
                                            ->where('inventory_id', $checkIdInven->id)
                                            ->first();
                  if ($getPemakain) {
                    $updateStock = $getPemakain->stock + $stock->jumlah;
                    $getPemakain->update([
                      'stock' => $updateStock
                    ]);
                  }else{
                    $updateBaru = stockBarang::create([
                      'kategori_barang' => $stock->kategori_komposisi,
                      'inventory_id'    => $checkIdInven->id,
                      'stock'           => $stock->jumlah,
                      'satuan'           => $stock->satuan_komposisi,
                    ]);
                  }
                }else{
                  $kategori = KategoriInventory::where('nama_kategori',$stock->kategori_komposisi)->first();
                  $tambahInve = Inventory::create([
                    'id_kategori' => $kategori->id,
                    'lokasi_id'   => $transaksi->id_gerai_transaksi,
                    'brand_inventory' => $stock->brand_komposisi,
                    'satuan_inventory' => $stock->satuan_komposisi,
                    'kuantitas_inventory' => 0,
                    'harga_inventory'   => 0,
                    'status_inventory' => 'invalid',
                  ]);
                  $updateBaru = stockBarang::create([
                    'kategori_barang' => $stock->kategori_komposisi,
                    'inventory_id'    => $tambahInve->id,
                    'stock'           => $stock->jumlah,
                    'satuan'          => $stock->satuan_komposisi,
                  ]);
                }
              }
            }
            $jurnal = jurnal::where('doc_id',$selectCheckBox[$i])->where('type_id', 1)->first();
            if ($jurnal) {
              $jurnal->update([
                'status' => 1
              ]);
            }
          }
          return redirect()->back()->with('toast_success', 'Transaksi Telah Dinyatakan Valid');
        }elseif($allCheckBox < $selectCheckBox){
          for ($i=0; $i < count($selectCheckBox); $i++) {
            $transaksi = transaksi::where('id', $selectCheckBox[$i])->first();
            $transaksi->update([
              'status'=>'valid'
            ]);
            $stockDigunakan = DB::table('transaksis')
                                ->leftJoin('detail_transaksis','detail_transaksis.id_transaksi','=','transaksis.id')
                                ->leftJoin('menus','detail_transaksis.id_menu','=','menus.id')
                                ->leftJoin('komposisis','komposisis.menu_id','=','menus.id')
                                ->where('transaksis.id',$transaksi->id)
                                ->groupBy('komposisis.kategori_komposisi')
                                ->select('komposisis.kategori_komposisi','komposisis.brand_komposisi',DB::raw('sum(komposisis.jumlah_komposisi) as jumlah'),'komposisis.satuan_komposisi')
                                ->get();
            foreach ($stockDigunakan as $key => $stock) {
              if ($stock->brand_komposisi != null) {
                $checkIdInven = Inventory::where('brand_inventory', $stock->brand_komposisi)
                                          ->where('lokasi_id', $transaksi->id_gerai_transaksi)
                                          ->first();
                if ($checkIdInven) {
                  $getPemakain = stockBarang::where('kategori_barang', $stock->kategori_komposisi)
                                            ->where('inventory_id', $checkIdInven->id)
                                            ->first();
                  if ($getPemakain) {
                    $updateStock = $getPemakain->stock + $stock->jumlah;
                    $getPemakain->update([
                      'stock' => $updateStock
                    ]);
                  }else{
                    $updateBaru = stockBarang::create([
                      'kategori_barang' => $stock->kategori_komposisi,
                      'inventory_id'    => $checkIdInven->id,
                      'stock'           => $stock->jumlah,
                      'satuan'           => $stock->satuan_komposisi,
                    ]);
                  }
                }else{
                  $kategori = KategoriInventory::where('nama_kategori',$stock->kategori_komposisi)->first();
                  $tambahInve = Inventory::create([
                    'id_kategori' => $kategori->id,
                    'lokasi_id'   => $transaksi->id_gerai_transaksi,
                    'brand_inventory' => $stock->brand_komposisi,
                    'satuan_inventory' => $stock->satuan_komposisi,
                    'kuantitas_inventory' => 0,
                    'harga_inventory'   => 0,
                    'status_inventory' => 'invalid',
                  ]);
                  $updateBaru = stockBarang::create([
                    'kategori_barang' => $stock->kategori_komposisi,
                    'inventory_id'    => $tambahInve->id,
                    'stock'           => $stock->jumlah,
                    'satuan'          => $stock->satuan_komposisi,
                  ]);
                }
              }
            }
            
            $jurnal = jurnal::where('doc_id',$selectCheckBox[$i])->where('type_id', 1)->first();
            if ($jurnal) {
              $jurnal->update([
                'status' => 1
              ]);
            }
          }
          return redirect()->back()->with('toast_success', 'Transaksi Telah Dinyatakan Valid');
        }
      }
    }
    public function geraiPin()
    {
      return view('page.pin');
    }
    public function checkPin($pin)
    {
      $gerai = Gerai::where('pin_gerai',$pin)->where('status_gerai','aktif')->first();
      $data = '';
      if ($gerai) {
        $data = $gerai->nama_gerai;
      }else{
        $data = 'kosong';
      }
      return response()->json($data, 200);
    }
}
