<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use App\Menu;
use App\Komposisi;
use App\KategoriInventory;
use App\kategoriMenu;
use DB;
use Auth;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class MenuController extends Controller
{
    public function index(){
      if(Auth::check()){
        $menus = Menu::all();
        return view('menu.index',compact('menus'));
      }else{
        return redirect('/login')->with('toast_info', 'Silahkan Login Terlebih Dahulu');
      }
    }
    public function menuPost(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'nama_menu' => 'required',
        'harga_menu' => 'required|integer',
        'tipe_menu' => 'required'
      ]);
      if ($validator->fails()) {
        return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
      }else{
        $imageName = 'blank.png';
        if ($request->avatar != null && $request->avatar != '') {
            $imageName = uniqid('img_menu_').'.'.$request->avatar->getClientOriginalExtension();
            $path = public_path('berkas/menu');
            $gambar = Image::make($request->avatar)->resize(200,200);
            if (!File::isDirectory($path)) {
                File::makeDirectory($path, 0777, true, true);
                $gambar->save(public_path('berkas/menu/'.$imageName),80);
            }else{
                $gambar->save(public_path('berkas/menu/'.$imageName),80);
            }
        }
        $kategoriName = '';
        $check = kategoriMenu::where('name', $request->kategori_menu)->first();
        if ($check) {
          $kategoriName = $check->name;
        }else{
          $createKategori = kategoriMenu::create([
            'name' => $request->kategori_menu
          ]);
          $kategoriName = $createKategori->name;
        }

        $create = Menu::create([
          'nama_menu'  => $request->nama_menu,
          'harga_menu' => $request->harga_menu,
          'tipe_menu' =>  $request->tipe_menu,
          'kategori_menu' => $kategoriName,
          'foto_menu' =>  $imageName,
          'status_menu' => 1,
        ]);

        if (count($request->kategori_komposisi)>0) {
          for ($i=0; $i < count($request->kategori_komposisi); $i++) {
            if ($request->kategori_komposisi[$i] != null) {
              $komposisi = Komposisi::create([
                'menu_id'  => $create->id,
                'kategori_komposisi' => $request->kategori_komposisi[$i],
                'brand_komposisi' => $request->brand[$i],
                'jumlah_komposisi'   => $request->jumlah_komposisi[$i],
                'satuan_komposisi'   => $request->satuan_komposisi[$i],
              ]);
            }
          }
        }
        return redirect()->back()->with('toast_success', 'Menu Berhasil Di Buat');
      }
    }
    public function deleteMenu(Request $request)
    {
      try {
        $menu = menu::findOrFail($request->id);
        $komposisi = Komposisi::where('menu_id', $request->id)->get();
        foreach ($komposisi as $key => $komposisis) {
          $komposisis->delete();
        }
        $menu->delete();
      } catch (ModelNotFoundException $exception) {
        return back()->with('toast_error', $exception->getMessage());
      }
      return redirect()->back()->with('toast_success', 'Menu Berhasil Di Hapus');
    }
    public function editMenu(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'name' => 'required',
        'harga' => 'required|integer',
        'tipe' => 'required'
      ]);
      if ($validator->fails()) {
        return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
      }else{
        $imageName = 'empty';
        if ($request->avatar != null) {
          if ($request->avatar != null && $request->avatar != '') {
              $imageName = uniqid('img_karyawan_').'.'.$request->avatar->getClientOriginalExtension();
              $path = public_path('berkas/menu');
              $gambar = Image::make($request->avatar)->resize(50,50);
              if (!File::isDirectory($path)) {
                  File::makeDirectory($path, 0777, true, true);
                  $gambar->save(public_path('berkas/menu/'.$imageName),80);
              }else{
                  $gambar->save(public_path('berkas/menu/'.$imageName),80);
              }
          }
        }elseif ($request->foto != null) {
          $imageName = $request->foto;
        }

        $kategoriName = '';
        $check = kategoriMenu::where('name', $request->kategori_menu)->first();
        if ($check) {
          $kategoriName = $check->name;
        }else{
          $createKategori = kategoriMenu::create([
            'name' => $request->kategori_menu
          ]);
          $kategoriName = $createKategori->name;
        }

        $create = menu::findOrFail($request->id);
        $create->update([
          'nama_menu'  => $request->name,
          'harga_menu' => $request->harga,
          'kategori_menu' => $kategoriName,
          'tipe_menu' =>  $request->tipe,
          'foto_menu' =>  $imageName,
          'status_menu' => 1,
        ]);

        // $komposisi = Komposisi::where('menu_id',$request->id)->get();
        // foreach ($komposisi as $i => $komposisis) {
          // if ($request->komposisi_id != null) {
          //   for ($i=0; $i < count($request->komposisi_id); $i++) {
              // if ($komposisis->id == $request->komposisi_id[$i]) {
              //   $datailMenu = Komposisi::where('id',$request->komposisi_id[$i])->first();
              //   $datailMenu->update([
              //     'kategori_komposisi' => $request->kategori[$i],
              //     'brand_komposisi' => $request->brand[$i],
              //     'jumlah_komposisi'   => $request->jumlah[$i],
              //     'satuan_komposisi'   => $request->satuan[$i],
              //   ]);
              // }else{
              //   $datailMenu = Komposisi::where('id',$komposisis->id)->first();
              //   $datailMenu->delete();
              // }
          //   }
          // }
        // }
        if ($request->komposisi_id != null) {
          for ($i=0; $i < count($request->komposisi_id); $i++) {
            $komposisi = Komposisi::where('id',$request->komposisi_id[$i])->first();
            $komposisi->update([
              'kategori_komposisi' => $request->kategori[$i],
              'brand_komposisi' => $request->brand[$i],
              'jumlah_komposisi'   => $request->jumlah[$i],
              'satuan_komposisi'   => $request->satuan[$i],
            ]);
          }
        }

        if ($request->hapusKomposisi != null) {
          for ($i=0; $i < count($request->hapusKomposisi); $i++) {
            $komposisi = Komposisi::where('id',$request->hapusKomposisi[$i])->first();
            $komposisi->delete();
          }
        }

        if (count($request->kategori_komposisi_new)>0) {
          for ($i=0; $i < count($request->kategori_komposisi_new); $i++) {
            if ($request->kategori_komposisi_new[$i] != null) {
              $komposisi = Komposisi::create([
                'menu_id'  => $create->id,
                'kategori_komposisi' => $request->kategori_komposisi_new[$i],
                'brand_komposisi' => $request->brand_komposisi_new[$i],
                'jumlah_komposisi'   => $request->jumlah_komposisi_new[$i],
                'satuan_komposisi'   => $request->satuan_komposisi_new[$i],
              ]);
            }
          }
        }

        return redirect()->back()->with('toast_success', 'Menu Berhasil Di Edit');
      }
    }
    public function getKomposisiMenu($id)
    {
      $tmp = explode('||',$id);
      $kompos = [];
      $komposisi = Komposisi::where('menu_id', $tmp[0])->get();
      foreach ($komposisi as $key1 => $resep) {
        $kategori = KategoriInventory::where('nama_kategori', $resep->kategori_komposisi)->first();
        $kompos[$key1] = [
          'id' => $resep->id,
          'kategori_id' => $kategori->id,
          'kategori' => $resep->kategori_komposisi,
          'brand_komposisi' => $resep->brand_komposisi,
          'qty' => $resep->jumlah_komposisi,
          'satuan' => $resep->satuan_komposisi,
        ];
      }
      return response()->json($kompos);
    }
    public function tambahKategori(Request $request)
    {
      $kategoriName = '';
      $check = kategoriMenu::where('name', $request->kategori_menu)->first();
      if ($check) {
        $kategoriName = $check->name;
      }else{
        $createKategori = kategoriMenu::create([
          'name' => $request->kategori_menu
        ]);
        $kategoriName = $createKategori->name;
      }
      return redirect()->back()->with('toast_success', 'Kategori Berhasil Di Tambah');
    }
}
