<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\stockBarang;
use DB;
use Carbon\Carbon;

class LaporanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $income = DB::table('transaksis')
                    ->leftJoin('gerais','transaksis.id_gerai_transaksi','=','gerais.id')
                    ->whereNotNull('transaksis.id_gerai_transaksi')
                    ->where('transaksis.status','!=','batal')
                    ->groupBy('transaksis.id_gerai_transaksi')
                    ->select('gerais.nama_gerai',DB::raw('SUM(transaksis.total_dibayar_transaksi-(transaksis.discount_transaksi/100*transaksis.total_dibayar_transaksi)) as total'))
                    ->get();
        $terlaris = DB::table('transaksis')
                    ->leftJoin('detail_transaksis', 'detail_transaksis.id_transaksi','=','transaksis.id')
                    ->leftJoin('gerais','transaksis.id_gerai_transaksi','=','gerais.id')
                    ->whereNotNull('transaksis.id_gerai_transaksi')
                    ->where('transaksis.status','!=','batal')
                    ->groupBy('detail_transaksis.nama')
                    ->select('detail_transaksis.nama',DB::raw('count(detail_transaksis.id_menu) as jumlah'))
                    ->orderBy('jumlah', 'desc')
                    ->limit(5)
                    ->get();
        $inventory = DB::table('transaksis')
                        ->leftJoin('detail_transaksis','detail_transaksis.id_transaksi','=','transaksis.id')
                        ->leftJoin('menus','detail_transaksis.id_menu','=','menus.id')
                        ->leftJoin('komposisis','komposisis.menu_id','=','menus.id')
                        ->leftJoin('kategori_inventories','kategori_inventories.nama_kategori','=','komposisis.kategori_komposisi')
                        ->leftJoin('inventories','inventories.id_kategori','=','kategori_inventories.id')
                        ->whereNotNull('transaksis.id_gerai_transaksi')
                        ->where('transaksis.status','!=','batal')
                        ->groupBy('kategori_inventories.nama_kategori')
                        ->select('kategori_inventories.nama_kategori',DB::raw('count(komposisis.jumlah_komposisi) as jumlah'),'komposisis.satuan_komposisi')
                        ->orderBy('jumlah', 'desc')
                        ->get();
        $customer = DB::table('users')
                    ->where('level','customer')
                    ->whereNotNull('alamat')
                    ->groupBy('alamat')
                    ->select('alamat', DB::raw('count(alamat) as jumlah'))
                    ->get();
        $gerai = DB::table('gerais')
                    ->select('nama_gerai', 'lat_lng_gerai','lokasi_gerai')
                    ->get();
        $data = [
            'income' => $income,
            'terlaris' => $terlaris,
            'inventory' => $inventory,
            'customer' => $customer,
            'gerai' => $gerai,
        ];
        return view('laporan.index',compact('data'));
    }
    public function laporanApi()
    {
        $income = DB::table('transaksis')
                    ->leftJoin('gerais','transaksis.id_gerai_transaksi','=','gerais.id')
                    ->whereNotNull('transaksis.id_gerai_transaksi')
                    ->where('transaksis.status','!=','batal')
                    ->groupBy('transaksis.id_gerai_transaksi')
                    ->select('gerais.nama_gerai',DB::raw('SUM(transaksis.total_dibayar_transaksi-(transaksis.discount_transaksi/100*transaksis.total_dibayar_transaksi)) as total'))
                    ->get();
        $terlaris = DB::table('transaksis')
                    ->leftJoin('detail_transaksis', 'detail_transaksis.id_transaksi','=','transaksis.id')
                    ->leftJoin('gerais','transaksis.id_gerai_transaksi','=','gerais.id')
                    ->whereNotNull('transaksis.id_gerai_transaksi')
                    ->where('transaksis.status','!=','batal')
                    ->groupBy('detail_transaksis.nama')
                    ->select('detail_transaksis.nama',DB::raw('count(detail_transaksis.id_menu) as jumlah'))
                    ->orderBy('jumlah', 'desc')
                    ->limit(5)
                    ->get();
        // $inventory = DB::table('transaksis')
        //                 ->leftJoin('detail_transaksis','detail_transaksis.id_transaksi','=','transaksis.id')
        //                 ->leftJoin('menus','detail_transaksis.id_menu','=','menus.id')
        //                 ->leftJoin('komposisis','komposisis.menu_id','=','menus.id')
        //                 ->leftJoin('kategori_inventories','kategori_inventories.nama_kategori','=','komposisis.kategori_komposisi')
        //                 ->leftJoin('inventories','inventories.id_kategori','=','kategori_inventories.id')
        //                 ->whereNotNull('transaksis.id_gerai_transaksi')
        //                 ->where('transaksis.status','!=','batal')
        //                 ->groupBy('kategori_inventories.nama_kategori')
        //                 ->select('kategori_inventories.nama_kategori',DB::raw('sum(komposisis.jumlah_komposisi) as jumlah'),'komposisis.satuan_komposisi')
        //                 ->orderBy('jumlah', 'desc')
        //                 ->get();
        $inventory = stockBarang::groupBy('kategori_barang')
                        ->select('kategori_barang as nama_kategori',DB::raw('sum(stock) as jumlah'), 'satuan as satuan_komposisi')
                        ->get();
        // dd($inventory);        
        $customer = DB::table('users')
                    ->where('level','customer')
                    ->whereNotNull('alamat')
                    ->groupBy('alamat')
                    ->select('alamat', DB::raw('count(alamat) as jumlah'))
                    ->get();
        $gerai = DB::table('gerais')
                    ->select('nama_gerai', 'lat_lng_gerai','lokasi_gerai')
                    ->get();
        $data = [
            'income' => $income,
            'terlaris' => $terlaris,
            'inventory' => $inventory,
            'customer' => $customer,
            'gerai' => $gerai,
        ];
        return response()->json($data, 200);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
