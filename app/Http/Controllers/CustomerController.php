<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KategoriInventory;
use App\inventory;
use App\user;
use App\email;
use DB;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Mail\custumerEmail;
use Illuminate\Support\Facades\Mail;

class CustomerController extends Controller
{
    public function customerIndex()
    {
        $data = User::where('level','customer')->get();
        return view('customer.index',compact('data'));
    }
    public function customerPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'email' => 'required',
            'no_tlpn' => 'required',
          ]);
        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }else{
            if ($request->code_post == 1) {
                User::create([
                'level'=>'customer',
                'nama'=>$request->nama,
                'email'=>$request->email,
                'no_tlpn'=>$request->no_tlpn,
                'alamat'=>$request->alamat,
                'password'   => bcrypt('cheesteacustomer'),
                'status_user'=>'aktif',
                //   'foto_user'=>$foto_user
                ]);
                //jika edit
            }elseif($request->code_post == 2){
                $user = User::find($request->id_user);
                $user->update([
                'nama'=>$request->nama,
                'email'=>$request->email,
                'no_tlpn'=>$request->no_tlpn,
                'alamat'=>$request->alamat,
                'password'=>bcrypt('cheestea'),
                'foto_user'=>$foto_user,
                ]);
        
                //jika non aktif
            }else {
                $user = User::find($request->id_user);
                $user->update([
                'status_user'=>'tidak aktif'
                ]);
            }
        
            return redirect()->back()->with('toast_success', 'data tersimpan');
        }
    }
    public function customerEmail(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'email' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }else{
            $imageName = 'empty';
            if ($request->berkas_inventory != null) {
              if ($request->berkas_inventory != null && $request->berkas_inventory != '') {
                  $imageName = uniqid('berkas_email_').'.'.$request->berkas_inventory->getClientOriginalExtension();
                  $path = public_path('storage');
                  if (!File::isDirectory($path)) {
                      File::makeDirectory($path, 0777, true, true);
                      $request->berkas_inventory->move('storage/', $imageName);
                  }else{
                    $request->berkas_inventory->move('storage/', $imageName);
                  }
              }
            }elseif ($request->foto != null) {
              $imageName = $request->berkas_inventory;
            }

            $email = email::create([
                'email' => $request->email,
                'title' => $request->title,
                'pesan' => $request->pesan,
                'attchment' => $imageName,
            ]);
               
            \Mail::to($request->email)->send(new custumerEmail($email));
            return redirect()->back()->with('toast_success', 'Berhasil Mengirim Email');

        }

    }
}
