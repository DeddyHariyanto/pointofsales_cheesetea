<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\ValidationException;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
use App\Gerai;
use App\UserGerai;
use App\detailTransaksi;
use App\User;
use App\Absensi;
use App\jurnal;
use App\Menu;
use Carbon\Carbon;
use App\transaksi;
use App\Inventory;
use DB;
use Auth;

class PesananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pesananindex($status)
    {
        if(Auth::check()){
            $data = [];
            $user = UserGerai::where('id_user',Auth::user()->id)->first();
            if(Auth::user()->level == 'owner'){
                $pesanan = DB::table('transaksis')
                            ->leftJoin('gerais','transaksis.id_gerai_transaksi','=','gerais.id')
                            ->leftJoin('users','transaksis.id_customer_transaksi','=','users.id')
                            // ->whereDate('transaksis.created_at', Carbon::today())
                            ->where('transaksis.status', $status)
                            // ->where('transaksis.tipe_bayar_transaksi', 1)
                            ->select(
                                'transaksis.id as id',
                                'transaksis.code_transaksi as code',
                                'gerais.nama_gerai as nama_gerai',
                                'users.nama as customer',
                                'users.email as email',
                                'transaksis.tipe_bayar_transaksi as tipe_bayar',
                                'transaksis.discount_transaksi as discount',
                                'transaksis.jumlah_bayar_transaksi as jumlah_bayar',
                                'transaksis.total_dibayar_transaksi as total_dibayar',
                                'transaksis.status as status')->orderBy('transaksis.id','desc')->get();
                foreach ($pesanan as $key => $pesanans) {
                    
                    $data[$key] = [
                        'id' => $pesanans->id,
                        'nama_gerai' => $pesanans->nama_gerai,
                        'code' => $pesanans->code,
                        'customer' => $pesanans->customer,
                        'email' => $pesanans->email,
                        'tipe_bayar' => $pesanans->tipe_bayar,
                        'discount' => $pesanans->discount,
                        'jumlah_bayar' => $pesanans->jumlah_bayar,
                        'total_dibayar' => $pesanans->total_dibayar,
                        'status' => $pesanans->status,
                        // 'detail' => $detail,
                    ];
                }
                return view('pesanan.index',compact('data','status'));
            }elseif ($user) {
                $pesanan = DB::table('transaksis')
                            ->leftJoin('gerais','transaksis.id_gerai_transaksi','=','gerais.id')
                            // ->leftJoin('user_gerais','transaksis.id_user_transaksi','=','user_gerais.id_user')
                            ->leftJoin('users','transaksis.id_customer_transaksi','=','users.id')
                            ->whereDate('transaksis.created_at', Carbon::today())
                            ->where('transaksis.status', $status)
                            ->where('transaksis.id_gerai_transaksi', $user->id_gerai)
                            // ->Where('user_gerais.status_user_gerai', "aktif")
                            // ->where('transaksis.tipe_bayar_transaksi', 1)
                            ->select(
                                'transaksis.id as id',
                                'transaksis.code_transaksi as code',
                                'gerais.nama_gerai as nama_gerai',
                                'users.nama as customer',
                                'users.email as email',
                                'transaksis.tipe_bayar_transaksi as tipe_bayar',
                                'transaksis.discount_transaksi as discount',
                                'transaksis.jumlah_bayar_transaksi as jumlah_bayar',
                                'transaksis.total_dibayar_transaksi as total_dibayar',
                                'transaksis.status as status')->orderBy('transaksis.id','desc')->get();
                // dd($pesanan);
                foreach ($pesanan as $key => $pesanans) {
                    
                    $data[$key] = [
                        'id' => $pesanans->id,
                        'nama_gerai' => $pesanans->nama_gerai,
                        'code' => $pesanans->code,
                        'customer' => $pesanans->customer,
                        'email' => $pesanans->email,
                        'tipe_bayar' => $pesanans->tipe_bayar,
                        'discount' => $pesanans->discount,
                        'jumlah_bayar' => $pesanans->jumlah_bayar,
                        'total_dibayar' => $pesanans->total_dibayar,
                        'status' => $pesanans->status,
                        // 'detail' => $detail,
                    ];
                }
                // dd($data);
                return view('pesanan.index',compact('data','status'));
            }else{
                return view('pesanan.index',compact('data','status'));

            }
        }else{
            return redirect('/login')->with('toast_info', 'Silahkan Login kembali');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pesananKonfirmasi(Request $request)
    {
        $transaksi = transaksi::where('id', $request->idKonfirmasiPesanan)->first();
        $transaksi->update([
            'status'=>'selesai'
        ]);
        return redirect()->back()->with('toast_success', 'Pesanan Telah Selesai');
    }
    public function hapusPesanan(Request $request)
    {
        try {
            $transaksi = transaksi::findOrFail($request->id);
            $transaksi->update([
                'status'=>'batal'
            ]);
            $checkJurnal = jurnal::where('type_id',1)->where('doc_id',$transaksi->id)->first();
            if ($checkJurnal) {
                $checkJurnal->delete();
            }
        } catch (ModelNotFoundException $exception) {
        return back()->with('toast_error', $exception->getMessage());
        }
        return redirect()->back()->with('toast_success', 'Transaksi Berhasil Di Batalkan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
