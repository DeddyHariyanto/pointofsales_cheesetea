<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\ValidationException;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
use App\Gerai;
use App\User;
use App\UserGerai;
use App\Menu;
use App\akses;
use App\transaksi;
use App\detailTransaksi;
use App\Komposisi;
use App\Inventory;
use App\stockBarang;
use App\KategoriInventory;
use App\jurnal;
use App\kategoriMenu;
use Carbon\Carbon;
use Auth;
use DB;
use App;
use Barryvdh\DomPDF\Facade\Pdf;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\Mail;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\TransaksiAllExport;
use App\Exports\TransaksiBulanIniExport;
use App\Exports\TransaksiHariIniExport;
use App\Exports\JurnalBulanIniExport;

use App\Imports\jurnalImport;

class KasirController extends Controller
{
    public function kasir(){
      if(Auth::check()){
        $akses = akses::where('id_user', Auth::user()->id)->orderby('id','desc')->first();
        if ($akses) {
          $gerai = Gerai::find($akses->id_gerai);
          $start = new Carbon($akses->date_in);
          $end = Carbon::now('Asia/Makassar');
          $total_actual = $start->diffInMinutes($end);
          if (gmdate("H:i", $total_actual) < '08:00') {
            $no = 0;
            $dataMenu = [];
            $kategori = kategoriMenu::get();
            foreach ($kategori as $key => $kategoris) {
              $subdata = [];
              $menu = Menu::where('kategori_menu',$kategoris->name)->get();
              foreach ($menu as $keys => $menuKate) {
                $subdata[$no] = [
                  'id' => $menuKate->id,
                  'nama_menu' => $menuKate->nama_menu,
                  'harga_menu' => $menuKate->harga_menu,
                  'foto_menu' => $menuKate->foto_menu,
                ];
                $no++;
              }
              $dataMenu[] = [
                'kategori' => $kategoris->name,
                'menu' => array_values($subdata),
              ];
            }
            $data = [
              'gerai_id'=>$akses->id_gerai,
              'nama_gerai'=>$gerai->nama_gerai,
              'dataMenu'=> $dataMenu,
            ];
            return view('kasir.index',compact('data'));
          }elseif (gmdate("H:i", $total_actual) >= '08:00') {
            return redirect('/pin');
          }
        }else{
          return redirect('/pin')->with('toast_info', 'Silahkan isi Pin kembali');

        }
      }else{
        return redirect('/pin')->with('toast_info', 'Silahkan isi Pin kembali');
      }

    }
    public function kasirPost(Request $request)
    {
      // dd($request->all());
      $no = 0;
      $transaksi = transaksi::orderby('id','desc')->first();
      if ($transaksi == null) {
        $no = 1;
      }else{
        $no = $transaksi->id + 1;
      }

      $customerid = '';
      // dd(explode('@',$request->customer_id));
      if ($request->customer_id != null) {
        $users = User::where('email',$request->customer_id)->first();
        if ($users) {
          $customerid = $users->id;
        }else{
          $customer = User::create([
            'level'=>'customer',
            'nama'=> explode('@',$request->customer_id)[0],
            'email'=> $request->customer_id,
            'password'   => bcrypt('sobatcheestea'),
            'status_user'=>'aktif',
          ]);
          $customerid = $customer->id;
        }
      }else{
        $customerid = '';
      }
      $discount = 0;
      if ($request->discount != null) {
        $discount = $request->discount;
      }
      if ($request->jumlah_dibayar >= ($request->total_dibayar - $request->jumlah_potongan)) {
        $transaksi = transaksi::create([
          'code_transaksi' => $no,
          'id_gerai_transaksi' => $request->gerai_id,
          'id_user_transaksi' => $request->user_id,
          'id_customer_transaksi' => $customerid,
          'tipe_bayar_transaksi' => $request->tipe_pembayaran,
          'discount_transaksi' => $discount,
          'jumlah_bayar_transaksi' => $request->jumlah_dibayar,
          'total_dibayar_transaksi' => $request->total_dibayar,
          'no_rekening' => $request->no_rekening,
          'nama_rekening' => $request->nama_rekening,
          'status' => 'invalid',
        ]);
        $jurnal = jurnal::create([
          'type_id'     => 1,
          'doc_id'      => $transaksi->id,
          'gerai_id'    => $request->gerai_id,
          'title'       => 'struk No '.$no,
          'description' => 'Pembeliaan',
          'debit'       => $request->total_dibayar - $request->jumlah_potongan,
          'status'      => 'invalid',
        ]);
        $item = $request->id_menu;
        for ($i=0; $i < count($item); $i++) { 
          $menu = Menu::where('id',$item[$i])->first();
          $itemsCreate = detailTransaksi::create([
            'id_transaksi'=> $transaksi->id,
            'kategori' => $menu->tipe_menu,
            'id_menu' => $item[$i],
            'kuantiti' => 1,
            'nama' => $menu->nama_menu,
            'harga' => $request->harga_menu[$i],
          ]);
        }
        $kembalian = $request->jumlah_dibayar - ($request->total_dibayar - $request->jumlah_potongan);
        // return redirect()->back()->with('toast_success', 'Transaksi Telah Tersimpan');
        if ($request->tanpa_struk == null) {
          $email = $request->customer_id;
          if ($email != null) {
            $details = [
              'id' => $transaksi->id,
              'title' => $transaksi->code_transaksi,
              'gerai_id' => $transaksi->id_gerai_transaksi,
              'kasir' => $transaksi->id_user_transaksi,
              // 'detail_struk' => detailTransaksi::where('id_transaksi',$transaksi->id)->get(),
              'discount_transaksi' => $transaksi->discount_transaksi,
              'jumlah_bayar_transaksi' => $transaksi->jumlah_bayar_transaksi,
              'total_dibayar_transaksi' => $transaksi->total_dibayar_transaksi,
            ];
             
            \Mail::to($request->customer_id)->send(new \App\Mail\MyTestMail($details));
          }
        }

        return redirect()->back()->with('success', '<b>Transaksi Berhasil<b><br> 
                                                    <b>No.Transaksi :#' . $transaksi->code_transaksi .'<b></b>
                                                    <br><p>Uang Kembali<p> <b>Rp. '.number_format($kembalian).'</b><br>
                                                    <a href="/kasir-struk/'. $transaksi->id .'" target="_blank">link Struk</a>');
        
      }else{
        return back()->with('toast_error', 'Nominal yang Anda masukan Kurang Dari Total Dibayar')->withInput();
      }
    }
    public function kasirStruk($id)
    {
      $struk = DB::table('transaksis')
                  ->leftJoin('gerais','transaksis.id_gerai_transaksi','=','gerais.id')
                  ->leftJoin('users','transaksis.id_user_transaksi','=','users.id')
                  // ->leftJoin('detail_transaksis','transaksis.id','=','detail_transaksis.id_transaksi')
                  ->where('transaksis.id',$id)
                  ->select(
                    'transaksis.id as id',
                    'transaksis.code_transaksi as code',
                    'gerais.nama_gerai as nama_gerai',
                    'gerais.lokasi_gerai as lokasi_gerai',
                    'users.nama as kasir',
                    'transaksis.tipe_bayar_transaksi as tipe_bayar',
                    'transaksis.jumlah_bayar_transaksi as jumlah_bayar',
                    'transaksis.discount_transaksi as discount',
                    'transaksis.total_dibayar_transaksi as total_dibayar',
                    'transaksis.created_at as tanggal')->first();
      $detail = detailTransaksi::where('id_transaksi', $id)
                                ->groupBy('id_menu')
                                ->select('nama','harga',DB::raw('count(id_menu) as qty'))
                                ->get();
      $detailStruk = [];
      foreach ($detail as $key => $details) {
        $detailStruk[$key] = [
          'nama' =>$details->nama,
          'harga_satuan' =>$details->harga,
          'harga_total' =>$details->harga * $details->qty,
          'qty' =>$details->qty,
        ];
      }
      $data = [
        'id' => $struk->id,
        'code' => $struk->code,
        'nama_gerai' => $struk->nama_gerai,
        'lokasi_gerai' => $struk->lokasi_gerai,
        'kasir' => $struk->kasir,
        'tipe_bayar' => $struk->tipe_bayar,
        'jumlah_bayar' => $struk->jumlah_bayar,
        'discount' => $struk->discount,
        'total_dibayar' => $struk->total_dibayar,
        'tanggal' => Carbon::parse($struk->tanggal)->format('d M Y'),
        'detail' => $detailStruk,
        'gambar' => '../assets/media/cheestea/icon.png'
      ];
      $customPaper = array(0,0,567.00,283.80);
      $pdf = PDF::loadView('kasir.pdf.struk', compact('data'))->setPaper($customPaper, 'landscape');
      return $pdf->stream();
    }
    public function kasirKirim(Request $request)
    {
      if ($request->code == 1) {
        $transaksi = transaksi::where('id',$request->id)->first();
        $checkEmail = User::where('id', $transaksi->id_customer_transaksi)->first();
        $details = [
          'id' => $transaksi->id,
          'title' => $transaksi->code_transaksi,
          'gerai_id' => $transaksi->id_gerai_transaksi,
          'kasir' => $transaksi->id_user_transaksi,
          // 'detail_struk' => detailTransaksi::where('id_transaksi',$transaksi->id)->get(),
          'discount_transaksi' => $transaksi->discount_transaksi,
          'jumlah_bayar_transaksi' => $transaksi->jumlah_bayar_transaksi,
          'total_dibayar_transaksi' => $transaksi->total_dibayar_transaksi,
        ];
         if ($checkEmail) {
           \Mail::to($checkEmail->email)->send(new \App\Mail\MyTestMail($details));
           return redirect()->back()->with('toast_success', 'Berhasil Kirim Struk');
         }else{
          return back()->with('toast_error', 'Tidak Ada Alamat Email')->withInput();
         }
      }elseif ($request->code == 2) {
        $transaksi = transaksi::where('id',$request->id)->first();
        if($transaksi){
          $details = [
            'id' => $transaksi->id,
            'title' => $transaksi->code_transaksi,
            'gerai_id' => $transaksi->id_gerai_transaksi,
            'kasir' => $transaksi->id_user_transaksi,
            // 'detail_struk' => detailTransaksi::where('id_transaksi',$transaksi->id)->get(),
            'discount_transaksi' => $transaksi->discount_transaksi,
            'jumlah_bayar_transaksi' => $transaksi->jumlah_bayar_transaksi,
            'total_dibayar_transaksi' => $transaksi->total_dibayar_transaksi,
          ];
           if ($request->email) {
             \Mail::to($request->email)->send(new \App\Mail\MyTestMail($details));
             return redirect()->back()->with('toast_success', 'Berhasil Kirim Struk');
           }else{
            return back()->with('toast_error', 'Tidak Ada Alamat Email')->withInput();
           }
        }else{
          return back()->with('toast_error', 'Gagal Mengirim Email code transaksi tidak ditemukan')->withInput();
        }
      }
    }
    public function kasirAkses(Request $request)
    {
      // dd($request->all());
      $validator = Validator::make($request->all(),[
        // 'id' => 'required',
        'master_pin' => 'required|max:6',
      ]);
      if ($validator->fails()) {
        return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
      }else{
        //jika login
        if(Auth::check()){
          if(Auth::user()->level == 'owner'){
            $checkPin = DB::table('gerais')
                          ->leftJoin('user_gerais', 'gerais.id', '=', 'user_gerais.id_gerai')
                          // ->leftJoin('users', 'user_gerais.id_user', '=', 'users.id')
                          ->where('pin_gerai', $request->master_pin)
                          // ->orWhere('pin_user_gerai', $request->master_pin)
                          // ->Where('user_gerais.status_user_gerai', "aktif")
                          ->select(
                            'gerais.id as gerai_id',
                            // 'user_gerais.id_user as user_id',
                            // 'user_gerais.status_user_gerai as status_user_gerai',
                            'gerais.status_gerai')->first();
                            // 'users.email as email',
                            // 'users.level as level',
                            // 'users.re_password as password'
            if($checkPin){
              if($checkPin->status_gerai == 'aktif'){
                  // $password = 123456;
                $akses = akses::Create([
                  'id_user' => Auth::user()->id,
                  'id_gerai'=> $checkPin->gerai_id,
                  'date_in' => Carbon::now('Asia/Makassar')->format('Y-m-d H:i:s'),
                ]);
                $akses = $akses->id;
                  return redirect('/kasir')->with('toast_success', 'Pin Benar');
              }else{
                return redirect('/pin')->with('toast_error', 'Pin Gerai Non Aktif');
              }
            }else{
              return redirect('/pin')->with('toast_error', 'Pin Anda Tidak ditemukan');
            }
          }elseif(Auth::user()->level == 'karyawan'){
            $checkPin = DB::table('gerais')
                          ->leftJoin('user_gerais', 'gerais.id', '=', 'user_gerais.id_gerai')
                          ->leftJoin('users', 'user_gerais.id_user', '=', 'users.id')
                          ->where('user_gerais.id_user', Auth::user()->id)
                          // ->where('pin_gerai', $request->master_pin)
                          ->Where('pin_user_gerai', $request->master_pin)
                          ->Where('user_gerais.status_user_gerai', "aktif")
                          ->orderby('user_gerais.id','desc')
                          ->select(
                            'gerais.id as gerai_id',
                            'user_gerais.id_user as user_id',
                            'user_gerais.status_user_gerai as status_user_gerai',
                            'gerais.status_gerai',
                            'users.email as email',
                            'users.level as level',
                            'users.status_user as status_user',
                            'users.re_password as password' )->first();
            if($checkPin){
              if($checkPin->status_gerai == 'aktif'){
                if($checkPin->status_user_gerai == 'aktif'){
                  if($checkPin->status_user == 'aktif'){
                    // $password = 123456;
                    $akses = akses::Create([
                      'id_user' => $checkPin->user_id,
                      'id_gerai'=> $checkPin->gerai_id,
                      'date_in' => Carbon::now('Asia/Makassar')->format('Y-m-d H:i:s'),
                    ]);
                    $akses = $akses->id;
                      return redirect('/kasir')->with('toast_success', 'Pin Benar');
                  }else{
                    return redirect('/pin')->with('toast_error', 'Akun Anda Telah Di Non Aktifkan');
                  }
                }else {
                  return redirect('/pin')->with('toast_error', 'Pin Anda Telah Di Non Aktifkan');
                }
              }else{
                return redirect('/pin')->with('toast_error', 'Pin Gerai Non Aktif');
              }
            }else{
              return redirect('/pin')->with('toast_error', 'Pin Anda Tidak ditemukan');
            }
          }
        }else{
          //jika tidak login
            $checkPin = DB::table('gerais')
                          ->leftJoin('user_gerais', 'gerais.id', '=', 'user_gerais.id_gerai')
                          ->leftJoin('users', 'user_gerais.id_user', '=', 'users.id')
                          ->where('pin_gerai', $request->master_pin)
                          ->orWhere('pin_user_gerai', $request->master_pin)
                          ->Where('user_gerais.status_user_gerai', "aktif")
                          ->orderby('user_gerais.id','desc')
                          ->select(
                            'gerais.id as gerai_id',
                            'user_gerais.id_user as user_id',
                            'user_gerais.status_user_gerai as status_user_gerai',
                            'gerais.status_gerai',
                            'gerais.pin_gerai',
                            'user_gerais.pin_user_gerai',
                            'users.email as email',
                            'users.level as level',
                            'users.status_user as status_user',
                            'users.re_password as password' )->first();
          if ($checkPin) {
            if ($checkPin->status_gerai == 'aktif') {
                if($checkPin->status_user == 'aktif'){
                  //role owner
                  if ($checkPin->pin_gerai != null && $checkPin->pin_user_gerai == null ) {
                    $checkLevel = User::where('level', 'owner')->first();
                      $fieldType = filter_var($checkLevel->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
                      // $password = 123456;
                      $akses = akses::Create([
                        'id_user' => $checkLevel->id,
                        'id_gerai'=> $checkPin->gerai_id,
                        'date_in' => Carbon::now('Asia/Makassar')->format('Y-m-d H:i:s'),
                      ]);
                      $akses = $akses->id;
                      if(auth()->attempt(array($fieldType => $checkLevel->email, 'password' => $checkLevel->re_password))){
                        return redirect('/kasir')->with('toast_success', 'Pin Benar');
                      }else{
                        return redirect('/pin')->with('toast_error', 'PIN Salah');
                      }
                  //role karyawan
                  }elseif ($checkPin->pin_gerai != null && $checkPin->pin_user_gerai != null ) {
                    $fieldType = filter_var($checkPin->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
                    // $password = 123456;
                    if($checkPin->status_user_gerai == 'aktif'){
                      $akses = akses::Create([
                        'id_user' => $checkPin->user_id,
                        'id_gerai'=> $checkPin->gerai_id,
                        'date_in' => Carbon::now('Asia/Makassar')->format('Y-m-d H:i:s'),
                      ]);
                      $akses = $akses->id;
                      if(auth()->attempt(array($fieldType => $checkPin->email, 'password' => $checkPin->password))){
                        return redirect('/kasir')->with('toast_success', 'Pin Benar');
                      }else{
                        return redirect('/pin')->with('toast_error', 'PIN Salah');
                      }
                    }else {
                      return redirect('/pin')->with('toast_error', 'Pin Anda Telah Di Non Aktifkan');
                    }
                  }
                }else{
                  return redirect('/pin')->with('toast_error', 'Akun Anda Telah Di Non Aktifkan');
                }
            }else{
              return redirect('/pin')->with('toast_error', 'Pin Gerai Non Aktif');
            }
          }else{
            return redirect('/pin')->with('toast_error', 'PIN Salah');
          }
        }
      }
    }
    public function kasirCash(Request $request)
    {
      $no = 0;
      $transaksi = transaksi::orderby('id','desc')->first();
      if ($transaksi == null) {
        $no = 1;
      }else{
        $no = $transaksi->id + 1;
      }
      // dd($request->all());
      if ($request->codeCash == 1) {//cash in
        $tipeBayar = '';
        if ($request->jenis == 'tunai' ) {
          $tipeBayar = 4;
        }elseif($request->jenis == 'debit' ){
          $tipeBayar = 5;
        }
        $transaksi = transaksi::create([
          'code_transaksi' => $no,
          'id_gerai_transaksi' => $request->gerai_id,
          'id_user_transaksi' => Auth::user()->id,
          'tipe_bayar_transaksi' => $tipeBayar,
          'total_dibayar_transaksi' => $request->jumlah_nominal,
          'status' => 'invalid',
        ]);

        $jurnal = jurnal::create([
          'type_id' => 3,
          'gerai_id'    => $request->gerai_id,
          'title'       => 'cashin',
          'description' => $request->description,
          'debit'       => $request->jumlah_nominal,
          'status'      => 'invalid',
        ]);
        
        return redirect()->back()->with('toast_success', 'Cash In Tersimpan');
      }elseif($request->codeCash == 2){ //cash out
        if ($request->jenis == 'tunai' ) {
          $tipeBayar = 6;
        }elseif($request->jenis == 'debit' ){
          $tipeBayar = 7;
        }
        $transaksi = transaksi::create([
          'code_transaksi' => $no,
          'id_gerai_transaksi' => $request->gerai_id,
          'id_user_transaksi' => Auth::user()->id,
          'tipe_bayar_transaksi' => $tipeBayar,
          'total_dibayar_transaksi' => $request->jumlah_nominal,
          'status' => 'invalid',
        ]);

        $jurnal = jurnal::create([
          'type_id' => 4,
          'gerai_id'    => $request->gerai_id,
          'title'       => 'cashout',
          'description' => $request->description,
          'kredit'       => $request->jumlah_nominal,
          'status'      => 'invalid',
        ]);
        return redirect()->back()->with('toast_success', 'Cash Out Tersimpan');
      }
    }
    public function exportTransaksiAll($id)
    {
        $bulan = Carbon::now()->month;
        return (new TransaksiAllExport)->id($id)->download('export-transaksi.xlsx');
    }
    public function exportTransaksiBulanIni($id)
    {
        $bulan = Carbon::now()->month;
        return (new TransaksiBulanIniExport)->id($id)->download('export-transaksi.xlsx');
    }
    public function exportTransaksiHariIni($id)
    {
        $bulan = Carbon::now()->month;
        return (new TransaksiHariIniExport)->id($id)->download('export-transaksi.xlsx');
    }
    public function exportJurnalBulanIni($id)
    {
        $bulan = Carbon::now()->month;
        return (new JurnalBulanIniExport)->id($id)->download('export-jurnal.xlsx');
    }
    public function jurnalImport(Request $request)
    {
      // dd($request->all());
      $data = new jurnalImport();
      Excel::import($data, $request->berkas_jurnal);
      for ($i=0; $i < $data->count; $i++) { 
        $checkJurnal = jurnal::findOrFail($data->jurnal[$i]['id']);
        
        if ($data->jurnal[$i]['status'] == 'valid') {
          if ($checkJurnal->type_id == 1) {
            $transaksi = transaksi::where('id', $checkJurnal->doc_id)->first();
            $transaksi->update([
              'status' => 'valid'
            ]);
            $stockDigunakan = DB::table('transaksis')
                                ->leftJoin('detail_transaksis','detail_transaksis.id_transaksi','=','transaksis.id')
                                ->leftJoin('menus','detail_transaksis.id_menu','=','menus.id')
                                ->leftJoin('komposisis','komposisis.menu_id','=','menus.id')
                                ->where('transaksis.id',$transaksi->id)
                                ->groupBy('komposisis.kategori_komposisi')
                                ->select('komposisis.kategori_komposisi','komposisis.brand_komposisi',DB::raw('sum(komposisis.jumlah_komposisi) as jumlah'),'komposisis.satuan_komposisi')
                                ->get();
            foreach ($stockDigunakan as $key => $stock) {
              if ($stock->brand_komposisi != null) {
                $checkIdInven = Inventory::where('brand_inventory', $stock->brand_komposisi)
                                          ->where('lokasi_id', $transaksi->id_gerai_transaksi)
                                          ->first();
                if ($checkIdInven) {
                  $getPemakain = stockBarang::where('kategori_barang', $stock->kategori_komposisi)
                                            ->where('inventory_id', $checkIdInven->id)
                                            ->first();
                  if ($getPemakain) {
                    $updateStock = $getPemakain->stock + $stock->jumlah;
                    $getPemakain->update([
                      'stock' => $updateStock
                    ]);
                  }else{
                    $updateBaru = stockBarang::create([
                      'kategori_barang' => $stock->kategori_komposisi,
                      'inventory_id'    => $checkIdInven->id,
                      'stock'           => $stock->jumlah,
                      'satuan'           => $stock->satuan_komposisi,
                    ]);
                  }
                }else{
                  $kategori = KategoriInventory::where('nama_kategori',$stock->kategori_komposisi)->first();
                  $tambahInve = Inventory::create([
                    'id_kategori' => $kategori->id,
                    'lokasi_id'   => $transaksi->id_gerai_transaksi,
                    'brand_inventory' => $stock->brand_komposisi,
                    'satuan_inventory' => $stock->satuan_komposisi,
                    'kuantitas_inventory' => 0,
                    'harga_inventory'   => 0,
                    'status_inventory' => 'invalid',
                  ]);
                  $updateBaru = stockBarang::create([
                    'kategori_barang' => $stock->kategori_komposisi,
                    'inventory_id'    => $tambahInve->id,
                    'stock'           => $stock->jumlah,
                    'satuan'          => $stock->satuan_komposisi,
                  ]);
                }
              }
            }
          }
          $checkJurnal->update([
            'status' => 1
          ]);
        }elseif ($data->jurnal[$i]['status'] == 1) {
          if ($checkJurnal->type_id == 1) {
            $transaksi = transaksi::where('id', $checkJurnal->doc_id)->first();
            $transaksi->update([
              'status' => 'valid'
            ]);
            $stockDigunakan = DB::table('transaksis')
                                ->leftJoin('detail_transaksis','detail_transaksis.id_transaksi','=','transaksis.id')
                                ->leftJoin('menus','detail_transaksis.id_menu','=','menus.id')
                                ->leftJoin('komposisis','komposisis.menu_id','=','menus.id')
                                ->where('transaksis.id',$transaksi->id)
                                ->groupBy('komposisis.kategori_komposisi')
                                ->select('komposisis.kategori_komposisi','komposisis.brand_komposisi',DB::raw('sum(komposisis.jumlah_komposisi) as jumlah'),'komposisis.satuan_komposisi')
                                ->get();
            foreach ($stockDigunakan as $key => $stock) {
              if ($stock->brand_komposisi != null) {
                $checkIdInven = Inventory::where('brand_inventory', $stock->brand_komposisi)
                                          ->where('lokasi_id', $transaksi->id_gerai_transaksi)
                                          ->first();
                if ($checkIdInven) {
                  $getPemakain = stockBarang::where('kategori_barang', $stock->kategori_komposisi)
                                            ->where('inventory_id', $checkIdInven->id)
                                            ->first();
                  if ($getPemakain) {
                    $updateStock = $getPemakain->stock + $stock->jumlah;
                    $getPemakain->update([
                      'stock' => $updateStock
                    ]);
                  }else{
                    $updateBaru = stockBarang::create([
                      'kategori_barang' => $stock->kategori_komposisi,
                      'inventory_id'    => $checkIdInven->id,
                      'stock'           => $stock->jumlah,
                      'satuan'           => $stock->satuan_komposisi,
                    ]);
                  }
                }else{
                  $kategori = KategoriInventory::where('nama_kategori',$stock->kategori_komposisi)->first();
                  $tambahInve = Inventory::create([
                    'id_kategori' => $kategori->id,
                    'lokasi_id'   => $transaksi->id_gerai_transaksi,
                    'brand_inventory' => $stock->brand_komposisi,
                    'satuan_inventory' => $stock->satuan_komposisi,
                    'kuantitas_inventory' => 0,
                    'harga_inventory'   => 0,
                    'status_inventory' => 'invalid',
                  ]);
                  $updateBaru = stockBarang::create([
                    'kategori_barang' => $stock->kategori_komposisi,
                    'inventory_id'    => $tambahInve->id,
                    'stock'           => $stock->jumlah,
                    'satuan'          => $stock->satuan_komposisi,
                  ]);
                }
              }
            }
          }
          $checkJurnal->update([
            'status' => 1
          ]);
        }elseif ($data->jurnal[$i]['status'] == 0 || $data->jurnal[$i]['status'] == 'invalid') {
          if ($checkJurnal->type_id == 1) {
            $transaksi = transaksi::where('id', $checkJurnal->doc_id)->first();
            $transaksi->update([
              'status' => 'valid'
            ]);
          }
          $checkJurnal->update([
            'status' => 1
          ]);
        }

      }
      return redirect()->back()->with('toast_success', 'Jurnal Import Berhasil Divalidasi');
    }
}
