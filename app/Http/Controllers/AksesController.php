<?php

namespace App\Http\Controllers;

use App\akses;
use Illuminate\Http\Request;

class AksesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\akses  $akses
     * @return \Illuminate\Http\Response
     */
    public function show(akses $akses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\akses  $akses
     * @return \Illuminate\Http\Response
     */
    public function edit(akses $akses)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\akses  $akses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, akses $akses)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\akses  $akses
     * @return \Illuminate\Http\Response
     */
    public function destroy(akses $akses)
    {
        //
    }
}
