<?php

namespace App\Http\Controllers;

use App\Absensi;
use Illuminate\Http\Request;
use App\KategoriInventory;
use App\inventory;
use App\user;
use DB;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AbsensiExport;

class AbsensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function absensiIndex()
    {
        // $data = [];
        $data = DB::table('absensis')
                      ->leftJoin('users', 'users.id', '=','absensis.absensis_user_id')
                      ->select('users.nama','waktu_absensi','lokasi','status_absensi')
                      ->get();

        return view('absensi.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function absensiPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            // 'lat' => 'required',
            // 'lng' => 'required',
            'foto' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }else{
            $imageName = 'blank.png';
            if ($request->foto != null && $request->foto != '') {
                $imageName = uniqid('img_absensi_').'.'.$request->foto->getClientOriginalExtension();
                $path = public_path('berkas/abensi');
                $gambar = Image::make($request->foto)->resize(200,200);
                if (!File::isDirectory($path)) {
                    File::makeDirectory($path, 0777, true, true);
                    $gambar->save(public_path('berkas/abensi/'.$imageName),80);
                }else{
                    $gambar->save(public_path('berkas/abensi/'.$imageName),80);
                }
            }
            $create = Absensi::create([
                'absensis_user_id' => $request->user_id,
                'tanggal_absensi'  => Carbon::now('Asia/Makassar')->format('Y-m-d'),
                'waktu_absensi'    => Carbon::now('Asia/Makassar')->format('H:i:s'),
                'lokasi'           => $request->lokasi_gps,
                'latitude'         => $request->lat,
                'longitude'        => $request->lng,
                'foto_absensi'     => $imageName,
                'status_absensi'   =>'clock-in',
            ]);
            return redirect()->back()->with('toast_success', 'Absensi Berhasil');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function exportAbsensi($id)
    {
        $bulan = Carbon::now()->month;
        return (new AbsensiExport)->id($id)->download('export-absensi-priode-'.$bulan .'.xlsx');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Absensi  $absensi
     * @return \Illuminate\Http\Response
     */
    public function show(Absensi $absensi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Absensi  $absensi
     * @return \Illuminate\Http\Response
     */
    public function edit(Absensi $absensi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Absensi  $absensi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Absensi $absensi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Absensi  $absensi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Absensi $absensi)
    {
        //
    }
}
