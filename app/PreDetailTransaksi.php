<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreDetailTransaksi extends Model
{
    protected $fillable = [
        'id_transaksi',
        'kategori',
        'id_menu',
        'kuantiti',
        'nama',
        'harga',
    ];
}
